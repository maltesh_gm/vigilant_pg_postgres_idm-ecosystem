import os
from fabric.api import *
from build_help import cat_list, wget, mkdir_p, rm_rf, wget_all, arg_value, wrap_text, cp_r, cp, mv, space_join
from StringIO import StringIO
from itertools import chain


WORKSPACE = os.environ.get('WORKSPACE', '/home/jenkins/Monitoring')

JENKINS_HOME = '/home/jenkins'


REPOS_DIR = os.path.join(WORKSPACE, 'Hovercraft/repos')
LIST_DIRS = os.path.join(REPOS_DIR, 'package_lists')
SAMPLE_SITES = os.path.join(WORKSPACE, 'build_scripts/sample_sites')


DETAILS_TEMPLATE = '''\
BUILD_PRODUCT=PHIRepo
BUILD_TIME={date}
BUILD_MACHINE={hostname}
BUILD_FULL_VERSION={version}
'''


def repotrack_list(package_list, output_dir, arch='x86_64'):
    package_one_line = run(space_join('cat', package_list, '| xargs'))
    repotrack_cmd = space_join(
        'repotrack',
        arg_value('-a', arch),
        arg_value('-p', output_dir),
        '-t -n',
        package_one_line
    )
    run(repotrack_cmd)


def pypi_download(package_list, output_dir):
    mkdir_p(output_dir)
    pip_download_cmd = space_join(
        'pip download',
        arg_value('--dest', output_dir),
        arg_value('--requirement', package_list)
    )
    run(pip_download_cmd)


@task
def repotrack(tracked_out_dir, pypi_out_dir, list_dirs=LIST_DIRS):
    with shell_env(http_proxy='', https_proxy=''):
        repotrack_list(os.path.join(list_dirs, 'neb_package_list.txt'), tracked_out_dir)
        repotrack_list(os.path.join(list_dirs, 'vigilant_package_list.txt'), tracked_out_dir)
        for remove_entry in cat_list(os.path.join(list_dirs, 'remove_list.txt')):
            run('rm -f %s' % os.path.join(tracked_out_dir, remove_entry))
        for add_entry in (x.strip() for x in cat_list(os.path.join(list_dirs, 'add_list.txt'))):
            if add_entry.endswith('/*'):
                wget_all_rpms(add_entry[:-2], tracked_out_dir)
            else:
                wget(add_entry, tracked_out_dir)

        pypi_download(os.path.join(list_dirs, 'pypi_package_list.txt'), pypi_out_dir)


def wget_all_rpms(url, destination):
    wget_all(url, destination, '.rpm')


def setup_phirepo_directories(phirepo_path):
    rm_rf(phirepo_path)
    mkdir_p(os.path.join(phirepo_path, 'ecosystem'))


def get_manifested_files(manifest_dir):
    list_manifests_cmd = space_join('ls -d -1', os.path.join(manifest_dir, '*.txt'))
    manifests = run(list_manifests_cmd)
    for item in chain.from_iterable(cat_list(manifest) for manifest in manifests.splitlines()):
        yield item


def copy_manifested_files(manifest_dir, source_root, output_dir):
    for item in get_manifested_files(manifest_dir):
        cp(os.path.join(source_root, item), output_dir, opts='-f')


def create_phirepo(phirepo_path):
    cp(os.path.join(REPOS_DIR, 'phirepo', 'comps.xml'), os.path.join(phirepo_path, ''))
    with cd(JENKINS_HOME):
        run('/usr/bin/createrepo -g comps.xml phirepo')


def generate_build_details(version):
    hostname = run('hostname')
    date = run('date')
    put(StringIO(DETAILS_TEMPLATE.format(date=date, hostname=hostname, version=version)), 'PHIRepo.properties')


def svn_file_import(source, destination, message):
    svn_file_import_cmd = space_join(
        'svn import',
        arg_value('-m', wrap_text(message, '"')),
        source,
        'file://{0}'.format(destination)
    )
    run(svn_file_import_cmd)


@task(default=True)
def phirepo(version, manifest_dir, input_root, tracked_dir):
    with cd(WORKSPACE):
        phirepo_path = os.path.join(JENKINS_HOME, 'phirepo')
        setup_phirepo_directories(phirepo_path)
        phirepo_ecosystem_path = os.path.join(phirepo_path, 'ecosystem')
        copy_manifested_files(os.path.join(input_root, manifest_dir), input_root, phirepo_ecosystem_path)
        cp_r(tracked_dir, os.path.join(phirepo_path, 'internet'))
        create_phirepo(phirepo_path)
        generate_build_details(version)


def get_directories(root):
    directories = run('echo {0}/*/'.format(root))
    return (x.rstrip('/') for x in directories.split())


@task
def svnibc(pdsc_dir, output_dir):
    rm_rf(output_dir)
    run('svnadmin create {0}'.format(output_dir))
    add_svn_sites(SAMPLE_SITES, pdsc_dir, output_dir)


@task
def add_svn_sites(directory, pdsc_dir, output_dir):
    for site_dir in get_directories(directory):
        create_svn_site(site_dir, pdsc_dir, output_dir)


def create_svn_site(site_dir, pdsc_dir, output_dir):
    site_name = os.path.basename(site_dir)
    site_path = os.path.join(output_dir, 'ibc/sites', site_name)
    svn_file_import(site_dir, site_path, 'Initial import')
    svn_file_import(os.path.join(pdsc_dir, 'ansible'), os.path.join(site_path, 'common/ansible'), 'Add EMP')
    svn_file_import(os.path.join(pdsc_dir, 'Merovingian'), os.path.join(site_path, 'common/Merovingian'), 'Add Merv')
