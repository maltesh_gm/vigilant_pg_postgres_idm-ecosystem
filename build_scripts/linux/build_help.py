import os
from fabric.api import *
from fabric.contrib.files import exists
from urlparse import urlparse


def wrap_text(text, character="'"):
    return '{0}{1}{0}'.format(character, text)


def arg_equals_value(arg, value):
    return '{0}={1}'.format(arg, value)


def arg_value(arg, value):
    return '{0} {1}'.format(arg, value)


def space_join(*args):
    return ' '.join(filter(None, args))


def cp(source, destination, opts=None):
    run(space_join('cp', opts, source, destination))


def mv(source, destination):
    run('mv {0} {1}'.format(source, destination))


def cp_r(source, destination):
    cp(source, destination, '-r')


def rm_rf(target):
    assert target
    assert (target != '/') and (target != '/*')
    run('rm -rf {0}'.format(target))


def mkdir(directory, opts=None):
    run(space_join('mkdir', opts, directory))


def mkdir_p(directory):
    mkdir(directory, '-p')


def dir_exists(dirname):
    run('[ -d {0} ]'.format(dirname))


def wget(url, destination=''):
    wget_cmd = ' '.join(['wget --no-proxy -P', str(destination), '-nv', wrap_text(url)])
    run(wget_cmd)


def ensure_clean_dir(directory):
    mkdir_p(directory)
    rm_rf(os.path.join(directory, '*'))


def cat_list(list_file):
    if not exists(list_file):
        return []
    else:
        return run(space_join('cat', list_file)).splitlines()


def wget_all(url, destination, accept=None):
    path_proc = urlparse(url).path.split('/')[1:]
    cut_dirs = len(path_proc)
    include_dir = os.path.join(*path_proc)
    accept_part = arg_value('-A', accept) if accept else ''
    wget_all_cmd = space_join(
        'wget -N -r -np -nH -nv',
        arg_value('--cut-dirs', cut_dirs),
        accept_part,
        '--no-proxy',
        arg_value('-P', destination),
        arg_value('-I', include_dir),
        url
    )
    run(wget_all_cmd)


def sign_rpms(rpm_dir):
    list_rpms_cmd = space_join('ls -d -1', os.path.join(rpm_dir, '*.rpm'))
    rpms = run(list_rpms_cmd)
    with settings(prompts={'Enter pass phrase: ': '1nf0M@t1cs'}):
        for rpm in rpms.splitlines():
            run(space_join('rpm --addsign', rpm))
