#!/usr/bin/env python
from bottle import Bottle, request, BaseRequest, abort
app = Bottle()

import argparse
import yaml
# import json
import time
import logging


LOG_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
LOG_FORMAT = '%(asctime)s.%(msecs).06dZ - %(process)d - %(name)s - %(levelname)s - %(message)s'


log = logging.getLogger('phim.mattis')
log.setLevel(logging.INFO)
logging.Formatter.converter = time.gmtime
formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
handler_console = logging.StreamHandler()
handler_console.setFormatter(formatter)
log.addHandler(handler_console)


results = {}


def read_config(infile):
    with open(infile, 'rb') as f:
        results = yaml.load(f)
    return results


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='input yaml data')
    parser.add_argument('-f', '--infile', help='input file', default='data.yml')
    parser.add_argument('-i', '--interface', help='IP address of the interface to bind to', default='0.0.0.0')
    parser.add_argument('-p', '--port', help='port to run on', type=int, default=8080)

    results = parser.parse_args(args)
    return results.infile, results.interface, results.port


@app.get('/reset')
def reset_results():
    global results
    results = read_config(infile)


# get {'rc': 0, 'output': 'ok - HELLO'}
@app.get('/config/<plugin>/<string_in:path>')
def return_info(plugin, string_in):
    log.info('request for plugin: %s, with arguments: %s', plugin, string_in)
    try:
        config_item = results[plugin][string_in]
    except KeyError:
        msg = 'config not found for plugin: {plugin}, arguments: {arguments}'.format(plugin=plugin, arguments=string_in)
        log.error(msg)
        abort(text=msg)

    count = config_item.get('counter', 0)
    max_ret = len(config_item['results'])

    if count >= max_ret:
        count = max_ret-1

    count = int(config_item['results'][count].get('goto', count))

    config_item['counter'] = count + 1

    return config_item['results'][count]


if __name__ == '__main__':
    infile, interface, port = check_arg()
    results = read_config(infile)
    app.run(host=interface, port=port, debug=True)
