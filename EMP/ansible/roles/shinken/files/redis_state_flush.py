#!/usr/bin/env python
import sys
import redis
from phimutils.stateredis import StateRedis

sys.path.append('/usr/lib/celery/')
from tbconfig import REDIS, STATE

rc = redis.StrictRedis.from_url(REDIS['URL'])
StateRedis(rc, STATE['REDIS_PREFIX'], STATE['REDIS_SET']).flush()
