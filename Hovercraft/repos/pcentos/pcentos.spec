# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name: pcentos
Version: %{_phiversion}
Release: 1%{?dist}
Summary: Philips mirrored CentOS repository

Group: none
License: GPL
URL: http://www.philips.com/healthcare
# Packager Information
Packager: Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot:%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0: %{name}-%{version}.tar.gz
#Patch0:

Requires(pre): yum-utils
Requires(postun): yum-utils
Provides: pcentos

%description
Philips mirrored CentOS repository

%prep
%setup -q

%build
echo OK

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/yum.repos.d
%{__cp} %{_builddir}/%{name}-%{version}/pcentos.repo %{buildroot}%{_sysconfdir}/yum.repos.d/

%clean
%{__rm} -rf %{buildroot}


%pre
%{_bindir}/yum-config-manager --disable base
%{_bindir}/yum-config-manager --disable updates
%{_bindir}/yum-config-manager --disable extras


%postun
if [ "$1" -eq "0" ] ; then
    # package removal
    %{_bindir}/yum-config-manager --enable base
    %{_bindir}/yum-config-manager --enable updates
    %{_bindir}/yum-config-manager --enable extras
fi


%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/yum.repos.d/pcentos.repo

%changelog
* Wed Sep 25 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
