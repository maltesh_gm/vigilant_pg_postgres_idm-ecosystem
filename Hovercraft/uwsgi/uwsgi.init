#!/bin/sh
# ======================================================
#  uwsgi - Starts the uwsgi
# ======================================================
#
# :Usage: /etc/init.d/uwsgi {start|stop|restart|reload|status}
# :Configuration file: /etc/default/uwsgi
#

### BEGIN INIT INFO
# Provides:          uwsgi
# Required-Start:    $network $local_fs $remote_fs
# Required-Stop:     $network $local_fs $remote_fs
# Default-Start:     2 3 5
# Default-Stop:      0 1 6
# chkconfig: 235 99 10
# Short-Description: uwsgi daemon
### END INIT INFO

# Source the RHEL/centos init functions
. /etc/init.d/functions

PID_FILE=/var/run/uwsgi.pid
DAEMON=/usr/bin/uwsgi
DAEMON_OPTS="--ini /etc/default/uwsgi"
prog="uwsgi"
desc="uwsgi daemon"
RETVAL=0

test -x $DAEMON || exit 0

# Include uwsgi defaults if available
#if [ -f /etc/default/uwsgi ] ; then
#    . /etc/default/uwsgi
#fi

start() {
    echo -n $"Starting $prog: "

    if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        echo $prog already running: $PID
        exit 2;
    else
        #daemon $DAEMON $DAEMON_OPTS &
        $DAEMON $DAEMON_OPTS
        RETVAL=$?
        sleep 2
        # ps ax | grep -m 1 uwsgi| awk '{print $1;}' > $PID_FILE
        [ $RETVAL -eq 0 ] && success || failure
        echo
        [ $RETVAL -eq 0 ] && touch /var/lock/subsys/$prog
        return $RETVAL
    fi
}

stop() {
    echo -n $"Shutting down $prog: "
    if [ -f $PID_FILE ]; then
        $DAEMON --stop $PID_FILE
        RETVAL=$?
        sleep 2
        if [ $RETVAL -eq 0 ]; then
            rm -f $PID_FILE
            rm -f /var/lock/subsys/$prog
            success
            echo
            return $RETVAL
        else
            failure
            echo
        fi
    else
        echo $prog not running
        success
        echo
    fi
}

status() {
    if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        echo $prog already running: $PID
        return $RETVAL
    fi
}

case "$1" in
    start)
      start
      ;;
    stop)
      stop
      ;;
    restart|reload)
      stop
          sleep 5
      start
      RETVAL=$?
      ;;
    status)
      status
      ;;
    *)
      echo $"Usage: $0 {start|stop|restart|reload|status}"
      RETVAL=1
esac

exit $RETVAL