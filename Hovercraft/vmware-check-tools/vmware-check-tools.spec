# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      vmware-check-tools
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   vmware-check-tools provides script to reconfigure vmware tools after a kernel is updated

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  shadow-utils
Requires:  initscripts
# This is for /sbin/service
Requires(preun):  initscripts
Requires(postun): initscripts
Provides:  vmware-check-tools

%description
vmware-check-tools provides script to reconfigure vmware tools after a kernel is updated.
This is setup to run on rc3

%prep
%setup -q

%build
echo OK


%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_initrddir}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/vmware-check-tools %{buildroot}%{_initrddir}/


%clean
%{__rm} -rf %{buildroot}


%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    %{__ln_s} %{_initrddir}/vmware-check-tools %{_sysconfdir}/rc.d/rc3.d/S09vmware-check-tools
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/unlink %{_sysconfdir}/rc.d/rc3.d/S09vmware-check-tools
fi


%files
%defattr(-,root,root,-)
%attr(0755,root,root) %{_initrddir}/vmware-check-tools


%changelog
* Fri Apr 23 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
