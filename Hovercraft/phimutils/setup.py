from distutils.core import setup
import os
phiversion = os.environ['phiversion']
setup(name='phimutils',
      version=phiversion,
      description='Philips Monitoring Utilities',
      author='Leonardo Ruiz',
      author_email='leonardo.ruiz@philips.com',
      #py_modules=['phimutils.timestamp'],
      packages=['phimutils']
      )
