
LOG_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
LOG_FORMAT = '%(asctime)s.%(msecs).06dZ - %(process)d - %(name)s - %(levelname)s - %(message)s'
