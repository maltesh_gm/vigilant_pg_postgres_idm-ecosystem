from time import time

PASSIVE_SERVICE_STRING = "[%(ttime)i] PROCESS_SERVICE_CHECK_RESULT;%(host)s;%(service)s;%(state)d;%(output)s"
PASSIVE_HOST_STRING = "[%(ttime)i] PROCESS_HOST_CHECK_RESULT;%(host)s;%(state)d;%(output)s"
STATE_MAP = {'OK': 0, 'WARNING': 1, 'CRITICAL': 2, 'UNKNOWN': 3}


def service_string(host, service, state, output, ttime=None):
    """Generates passive service check result string
    state - integer 0 to 3 or 'OK', 'WARNING', 'CRITICAL', 'UNKNOWN'
    ttime - float
    """
    ttime = get_ttime(ttime)
    state = get_state(state)
    return PASSIVE_SERVICE_STRING % locals()


def host_string(host, state, output, ttime=None):
    """Generates passive host check result string
    state - integer 0 to 3 or 'OK', 'WARNING', 'CRITICAL', 'UNKNOWN'
    ttime - float"""
    ttime = get_ttime(ttime)
    state = get_state(state)
    return PASSIVE_HOST_STRING % locals()


def get_ttime(ttime):
    if not ttime:
        ttime = time()
    else:
        ttime = float(ttime)
    return ttime


def get_state(arg):
    try:
        result = int(arg)
    except ValueError:
        result = STATE_MAP.get(arg.upper(), 4)
    if 0 <= result <= 3:
        return result
    else:
        raise ValueError("Invalid value for state: %s" % arg)