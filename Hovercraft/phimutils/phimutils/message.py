import json
import base64
import zlib
import copy
from phimutils.timestamp import iso_timestamp, dt_fromiso, iso_datetime
from phimutils.perfdata import perfdata2dict


STATES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


class Message(object):
    ATTRIBUTES = ['hostname', 'hostaddress', 'service', 'type', 'state', 'perfdata']
    def __init__(self, siteid, hostname='', type='',
                 hostaddress='', service='', state='', perfdata='', payload=None,
                 epoch_time=None, datetime=None, timestamp=None,
                 compress_payload=False, encode_payload=False, target_keys=None):
        # timestamp is expected as an iso format string
        self.siteid = siteid
        self.hostname = hostname
        self.hostaddress = hostaddress
        self.service = service
        self.type = type.upper() if type else ''
        state = state.upper() if state else ''
        self.state = state if not state or state in STATES else STATES[3]
        self.perfdata = perfdata
        self.payload = payload
        self.compress_payload = compress_payload
        self.encode_payload = encode_payload
        self.target_keys = [] if target_keys is None else target_keys  # list of keys that have content to be processed
        self._epoch_time = epoch_time
        self._input_datetime = datetime
        self._datetime = None
        self._input_timestamp = timestamp
        self._timestamp = None

    @classmethod
    def from_dict(cls, msg_dict):
        new_msg_dict = copy.deepcopy(msg_dict)
        cls.unpack_dict(new_msg_dict)
        perfdata = new_msg_dict.pop('perfdata', [])
        new_msg = cls(**new_msg_dict)
        new_msg._perfdata = perfdata
        return new_msg

    @classmethod
    def unpack_dict(cls, msg_dict):
        if 'payload' in msg_dict:
            compress = msg_dict.get('compress_payload')
            encode = msg_dict.get('encode_payload')
            target_keys = msg_dict.get('target_keys')
            if target_keys:
                for target in target_keys:
                    msg_dict['payload'][target] = cls.unpack(msg_dict['payload'][target], compress, encode)
            else:
                msg_dict['payload'] = cls.unpack(msg_dict['payload'], compress, encode)
        return msg_dict

    @staticmethod
    def unpack(content, compress, encode):
        if compress or encode:
            content = base64.b64decode(content)
            if compress:
                content = zlib.decompress(content)
        return content

    @classmethod
    def from_json(cls, msg_json):
        # when importing from json timestamp is expected in iso string format
        return cls.from_dict(json.loads(msg_json))

    @property
    def perfdata(self):
        return self._perfdata

    @perfdata.setter
    def perfdata(self, data):
        self._perfdata = perfdata2dict(data)

    def set_time_data(self):
        # precedence in order is: timestamp, epoch_time, datetime
        if self._input_timestamp:
            self._timestamp = self._input_timestamp
        elif not self._epoch_time and self._input_datetime:
            self._timestamp = iso_datetime(self._input_datetime)
        else:
            # if epoch_time is none, the current timestamp will be returned
            self._timestamp = iso_timestamp(self._epoch_time)
        # create the datetime object, mostly to validate the timestamp
        self._datetime = dt_fromiso(self._timestamp)

    @property
    def timestamp(self):
        if not self._timestamp:
            self.set_time_data()
        return self._timestamp

    @property
    def datetime(self):
        if self.timestamp:
            return self._datetime

    def pack(self, content):
        if self.compress_payload or self.encode_payload:
            if self.compress_payload:
                content = zlib.compress(content)
            content = base64.b64encode(content)
        return content

    def build_payload(self):
        payload_result = {}
        if not self.payload:
            return payload_result
        if self.target_keys:
            # if target_keys is passed, payload must be a dict
            payload_result['payload'] = copy.deepcopy(self.payload)
            for content in self.target_keys:
                payload_result['payload'][content] = self.pack(payload_result['payload'][content])
            payload_result['target_keys'] = self.target_keys
        else:
            payload_result['payload'] = self.pack(self.payload)
        if self.compress_payload:
            payload_result['compress_payload'] = True
        elif self.encode_payload:
            payload_result['encode_payload'] = True
        return payload_result

    def to_dict(self):
        items = ((attr, getattr(self, attr)) for attr in self.ATTRIBUTES if getattr(self, attr))
        properties = dict(items, siteid=self.siteid, timestamp=self.timestamp)
        properties.update(self.build_payload())
        return properties

    def to_json(self):
        return json.dumps(self.to_dict())


#msg = Message(siteid='xxxxx', hostname='somehost', type='error', target_keys=['sock'], payload={'sock': 'b', 'some': {'thing': [1,2,3]}}, encode_payload=True, compress_payload=True)
#msg = Message(siteid='xxxxx', hostname='somehost', type='error', payload='something here')
#print msg.to_json()
#msgre = Message.from_json(msg.to_json())
#print msgre.to_json()
