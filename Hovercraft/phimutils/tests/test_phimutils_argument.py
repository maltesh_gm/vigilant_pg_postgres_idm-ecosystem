import unittest
from mock import MagicMock, patch


class FakeArgumentTypeError(Exception):
    pass


class PhimutilsArgumentTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_json = MagicMock(name='json')
        modules = {
            'argparse': self.mock_argparse,
            'json': self.mock_json,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import argument
        self.module = argument
        self.module.argparse.ArgumentTypeError = FakeArgumentTypeError

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_positive_int(self):
        self.assertEqual(self.module.positive_int(3), 3)

    def test_positive_int_zero(self):
        self.assertRaises(self.module.argparse.ArgumentTypeError, self.module.positive_int, 0)

    def test_from_json(self):
        self.assertEqual(self.module.from_json('{"key1": "val1"}'), self.module.json.loads.return_value)
        self.module.json.loads.assert_called_once_with('{"key1": "val1"}')

    def test_from_json_falsey_in(self):
        self.assertEqual(self.module.from_json(None), None)

    def test_from_json_invalid(self):
        self.module.json.loads.side_effect = ValueError
        self.assertRaises(self.module.argparse.ArgumentTypeError, self.module.from_json, 'NOT JSON')
