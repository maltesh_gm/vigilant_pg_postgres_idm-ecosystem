import unittest
from mock import patch
from datetime import datetime
import dateutil.parser


class PhimutilsMessageTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import message
        self.module = message

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_message_timestamp(self):
        msg = self.module.Message(
            siteid='sit01', hostname='idmhost01.isyntax.net', type='error', timestamp='2014-07-16T17:57:43.061681Z'
        )
        self.assertEqual(
            msg.to_dict(),
            {
                'siteid': 'sit01',
                'timestamp': '2014-07-16T17:57:43.061681Z',
                'hostname': 'idmhost01.isyntax.net',
                'type': 'ERROR'
            }
        )

    @patch('phimutils.message.iso_timestamp')
    def test_message_no_date(self, mock_iso_timestamp):
        mock_iso_timestamp.return_value = '2013-07-16T17:57:43.061681Z'
        msg = self.module.Message(siteid='sit01', hostname='idmhost01.isyntax.net', type='error')
        self.assertEqual(
            msg.to_dict(),
            {
                'siteid': 'sit01',
                'timestamp': '2013-07-16T17:57:43.061681Z',
                'hostname': 'idmhost01.isyntax.net',
                'type': 'ERROR'
            }
        )

    def test_message_epoch_date(self):
        msg = self.module.Message(
            siteid='sit01', hostname='idmhost01.isyntax.net', type='error', epoch_time='1405541043'
        )
        self.assertEqual(
            msg.to_dict(),
            {
                'siteid': 'sit01',
                'timestamp': '2014-07-16T20:04:03.000000Z',
                'hostname': 'idmhost01.isyntax.net',
                'type': 'ERROR'
            }
        )

    def test_message_all_date(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            epoch_time='1405541043',
            timestamp='2014-07-16T17:57:43.061681Z',
            datetime=datetime(year=2011, month=02, day=23)
        )
        self.assertEqual(
            msg.to_dict(),
            {
                'siteid': 'sit01',
                'timestamp': '2014-07-16T17:57:43.061681Z',
                'hostname': 'idmhost01.isyntax.net',
                'type': 'ERROR'
            }
        )

    def test_message_all_args(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            hostaddress='127.0.0.1',
            service='all_service_name',
            state='ok',
            perfdata='x=3',
            payload={
                'to': 'someone',
                'from': 'someone else',
                'message': 'this message should compress',
                'app': 'also compressed'
            },
            compress_payload=True,
            encode_payload=True,
            target_keys=['message', 'app'],
            epoch_time='1405541043',
            timestamp='2014-07-16T17:57:43.061681Z',
            datetime=datetime(year=2011, month=02, day=23)
        )
        msg_dict = msg.to_dict()
        self.assertEqual(
            msg_dict,
            {
                'state': 'OK',
                'compress_payload': True,
                'service': 'all_service_name',
                'target_keys': ['message', 'app'],
                'timestamp': '2014-07-16T17:57:43.061681Z',
                'hostname': 'idmhost01.isyntax.net',
                'perfdata': [{'value': 3.0, 'label': 'x'}],
                'siteid': 'sit01',
                'hostaddress': '127.0.0.1',
                'type': 'ERROR',
                'payload': {
                    'to': 'someone',
                    'message': 'eJwrycgsVshNLS5OTE9VKM7IL81JUUjOzy0oAgoBAJ0aCvk=',
                    'from': 'someone else',
                    'app': 'eJxLzCnOV0jOzy0oSi0uTk0BAC9GBgU='
                }
            }
        )

    # asert that an equivalent message can be built from the output
    def test_message_recompose(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            hostaddress='127.0.0.1',
            service='all_service_name',
            state='ok',
            perfdata='x=3',
            payload={
                'to': 'someone',
                'from': 'someone else',
                'message': 'this message should compress',
                'app': 'also compressed'
            },
            compress_payload=True,
            encode_payload=True,
            target_keys=['message', 'app'],
            epoch_time='1405541043',
            timestamp='2014-07-16T17:57:43.061681Z',
            datetime=datetime(year=2011, month=02, day=23)
        )
        msg_dict = msg.to_dict()
        mm = self.module.Message.from_dict(msg_dict)
        self.assertEqual(mm.to_dict(), msg_dict)

    def test_message_recompose_string_payload(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            hostaddress='127.0.0.1',
            service='all_service_name',
            state='ok',
            perfdata='x=3',
            payload='this message should compress',
            compress_payload=True,
            timestamp='2014-07-16T17:57:43.061681Z'
        )
        msg_dict = msg.to_dict()
        mm = self.module.Message.from_dict(msg_dict)
        self.assertEqual(mm.to_dict(), msg_dict)

    def test_message_recompose_string_payload_encode(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            hostaddress='127.0.0.1',
            service='all_service_name',
            state='ok',
            perfdata='x=3',
            payload='this message should compress',
            encode_payload=True,
            timestamp='2014-07-16T17:57:43.061681Z'
        )
        msg_dict = msg.to_dict()
        mm = self.module.Message.from_dict(msg_dict)
        self.assertEqual(mm.to_dict(), msg_dict)

    # dictionary reference recompose
    def test_message_unpack_dict(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            hostaddress='127.0.0.1',
            service='all_service_name',
            state='ok',
            perfdata='x=3',
            payload={
                'to': 'someone',
                'from': 'someone else',
                'message': 'this message should compress',
                'app': 'also compressed'
            },
            compress_payload=True,
            encode_payload=True,
            target_keys=['message', 'app'],
            epoch_time='1405541043',
            timestamp='2014-07-16T17:57:43.061681Z',
            datetime=datetime(year=2011, month=02, day=23)
        )
        msg_dict = msg.to_dict()
        self.module.Message.unpack_dict(msg_dict)
        self.assertEqual(
            msg_dict,
            {
                'state': 'OK',
                'compress_payload': True,
                'service': 'all_service_name',
                'target_keys': ['message', 'app'],
                'timestamp': '2014-07-16T17:57:43.061681Z',
                'hostname': 'idmhost01.isyntax.net',
                'perfdata': [{'value': 3.0, 'label': 'x'}],
                'siteid': 'sit01',
                'hostaddress': '127.0.0.1',
                'type': 'ERROR',
                'payload': {
                    'to': 'someone',
                    'message': 'this message should compress',
                    'from': 'someone else',
                    'app': 'also compressed'
                }
            }
        )

    def test_message_compress_payload(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            hostaddress='127.0.0.1',
            service='all_service_name',
            state='ok',
            perfdata='x=3',
            payload='this message should compress',
            compress_payload=True,
            timestamp='2014-07-16T17:57:43.061681Z'
        )
        self.assertEqual(
            msg.to_dict(),
            {
                'state': 'OK',
                'perfdata': [{'value': 3.0, 'label': 'x'}],
                'compress_payload': True,
                'siteid': 'sit01',
                'hostaddress': '127.0.0.1',
                'service': 'all_service_name',
                'timestamp': '2014-07-16T17:57:43.061681Z',
                'hostname': 'idmhost01.isyntax.net',
                'type': 'ERROR',
                'payload': 'eJwrycgsVshNLS5OTE9VKM7IL81JUUjOzy0oAgoBAJ0aCvk='
            }
        )

    def test_message_encode_payload(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            hostaddress='127.0.0.1',
            service='all_service_name',
            state='ok',
            perfdata='x=3',
            payload='this message should encode',
            encode_payload=True,
            timestamp='2014-07-16T17:57:43.061681Z'
        )
        self.assertEqual(
            msg.to_dict(),
            {
                'state': 'OK',
                'perfdata': [{'value': 3.0, 'label': 'x'}],
                'encode_payload': True,
                'siteid': 'sit01',
                'hostaddress': '127.0.0.1',
                'service': 'all_service_name',
                'timestamp': '2014-07-16T17:57:43.061681Z',
                'hostname': 'idmhost01.isyntax.net',
                'type': 'ERROR',
                'payload': 'dGhpcyBtZXNzYWdlIHNob3VsZCBlbmNvZGU='
            }
        )

    def test_message_string_payload_target_keys_error(self):
        msg = self.module.Message(
            siteid='sit01',
            hostname='idmhost01.isyntax.net',
            type='error',
            hostaddress='127.0.0.1',
            service='all_service_name',
            state='ok',
            perfdata='x=3',
            payload='this message should compress',
            target_keys=['message'],
            compress_payload=True,
            timestamp='2014-07-16T17:57:43.061681Z'
        )
        self.assertRaises(TypeError, msg.to_dict)

    def test_message_datetime_using_timestamp(self):
        msg = self.module.Message(
            siteid='sit01', hostname='idmhost01.isyntax.net', timestamp='2014-07-16T17:57:43.061681Z'
        )
        self.assertEqual(msg.datetime, dateutil.parser.parse('2014-07-16T17:57:43.061681Z').replace(tzinfo=None))

    def test_message_datetime_using_datetime(self):
        date_time = dateutil.parser.parse('2014-07-16T17:57:43.061681Z').replace(tzinfo=None)
        msg = self.module.Message(siteid='sit01', hostname='idmhost01.isyntax.net', datetime=date_time)
        self.assertEqual(msg.datetime, date_time)


if __name__ == '__main__':
    unittest.main()
