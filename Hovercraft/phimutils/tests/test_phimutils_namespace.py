import unittest
from mock import patch


class PhimutilsNamespaceTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import namespace
        self.module = namespace

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_cleanup_key_nothing_to_clean(self):
        self.assertEqual(self.module.cleanup_key('nothing to clean'), 'nothing to clean')

    def test_cleanup_key_dots(self):
        self.assertEqual(self.module.cleanup_key('clean.this.up'), 'clean_this_up')

    def test_cleanup_key_dots_and_spaces(self):
        self.assertEqual(self.module.cleanup_key('further. clean this.up'), 'further_ clean this_up')

    def test_cleanup_key_some_cleanup(self):
        self.assertEqual(self.module.cleanup_key('furt"he;r. cl:ean th$i!s|.up'), 'further_ clean this_up')

    def test_cleanup_key_slashes(self):
        self.assertEqual(
            self.module.cleanup_key('OS__Microsoft__Windows__Subfolder__S:/Stentor/input file age\\'),
            'OS__Microsoft__Windows__Subfolder__S/Stentor/input file age\\'
        )

    def test_information_id_simple(self):
        self.assertEqual(
            self.module.information_id('Version', 'Product__Fake__Test'),
            'Product__Fake__Test__Information__Version'
        )

    def test_information_id_short(self):
        self.assertEqual(self.module.information_id('Version', 'Product__Fake'), 'Product__Fake__Information__Version')

    def test_information_id_cleanup(self):
        self.assertEqual(
            self.module.information_id('IP.address.1', 'Network__Secret Load Balancer__X9000__Board 1 __Address '),
            'Network__Secret Load Balancer__X9000__Information__IP_address_1'
        )

    def test_metric_value_simple_empty_value(self):
        self.assertEqual(
            self.module.metric_value(
                {"label": "", "value": "", "uom": ""}, 'OS__MS__Windows 98__Hard Drive Space__Total'
            ),
            ('OS__MS__Windows 98__Hard Drive Space__Total__', '')
        )

    def test_metric_value_simple_cleanup(self):
        self.assertEqual(
            self.module.metric_value(
                {"label": "some.thing", "value": "800", "uom": "mt"},
                'OS__MS__Windows 98__Hard Drive Space__Total'
            ),
            ('OS__MS__Windows 98__Hard Drive Space__Total__some_thing', '800mt')
        )

    def test_metric_value_missing_label(self):
        self.assertRaises(
            KeyError,
            self.module.metric_value,
            {"value": "800", "uom": "mt"},
            'OS__MS__Windows 98__Hard Drive Space__Total'
        )

    def test_metric_value_missing_value(self):
        self.assertRaises(
            KeyError,
            self.module.metric_value,
            {"label": "some.thing", "uom": "mt"},
            'OS__MS__Windows 98__Hard Drive Space__Total'
        )

    def test_metric_value_longer(self):
        self.assertEqual(
            self.module.metric_value(
                {"label": "some.thing", "value": "800"},
                'OS__MS__Windows 98__Hard Drive Space__Total'
            ),
            ('OS__MS__Windows 98__Hard Drive Space__Total__some_thing', '800')
        )

    def test_ns_attach_one(self):
        self.assertEqual(self.module.ns_attach('OS', 'MS'), 'OS__MS')

    def test_ns_attach_two(self):
        self.assertEqual(self.module.ns_attach('OS__Linux', 'CentOS'), 'OS__Linux__CentOS')

    def test_list2ns_simple(self):
        self.assertEqual(self.module.list2ns(['OS', 'Linux', 'CentOS']), 'OS__Linux__CentOS')

    def test_parse_ns(self):
        parsed = self.module.parse_ns('OS__Linux__Secret_Linux__RAM__Status')
        self.assertEqual('OS', parsed.category)
        self.assertEqual('Linux', parsed.vendor)
        self.assertEqual('Secret_Linux', parsed.object)
        self.assertEqual('RAM', parsed.attribute)
        self.assertEqual('Status', parsed.dimension)

    def test_parse_ns_defaults(self):
        parsed = self.module.parse_ns('OS__Linux__CentOS')
        self.assertEqual('OS', parsed.category)
        self.assertEqual('Linux', parsed.vendor)
        self.assertEqual('CentOS', parsed.object)
        self.assertEqual(None, parsed.attribute)
        self.assertEqual(None, parsed.dimension)

    def test_ns2dict(self):
        self.assertEqual(
            self.module.ns2dict('OS__Linux__CentOS__RAM__Status'),
            {'category': 'OS', 'attribute': 'RAM', 'object': 'CentOS', 'vendor': 'Linux', 'dimension': 'Status'}
        )


if __name__ == '__main__':
    unittest.main()
