=================
Phimutils decider
=================
Decider Class
-------------
The decider class is to be used to determine what things apply to a certain item based on the pertaining siteid and
namespace.

A decider is loaded with rules, which are composed of siteids, namespace keys and the results wanted for items matching
the parameters.
Rules with more specific descriptions will take precedence over those more generic.

Results that start with "!"indicate that they are to be removed from the list if present

Siteids are usually a 5 character alphanumeric string. Eg: XYZ00
Namespace keys are a string with a special divider to represent a hierarchical set of categories describing an object.
Eg1: Administrative__Philips__Localhost__Information__IPAddress
Eg2: OS__CentOS__Linux__Network__PING

For the purpose of the decider a shorter key represents a more general categorization, for example OS__CentOS__Linux is
more general than OS__CentOS__Linux__Network.

For a rule an empty list of siteid or namespaces will make it apply to all.

The rule: ::

    {
        "sites": [],
        "namespaces": [],
        "result": ["backoffice", "hb_email"]
    }

will apply to all sites all namespaces

A more complicated set of rules, such as: ::

    [
        {
            "sites": [],
            "namespaces": [],
            "result": ["backoffice", "hb_email"]
        },
        {
            "sites": ["xyz01", "sed01"],
            "namespaces": [],
            "result": ["!hb_email"]
        },
        {
            "sites": ["xyz01"],
            "namespaces": ["OS__CentOS__Linux", "OS__Ubuntu__Linux"],
            "result": ["linux_experts"]
        },
        {
            "sites": ["xyz01"],
            "namespaces": ["OS__CentOS__Linux__Network__PING"],
            "result": ["!linux_experts"]
        }
    ]

For a message going through the rule set with the parameters: siteid=abc00 and key=OS__Microsoft__Windows__CPU__Usage
only the first rule would be matched, resulting in: ["backoffice", "hb_email"]

For a message going through the rule set with the parameters: siteid=xyz01 and key=OS__Microsoft__Windows__CPU__Usage
the first two rules would apply, resulting in: ["backoffice"]

For a message going through the rule set with the parameters: siteid=xyz01 and key=OS__CentOS__Linux__Processes__Total
the first three rules would apply, resulting in: ["backoffice", "linux_experts"]

For a message going through the rule set with the parameters: siteid=xyz01 and key=OS__CentOS__Linux__Network__PING
all would apply, resulting in: ["backoffice"]