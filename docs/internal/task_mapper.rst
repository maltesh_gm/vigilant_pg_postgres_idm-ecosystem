===========
Task Mapper
===========

Task mapper is a consumer step added to celery, it adds the ability to consume from queues and push the messages
consumed to a celery task.

Celery tasks to be used by task mapper should take a single argument, which will come from the message received from
the queue.

The queue to task configuration is a dictionary: QUEUE_TASK_MAP in tbconfig.
The keys are queue names, the values are the task names. This should be a 1 to 1 mapping.

Queues listed in QUEUE_TASK_MAP need to be present in EXCHANGE_QUEUE_MAP.