Intellespace Database Discovery Overview
========================================

Intellespace Database Discovery is a process through which information from the field is used to generate configuration to monitor and manage
Intellespace Database nodes.

Discovery Endpoints
-------------------

Endpoints are located in the lhost.yml file for a particular site. The ``address`` and ``scanner`` keys are required for
all endpoints. Other keys may be required depending on the scanner and type of system. Most systems will require
the ``username`` and ``password`` for access.

For Example : ::

    ---
    fqdn : idm00idm01.stidm00.isyntax.net
    siteid : IDM00
    philips_subnets : []
    dns_nameservers:
      - 10.6.88.52
      - 10.6.88.57

    endpoints:
      - address: 167.81.183.99
        scanner: ISP
        tags: [Prod, Primary]
        username: tester
        password: CryptThis


* Address - This is the IP or Hostname that stores configuration of the product to scan.
* Scanner - What type of scanner to use on this. Current valid values are ``ISP``, ``I4``, ``LN``, ``vCenter``,
  ``AnalyticsPublisher``, ``AdvancedWorkflowServices``, ``Windows``, and ``Linux``
* Tags - This is a list of properties we wish to propagate up to the configuration generator. In the case of ISP
  scanners it used to differentiate Production from Test and BCS systems.
* Username - Username of the product scanner to retrieve configuration
* Password - Password of the username on the product scanner system.

Values may be passed as a dictionary with a "$resource" key, this will use the value of the specified resource from
the shinken configuration. Decryption is used on values, as when the cryptresource module is used.

The individual endpoint dictionary is passed to the Product scanner, the product scanner decides which keys to pass to
the host scanners used.

Product Scanners
----------------

The purpose of these items is to take an endpoint and scan the given product configuration for all associated hosts.
Only methods involved with configuration retrieval and parsing should be here.

This set of classes are passed on the Template pattern with the ``ISPProductScanner`` as the base class. In general, the
methods that need to be over-ridden are ``get_hosts`` and ``get_hostname``. The ``get_hosts`` method should return an
iterable of host objects to pass to the ``HostScanner``, host objects are not strictly defined they may simply be IPs or
Hostnames. The ``get_hostname`` method should take a host object as parameter and return a hostname for it. In the case
of single machine products, there is no need to override ``get_hosts`` as the base method returns the configured
endpoint IP as the only host.

Basically ISPDBConfiguration is main Class in Product Scanner which is used for DB node discovery.

Host list which is retrived look like : ::

<Message>
    <ListNonCoreNodesResponse>
        <ArrayOfNode
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <Node>
                <HostName>nts03db1.NTS03.iSyntax.net</HostName>
                <NodeType>Database</NodeType>
                <Location>Main Location</Location>
                <FilePath>
                    <string>s:\\Mount</string>
                </FilePath>
            </Node>
            <Node>
                <HostName>nts03hl7.nts03.isyntax.net</HostName>
                <NodeType>HL7</NodeType>
                <Location>Main Location</Location>
            </Node>
            <Node>
                <HostName>NTS03MG1.NTS03.iSyntax.net</HostName>
                <NodeType>Database</NodeType>
                <Location>Main Location</Location>
                <FilePath>
                    <string>s:\\Mount</string>
                </FilePath>
            </Node>
        </ArrayOfNode>
    </ListNonCoreNodesResponse>
</Message>



Host Scanners
-------------

The purpose of host scanners is to retrieve host specific items and place them in the facts collection format.

In the fact format, information is split up into modules, a module holds information specific to a particular part of
the host being scanned. For example, information about ISP would be found under the ISP module. Windows information for
the host would be found under the Windows module.

Below is the properties which is retrieved for DB nodes : ::

'module_type', 'identifier', 'version', 'input_folder', 'noncore_node'


Main properties for DB nodes : ::

 - Input folder : which will contain path to monitor
 - module type : which will tell which type of node its is.
 - noncore node : for all DB node it will return DB for else it will be null.


 


