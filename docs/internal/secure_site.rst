=====================
Security Enhanced Neb
=====================

The secure Neb build process has not been fully merged into the regular Neb node due to some differences not yet fully
accounted for on production sites.

Because of this a procedure to create a secure neb node is outlined in this document.


Differences
===========

#. The secure emp uses a heavily commented out ansible-lockdown role from ansible galaxy targeted to address the STIGS:

   - V-38452
   - V-38453
   - V-38454
   - V-38465
   - V-38469
   - V-38472
   - V-38519
   - V-38518
   - V-38623
   - V-38643
   - V-38653
   - V-38663

#. Enables and configures iptables.

#. Enables and configures audit.

#. enables and configures selinux.

#. schedules aide. (Install baked into sec ova).

#. Sets up lrepos GPG.


Site Parameters
===============

The following site parameters are to be used to implement features:

sshd_uptight: True

This disables root login via ssh as well ass adding timeout to sessions.

use_https: True

This changes all backoffice communication to be over https.


Procedure
=========

- Create virtual machine from secure neb node OVA, located
  at: `\\\\usdsfosfc1msna1\\EII_Eng_Share\\SecureNeb\\neb-1.7-v9i.ova`

- Create a site in IBC without an external for common. Copy the contents of the actual common folder (in the root of
  IBC). Replace common/ansible with EMP/ansible directory from the secure-develop git branch.

- Add secure parameters to lhost.yml for site

- Create 'issue' file in same directory as lhost.yml with the appropriate banner.

- Initialize neb node.
