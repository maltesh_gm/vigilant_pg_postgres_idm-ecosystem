==========================
Nagios Configuration Rules
==========================

Overview
--------

Nagios configuration rules provides a way to generate nagios configuration objects from facts discovered from the field,
facts are stored in a specific format, this is used to allow evaluating rules to set attributes on the output.

The rules are defined using YAML format, a list of rule sets is the top layer object in the document. Every item in the
list is a single item dictionary, where the key is a key required to be in present in the data evaluated, the value in
this dictionary is a list of rules to apply to items which have the required key.

Evaluation is done in the order the rules are present in the YAML configuration, top to bottom. When multiple rules
for one attribute match the latest one to match will be the one set in the output. There is one exception to this, the
"hostgroup" attribute has a provision to allow for a list to be generated, rules with output beginning with "+" for this
attribute will be added to the existing elements in that attribute.

Rules in the YAML configuration are defined by a dictionary. This dictionary must have a key "attribute" with the value
of the desired nagios configuration attribute. There may optionally be a "where" key with the value being a jinja2
expression to be evaluated by the rule. The rest of the keys present in the dictionary depend on which of type of rule
is defined.


Rules
-----

Rules must have an "attribute" key, and may optionally have a "when" key.

attribute
  This is the attribute to be returned when evaluation is done.

when
  This is a jinja2 expression to evaluate based on the data passed at rule evaluation time.


Value Rule
^^^^^^^^^^

Value rules do an evaluation using the "value" key.

value
  A jinja2 evaluable expression, this may simply be a key expected to be present in the data being evaluated.

**Example**

Having the rule::

    attribute: _custom_key1
    value: module1.key1
    when: address.split('.')|first == '10'

Evaluating the data::

    {
        'address': '10.3.4.34',
        'module1': {
            'key1': 'special'
        }
    }

Will yield: (**_custom_key1** **special**)

If a literal value is desired to be the value, then it must be quoted, like in the following rule::

    attribute: use
    value: '"super"'

Evaluating any data, since "where" key is not present, with this rule will yield (**use** **super**)

Map Rule
^^^^^^^^

Map rules map a value to an output using a "source" key.

source
  A jinja2 expression, this may simply be a key expected to be present in the data being evaluated

default
  The default output if the source can not be mapped to anything in the map.

map
  A dictionary with keys being a possible result of evaluating "source" and the value the output.

Keys in hte map must be strings since the source will be evaluated into a string before mapping is done.

**Example**

Having the rule::

    attribute: hostgroups
    source: module1.make
    default: othergroup
    map:
      processor: procgroup
      capturer: capturegroup
    when: module1.key1 == 'special'

Evaluating the data::

    {
        'address': '10.3.4.34',
        'module1': {
            'key1': 'special',
            'make': 'processor'
        }
    }

Will yield: (**hostgroups** **procgroup**)

Evaluating the data::

    {
        'address': '10.3.4.34',
        'module1': {
            'key1': 'special',
            'make': 'nothing'
        }
    }

Will yield: (**hostgroups** **othergroup**)

Hostgroups
----------

The "hostgroups" attribute has special processing to allow for a list of hostgroups to be generated.

When a rule returns a string that does not start with "+" it works as other attributes, the attribute replaces any value
already present for that filed. However, if the string returned starts with "+" then the value after "+" will be joined
using a comma to any current value already present.

That is if:

#. Rule 1 returns (**hostgroups** **hostgroup1**)
#. Rule 2 returns (**hostgroups** **+hostgroup2**)

The resulting output will be (**hostgroups** **hostgroup1,hostgroup2**)

If the first matching output from a rule starts with "+", the "+" will be present in the output, this allows for normal
Nagios inheritance.

Rule Set
--------

A rule set is a YAML file to be parsed for rules to evaluate and produce resulting Nagios configuration.

The YAML file must be a list as the root object, each element in that list a single entry dictionary with the "required
key" as key and the value a list of rules.

For example::

    ---

    - module1:
        - attribute: _custom_key1
          value: module1.key1
          when: address.split('.')|first == '10'

        - attribute: hostgroups
          source: module1.make
          default: othergroup
          map:
            processor: procgroup
            capturer: capturegroup
          when: module1.key1 == 'special'

    - module2:
        - attribute: use
          value: '"secondgroup"'

Custom Jinja2 Tests
-------------------
The following jinja2 custom tests are added to enhance functionality.

re_match
^^^^^^^^

This test results in True if the item being evaluated matches the regular expression specified as argument.

**Example** ::

    'somestring' is re_match('\.*me')

Should result in True

YAML and Jinja2
---------------
There are some quirks to using YAML and Jinja2.

The python YAML parser will try to convert things that look like numbers into numbers, it is important to use quotation
around numbers. The YAML parser eliminates dashes between numbers if not quoted.

When using a literal where Jinja2 evaluation will be done this needs to be quoted, in cases where the literal is the
only thing in the evaluation, a way to do this is to use two different types of quotes as YAML parsing will remove the
outer set of quotes. Eg: '"literal-value"'
