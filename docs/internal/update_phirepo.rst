..  _update_phirepo:

Updating PHIRepo with New Third Party RPM Packages
==================================================

Overview
--------

This section describes the procedure to update PHIRepo with new third party rpm packages.

Steps for RPM Packages Obtained from Internet Mirror Repository
---------------------------------------------------------------

#. Update the appropriate package list under IDM Ecosystem->trunk->Hovercraft->repos->package_lists with the packages
   that are to be mirrored to PHIrepo.  The package name should be provided here without the version number.
   - Neb node packages get added to the neb_package_list.txt file.
   - Vigilant packages get added to the vigilant_package_list.txt file.
   - PyPi packages get added to the pypi_package_list.txt file.
#. To track the specific versions of the required packages, add them to the appropriate package list in the same
   location.  The full package name with the version number is required.
   - Neb node packages get added to the neb_packages.txt list.
   - Vigilant packages are added to the vigilant_packages.txt file.
#. Commit the file to source control and an IDM Ecosystem build is triggered.  A successful IDM Ecosystem build will
   then trigger a PHIRepo build.
#. Once the build is complete, a Repotrack build needs to be initiated.  Go to the Jenkins website and initiate a IDM
   Ecosystem Repotrack - el6-x86_64 build.  Repotrack will then initiate another PHIRepo build.
#. Verify the new rpm packages are available on the repository
   (`http://Repository_URL/phirepo/thirdparty <http://Repository_URL/phirepo/thirdparty>`_).

Steps for RPM Packages NOT Obtained from the Internet Mirror (Manual Update)
----------------------------------------------------------------------------

#. Obtain rpm packages from a trusted source.
#. Copy the rpm packages to the file share in the
   `third party area <file://usdsfosfc1msna1.code1.emi.philips.com/ComputeEnvironment/thirdparty>`_
#. Obtain the URL for the newly added packages.  Packages placed under the third party directory on the file share will
   appear under `http://sfo-ce-webhead/thirdparty <http://sfo-ce-webhead/thirdparty>`_
#. Add the URL for each new rpm package to the add_list.txt file found in source control.  This is found under
   IDM Ecosystem->trunk->Hovercraft->repos->package_lists.
#. Commit the add_list.txt file and this should initiate a PHIRepo build (PHIRepo in Jenkins).
#. Once the build is complete, a Repotrack build needs to be initiated.  Go to the Jenkins website and initiate a
   IDM Ecosystem Repotrack - el6-x86_64 build.  Repotrack will then initiate another PHIRepo build.
#. Verify the new rpm packages are available on the repository
   (`http://Repository_URL/phirepo/thirdparty <http://Repository_URL/phirepo/thirdparty>`_).
