check_equallogic
================

From this address:

http://www.claudiokuenzler.com/nagios-plugins/check_equallogic.php

Requirements
- The following shell commands must exist and be executable by your Nagios user: snmpwalk, awk, grep, wc, cut
- SNMP must be enabled on the Dell Equallogic device. If it is not already, enable it on the member.