WMI Monitoring
==============

WMI Monitoring is done mainly by the check_wmi_plus plugin, a thirdparty plugin written in perl.

WMI check commands using check_wmi_plus plugin are made to check the output of service
**OS__Microsoft__Windows__WMI__Status** of the host where the check is to be performed before attempting to run. This is
done because check_wmi_plus is expensive to run. When creating new sites it is possible for many nodes to have incorrect
parameters (username/password) configured which will result in many wmi checks executing and failing. Many failures
seems to be particularly expensive and potentially rendering the node inoperable.

The service OS__Microsoft__Windows__WMI__Status is doen by a different command, a lighter one written in python.

Having the dependency built into the command makes it possible to not have to define service dependencies for all WMI
related services, this would be particularly verbose in shinken where servicegroups do not seem to be considered in
dependency definitions.