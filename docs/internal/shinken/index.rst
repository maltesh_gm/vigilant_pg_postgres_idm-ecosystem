Shinken
=======

Contents:

.. toctree::
   :maxdepth: 2

   wmi_monitoring
   plugins/index
   eventhandlers/index
   modules
   credentials
