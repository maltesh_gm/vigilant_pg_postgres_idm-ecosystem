===============
Queue Transport
===============
On site node
------------
Queue transport is a capability to transport messages from the neb node RabbitMQ queues to backoffice RabbitMQ queues.

One celery task per queue to be transported is created, these tasks wrap the message adding the queue where it was
picked up from, and transport it to the backoffice to the endpoint configured under QUEUE_TRANSPORT_ENDPOINT.

The following tbconfig entries are relevant to transport on Neb nodes:
QUEUE_TRANSPORT_ENDPOINT, the endpoint where the messages are sent.
QUEUE_TRANSPORT_RETRIES, the number of retries to send the message before discarding it.
QUEUE_TRANSPORT_RETRY_DELAY, the time in seconds to wait before retrying.

Backoffice
----------
Gateway forwards the message to a celery task which uses TRANSPORT_MAP (a queue name to exchange dictionary)
to determine to which exchange to forward the message to, based on the queue it comes from.

Queues subscribing to the exchanges are not created by worker, application consuming from these exchanges is expected
to create queries and bind them to exchange.