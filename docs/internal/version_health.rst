========================================
Business Application Integration Support
========================================

The exact URL used must be communicated and incorporated into IDM in order for actual checking to occur.

Exposed Component Versioning
----------------------------

A HTTP GET request will be executed to for the URI. The return type shall be in the form of a JSON object. The following
will give an example of how the formatted return response is expected to be formatted.

Expected interface to return the following object
**http(s)://{fqdn}[:{port}]/{project}/version**::

    {
        "Applications": [
            {
                "Version": "1.0.0.0",
                "ApplicationName": "DynamicService.Service"
            },
            {
                "Version": "1.0.0.0",
                "ApplicationName": "DynamicService.FileService"
            },
            {
                "Version": "1.0.0.0",
                "ApplicationName": "DynamicService.ImageXChange"
            }
        ]
    }

------------

Exposed Component Health Status
-------------------------------

**http(s)://{fqdn}[:{port}]/{project}/health**::

    {
        "Applications": [
            {
                "Status": "OK",
                "Message": "All systems normal",
                "ApplicationName": "DynamicService.Service"
            },
            {
                "Status": "OK",
                "Message": "For a file service, I'm doing pretty good",
                "ApplicationName": "DynamicService.FileService"
            },
            {
                "Status": "UNKNOWN",
                "Message": "Current state unknown",
                "ApplicationName": "DynamicService.ImageXChange"
            }
        ]
    }

For the health key, the following strings are supported status responses

- OK
- WARNING
- CRITICAL
- UNKNOWN
