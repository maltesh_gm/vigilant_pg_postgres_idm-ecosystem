==========================================
IDM Ecosystem Onsite deployment within VCE
==========================================
Procedure
---------
1. Determine if site meets criteria for deployment (Reference IDM Ecosystem Design Specifications - Section 7)
  
2. Pre-requisite information needed
    - Available IP Address within the VCE network
    - Associated Subnet Mask and Default Gateway information
    - Assertion of SSH port (TCP 22) being open from datacenter to site.
    - Assertion of required ports TCP 80 and TCP 443 being open from site to datacenter.
    - SiteID (following the PRO4.08-01 Customer Site Naming Convention)
    - Philips instances acting as an internal DNS server.
    - Endpoint to send data to the Philips Datacenter
        - Customer not implementing an Outbound NAT towards Philips should use the Endpoint address as **63.147.62.11**
    - The ESXi version used at the site, 4.x and 5.0 are supported.

3. Copy the IDM Ecosystem onsite instance from the Release location. The file will be named neb-base-revN-vhX.ova. This
   is an appliance and will need to be installed into the VCE Cluster with enough capacity as listed in the above
   document reference.

Where X is the virtual hardware version to be used at the site, 7 corresponds to ESXi 4.x and 8 to 5.0.

4. Launch the vCenter
5. Deploy a OVA as per VMware documentation, `VMware ESX and vCenter Server <http://pubs.vmware.com/vsphere-4-esx-vcenter/index.jsp?topic=/com.vmware.vsphere.vmadmin.doc_41/vsp_vm_guide/working_with_ovf_templates/t_import_a_virtual_appliance.html>`_

- Name the new Virtual Machine as per the site naming conventions

6. Power on the Virtual Guest
7. Once the system is booted, log into the console ::

    Username: root
    Password: ph...r
    
8. Execute the initialization tool ::

    # /usr/lib/philips/initialization/initialization.py -i <ip address> -n <netmask> -g <default gateway> -d <dns1,dns2> -s <siteid> -e <endpoint>

- If successful a message will display on the screen stating **'OK - Posted'**
- Any other message will need further definition.

------------

Common issues
-------------
    - Network access to the Philips Endpoint is incorrect
    - Network ports needed to access the Philips endpoint are not open
        - See IDM Ecosystem Design Specifications for the necessary ports.
    - Site ID does not meet the PRO4.08-01 Customer Site Naming Convention
