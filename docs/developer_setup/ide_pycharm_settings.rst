..  _ide_pycharm_settings:

====================
IDE PyCharm Settings
====================

Remote Deployment
-----------------

#. Navigate to Tools --> Deployment --> Configuration

   The following screen is presented for the configurations

   .. image:: ../images/deployment_configuration_01.png

#. Change the following setting on the connection tab

   - Name: **dev-wks-01**
   - Type: **SFTP**
   - SFTP host: **[IP ADDRESS]**
   - Root Path: **/home/developer**
   - Username: **developer**
   - Password: **password**

#. Click **Test Connection** to verify the remote system finger print
#. Click **Yes**
#. Click **Apply**
#. Select the **Mappings** tab

   .. image:: ../images/deployment_configuration_02.png

#. Change the following settings

   - Local path: **/IDMEcosystem**
   - Deployment path on server: **/**

#. Click **Apply**
#. Click **Ok**

Remote Interpreter
------------------

#. Navigate to File --> Settings
#. Find **Project Interpreter** in the list on the Left side of the screen

   .. image:: ../images/remote_interpreter.png

#. In the **Project Interpreter** drop down list, select **Show all**
#. In the **Project Interpreter** screen, click the **+** on the right and select **Add Remote**
#. In the **Configure Remote Python Interpreter** screen, highlight *Deployment configuration*
#. Verify that in the **Deployment configuration** field that your remote system name is showing

   If it is not showing, then select the **SSH Credentials** and file in the appropriate fields.

#. Click **Ok**

   .. note:: PyCharm is now connecting to the remote system and loading its libraries and modules. There will be some
             background tasks that update skeletons, etc.

#. Once it is completed, your remote interpreter should be showing in the **Project Interpreters** screen
#. Click **Ok**
#. Click **Ok**

Customizations
--------------

Display of files that may have incorrect file endings is accomplished by changing the project **Inspections**

#. Navigate to File --> Settings
#. Find **Inspections** in the list on the Left side of the screen
#. On the right section of the screen, **Enable** *Portability issues* so that *Inconsistent line separators*
   is *Enabled*

   .. image:: ../images/inspections.png

#. Under **Code Style** select **General** in the list on the left side of the screen
#. Set Line separator (for new files) to *Unix and OS X (\\n)* and apply the settings

   .. image:: ../images/pycharm_codestyle_settings.png
