..  dev_ci_setup:

======================================
Developer Continuous Integration Setup
======================================

High level Overview
-------------------

.. image:: ../images/dev_ci_overview.png

Pipeline
--------

.. image:: ../images/dev_ci_pipeline.png

The CI Setup runs inside of Jenkins as a series of jobs. To push the big red button, click build on the head job

* CI DEV01 Repository
* CI DEV01 PDSC
* CI DEV01 Vigilant
* CI DEV01 Neb

Endpoints
---------

The following nodes are for the Dev CI environments

DEV01

::

    dev01rep01 : 172.16.4.21    Repository Node (phirepo and pypi)
    dev01scm01 : 172.16.4.22    Source Control Node (svn)
    dev01idm01 : 172.16.4.23    Vigilant Node
    dev01nlb01 : 172.16.4.24    Nginx Load Balancer
    dev01neb01 : 172.16.4.25    Neb Node

DEV02

::

    dev02rep01 : 172.16.4.26    Repository Node (phirepo and pypi)
    dev02scm01 : 172.16.4.27    Source Control Node (svn)
    dev02idm01 : 172.16.4.28    Vigilant Node
    dev02nlb01 : 172.16.4.29    Nginx Load Balancer
    dev02neb01 : 172.16.4.30    Neb Node



Cloning a New Environment
-------------------------

A few steps have to be performed for cloning new environments

* Revert a DEV0X environment to snapshot and leave the machine turned off
* Change the hostname
* Change the IP
* Insert the new host mappings
* Apply fix for cloned VM eth01 MAC issue in CentOS

::

    vi /etc/sysconfig/network
    vi /etc/sysconfig/network-scripts/ifcfg-eth0
    rm /etc/udev/rules.d/70-persistent-net.rules
    vi /etc/hosts/
    
    reboot


Add a new Vigilant inventory file to the megacity CI environment. 

Then the Following Jenkins jobs should be cloned

* CI DEV01 Vigilant
* CI DEV01 Repository
* CI DEV01 PDSC
* CI DEV01 Neb

Ensure that the Custom Workspaces and Shell Paths are set correctly

