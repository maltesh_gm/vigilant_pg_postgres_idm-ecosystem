..  _iecg:

==============================
iECG Integration
==============================

**HostScanner** - IECGHostScanner

**Hostgroups** - iecg-servers


.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - mssql checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "TMVIOProxyService", "Product__IntelliSpace__iECG__TraceMasterVueIOProxyService__Status", check_win_process!TMVIOProxyService.exe
   "TMVServiceManager", "Product__IntelliSpace__iECG__TraceMasterVueServiceManager__Status", check_win_process!TMVServiceManager.exe
   "TMVStatusManager", "Product__IntelliSpace__TraceMasterVueStatusManager__Status", check_win_process!TMVStatusManager.exe
