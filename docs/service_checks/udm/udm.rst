..  _udm:

==============================
UDM Services
==============================

**HostScanner** - UDMHostScanner

**Hostgroups** - udm-redis-server, udm-app-server, udm-prep-server, udm-strg-server

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Redis Server
 - UDM App Server
 - UDM prep Server
 - UDM Storage Server

.. csv-table:: **Redis Server**
   :header: "Service", "Namespace", "port", "Check Command"
   :widths: 20, 40, 20, 40

   "Redis", "Product__UDM__Redis__ConnectionResponse__Status", "6379", "check_redis!-T $REDIS_CONNTIME_WARN$,$REDIS_CONNTIME_CRIT$"
   "Redis", "Product__UDM__Redis__Memory__Utilization", "6379", "check_redis!--memory_utilization=$REDIS_MEMORY_WARN$,$REDIS_MEMORY_CRIT$ -M $REDIS_MAXMEMORY$"
   "Redis", "Product__UDM__Redis__Client_Connections__Count", "6379", "check_redis!--connected_clients=$REDIS_CONN_COUNT_WARN$,$REDIS_CONN_COUNT_CRIT$"


.. csv-table:: **UDM App Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Hybrid Storage Scheduler Service", "Product__UDM__HybridStorage__SchedulerService__Status", "check_win_process!Philips.UDM.HybridStorage.WindowsService.EXE"
   "UDM Monitor", "Product__UDM__Monitoring__Service__Status", "check_win_process!Philips.UDM.Monitor.EXE"
   "CILM Dataservice", "Product__UDM__CILM__Dataservice__Status", "check_win_process!Philips.CILM.DataService.Host.EXE"
   "CILM Scheduler", "Product__UDM__CILM__Scheduler__Status", "check_win_process!Philips.CILM.Prefetch.Scheduler.WindowsService.EXE"
   "Prefetch Message Transformer", "Product__UDM__PrefetchMessage__Transformer__Status", "check_win_process!Prefetch.MessageTransformLauncher.EXE"
   "Prefetch Manager","Product__UDM__Prefetch__Manager__Status", "check_win_process!Prefetch.ManagerLauncher.EXE"
   "Prefetch Replay Manager","Product__UDM__Prefetch__ReplayManager__Status", "check_win_process!MessagingFramework.ReplayManger.EXE"
   "Rule Engine","Product__UDM__RuleEngine__Service__Status", "check_win_process!Java.EXE -jar RuleEngine.jar"
   "Imaging Services","Product__UDM__Imaging__Service__Status", "check_win_process!ImagingServices.Host.exe"
   "I-Site","Product__UDM__iSite__Monitor__Status", "check_win_process!iSiteMonitor.exe"
   "Storage Service","Product__UDM__Storage__Service__Status", "check_win_process!StorageService.exe"
   "IIS Server","Product__UDM__IIS__Server__Status", "check_win_process!inetinfo.exe"
   "Rule Engine DB Service","Product__UDM__Rule__Engine__DB__Service", "check_http_service!/RuleEngineDbService/RuleEngineDbService.svc"
   "UDM Service","Product__UDM__Configuration__Service__Status", "check_http_service!/UDMServices"
   "DICOM Wado Service","Product__UDM__DICOMWado__Service__Status", "check_http_service!/IHEAuthority/WadoRs/ping"
   "DICOM StoRS Service","Product__UDM__DICOMStoRS__Service__Status", "check_http_service!/IHEAuthority/StowRs/ping"
   "Non DICOM RS Service","Product__UDM__NonDICOMRS__Service__Status", "check_http_service!/IHEAuthority/NondicomRs/ping"
   "DICOM RS Service","Product__UDM__DICOMRS__Service__Status", "check_http_service!/IHEAuthority/DicomRs/ping"


.. csv-table:: **UDM Prep Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "BoS Eventing Service","Product__UDM__BoSEventing__Service__Status", ""
   "UDM Monitor", "Product__UDM__Monitoring__Service__Status", "check_win_process!Philips.UDM.Monitor.EXE"
   "Imaging Services","Product__UDM__Imaging__Service__Status", check_win_process!ImagingServices.Host.exe
   "I-Site","Product__UDM__iSite__Monitor__Status", "check_win_process!iSiteMonitor.exe"
   "Storage Service","Product__UDM__Storage__Service__Status", "check_win_process!StorageService.exe"
   "IIS Server","Product__UDM__IIS__Server__Status", "check_win_process!inetinfo.exe"
   "UDM Service","Product__UDM__Configuration__Service__Status", "check_http_service!/UDMServices"
   "FHIR Extension Service","Product__UDM__FHIRExtension__Service__Status", "check_http_service!/FHIRExtensionServices/fhir/ImagingStudyExtension"


.. csv-table:: **UDM Storage Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Storage Service","Product__UDM__Storage__Service__Status", "check_win_process!StorageService.exe"
   "IIS Server","Product__UDM__IIS__Server__Status", "check_win_process!inetinfo.exe"
   "SAP","Product__UDM__SAP__Service__Status", "check_http_service!/storage-access/options"
   "Notification Service","Product__UDM__Notification__Service__Status", "check_http_service!/notification/options"
   "Backup SAP Service","Product__UDM__BackupAccess__Service__Status", "check_http_service!/backup-access/options"
   "Backup Notification Service","Product__UDM__BackupNotification__Service__Status", "check_http_service!/backupnotification/options"
   "UDM Service","Product__UDM__Configuration__Service__Status", "check_http_service!/UDMServices"
