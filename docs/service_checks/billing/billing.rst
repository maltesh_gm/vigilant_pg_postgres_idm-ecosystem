..  _concerto:


======================
IDM iSite Billing
======================


The IDM iSite Billing statistics integration is all about fetching 24hrs study information from a Site and uploading the same to the S3 bucket in CSV format. 


I Functionality
##################

-   Discover Database Node
-   Connect Stentor Statistics DB
-   Extracts Received Studies Table
-   Creates CSV file with the Daily incremental Data
-   Upload the CSV file to S3
-   Handles Errors 



**Figure1:**

.. image::  images/iSiteBilling.PNG
    :width: 1000px
    :align: center
    :height: 530px
    :alt: default values


II iSiteBilling Plugin
###############
iSiteBilling Plugin is a custom Plugin, designed to do billing specific activities.

- Initially the plugin retrievs the recent billing date for a site (**/billingapp/status/<SITEID>**), if the date is available, billing will resume from that date, if the date is not available then, billing will be done only for the present date.

- Performs database query to fetch study data for 24hours duration.
        - That is if the Plugin is running at June 26,11:00 AM UTC, data would be fetched respectively,
        - From June 25, 12:00:00 AM UTC to JUNE 26, 12: 00:00 AM UTC (Exclusive)
        - CSV file name would be generated out of the from date in the query field.
        - In the above scenario file name would be `BillingStatistics_SITE25_2017-06-25_00-00-00.csv`

        **Procedure Statement**::

                SELECT R.StudyUID, R.ReceivedDateTime, R.SourceIP, S.aeTitle AS AETitle,
                R.StudyDateTime, S.StationName, S.modalityType AS ModalityType, R.BodyPart,\
                S.InstitutionName AS InstitutionName, S.departmentName AS DepartmentName, \
                SUM(R.numberOfImages) AS NumberOfImages, SUM(R.studySize)/1024.0/1024.0 AS 'StudySize (MB)',\
                R.ArchivedDateTime, R.DeletedDateTime, S.manufacturerModelName AS ManufacturerModelName, \
                R.Host, S.manufacturer AS Manufacturer, S.Ghost, S.SourceInternalId, R.ConsolidationDateTime, \
                R.ProcessorId, R.ProcessorLocationName, R.OrganizationId FROM DBO.Sources \
                AS S JOIN DBO.ReceivedStudies AS R on S.sourceInternalId = R.sourceInternalId \
                WHERE R.ReceivedDateTime >= @FromDate AND R.ReceivedDateTime < @EndDate \
                GROUP BY R.ProcessorLocationName, R.OrganizationId, R.StudyUID, \
                R.AccessionNumber, R.PatientID,R.ReceivedDateTime,R.ArchivedDateTime, \
                R.DeletedDateTime, R.StudyDateTime, R.ProcessorId,R.ConsolidationDateTime, \
                S.AETitle,S.ModalityType,S.manufacturerModelName, S.Manufacturer, \
                S.StationName,S.InstitutionName,S.DepartmentName, S.SourceIP,R.BodyPart, \
                R.SourceIP, S.Ghost, S.SourceInternalId, R.Host ORDER BY ReceivedDateTime

- Push the data to the Vigilant **/billingapp/upload** route in JSON format
        **Payload**:: 

                {
                  "siteid": "SIJO1",
                  "studies": [
                    {
                      "ModalityType": "CT",
                      "StudyDateTime": "2017-07-19 12:58:10",
                      "OrganizationId": "DEFAULT",
                      "ArchivedDateTime": "2017-07-19 00:37:09",
                      "AETitle": "DEFAULT",
                      "ReceivedDateTime": "2017-07-19 00:37:10",
                      "ProcessorLocationName": "Main Location",
                      "InstitutionName": "Stentorian Health Group",
                      "ConsolidationDateTime": "2017-07-19 00:37:09",
                      "ProcessorId": "idm01pr3.idm01.isyntax.net",
                      "Host": "idm01pr3.idm01.isyntax.net",
                      "SourceIP": "192.168.180.39",
                      "Manufacturer": "Picker International, Inc.",
                      "Ghost": false,
                      "DepartmentName": null,
                      "BodyPart": "STNDCMTK",
                      "NumberOfImages": 1,
                      "StudySize (MB)": 0.47258663085899999,
                      "DeletedDateTime": null,
                      "StudyUID": "1.2.3.2017719125810.10000",
                      "StationName": "Picker_CT",
                      "ManufacturerModelName": "PQCT",
                      "SourceInternalId": -6442450943
                    }
                  ],
                  "file_name": "BillingStatistics_SIJO1_2017-07-18_00-00-00.csv"
                }

- Configured to Shinken to run every day 1AM (UTC)


- Verified with MSSQL 2008


III Configuration.
###############
    In order to the iSite billing to work the DB node has to be discovered.

    #. lhost.yml::

        endpoints :
        - address: <address>
          scanner: iSite
          username: <username>
          password: <password>

    #. Plugin Configuration

         **Shinken  service definition for iSite Billing Plugin**    


            */etc/philips/shinken/services/isite.cfg*::


                define service {
                    use                     standard-service
                    hostgroup_name          isitebilling-servers
                    service_description     Product__IntelliSpace__BillingStatistics__Service__Status
                    check_command           check_billing
                    check_period            billing_period
                    max_check_attempts      3
                    retry_interval          15
                    check_interval          1440
                    notification_interval   1440
                    }
          
         **Shinken  time period definition for iSite Billing Plugin**    


            */etc/philips/shinken/timeperiods/timeperiods.cfg*::


                define timeperiod {
                  timeperiod_name                billing_period
                  alias                          iSiteBillingTimePeriod
                  monday                         01:00-02:00
                  tuesday                        01:00-02:00
                  wednesday                      01:00-02:00
                  thursday                       01:00-02:00
                  friday                         01:00-02:00
                  saturday                       01:00-02:00
                  sunday                         01:00-02:00
                }


    #. iSite Billing Thruk view

        **Figure1:**

            .. image::  images/iSiteBillingThrukview.PNG
                :align: center
                :alt: default values


    #. S3 

        - The S3 Access key, Secret key and bucket name should be provided in the Vigilant **tbconfig** file.


        if the S3_FOLDER name is given, then files will be uploaded to that folder inside the S3 bucket.
          ::

               'BILLING':{
                    'ACCESS_KEY':<access-key>,
                    'SECRET_KEY':<secret-key>,
                    'BUCKET':<bucket-name>
                    'S3_FOLDER':<folder-name>
               }
        - The celery service should be restarted once you modify the **tbconfig** file.
           ::


            service celeryd restart


    #. Load Balancer Billing EndPoint 

        - The **/billingapp** should be up and running and proper redirection has to be done at the LoadBalancer **/etc/nginx/conf.d/philips-lb.conf** file
        
        - Set proxy timeouts.
           ::


            service nginx restart
         Load Balancer nginx :- Route and Timeout configuration:

            .. image::  images/load_balancer_nginx_philips-lb1.PNG
                :align: center
                :alt: default values


    #. Gateway Configuration 

        - nginx - /etc/nginx/conf.d/default.conf uwsgi timeout

            .. image::  images/gateway_nginx_default.PNG
                :align: center
                :alt: default values
        
        - nginx - /etc/nginx/conf.d/default_server_includes/gateway.conf uwsgi timeout

            .. image::  images/gateway_nginx_gateway.PNG
                :align: center
                :alt: default values


           Restart nginx
            ::

             service nginx restart

        - uwsgi - /etc/default/uwsgi

            .. image::  images/uwsgi.PNG
                :align: center
                :alt: default values


           Restart uwsgi
            ::

             service uwsgi restart






    #. Proxy configurations

        - If Vigilant requires proxy to access internet, the following needs to be added to the to the bottom of **/etc/default/celeryd** file
           ::

                export C_FORCE_ROOT='true'
                export HTTP_PROXY='http://<ip>:<port>'
                export HTTPS_PROXY='http://<ip>:<port>'
                export http_proxy='http://<ip>:<port>'
                export https_proxy='http://<ip>:<port>'


        - The celery service should be restarted once you modify this.
           ::

                service celeryd restart

IV Billing APP
##############
There is a separate **uwsgi** + **bottle** application, created to handle all the iSite billing specific requirements, which will run at the gatway node.

There are Three routes available:

      +----------+---------+--------------------+
      |  upload  |  POST   | Upload data to S3  |
      +----------+---------+--------------------+
      |  status  |  GET    | Retrieve recent    |
      |          |         | successfullupload  |
      +----------+---------+--------------------+
      |  health  |   GET   | App health         |
      +----------+---------+--------------------+



      1  Load Balancer configuration
      ::

        location ~ ^/billingapp/ {
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_pass http://<IP>;
          client_max_body_size 100M;
          proxy_read_timeout 120s;
          proxy_connect_timeout   120s;
          proxy_send_timeout      120s;
          }


      2 Gateway nginx configuration
      ::
          location /billingapp/ {
          include         /etc/nginx/uwsgi_params;
          uwsgi_param     UWSGI_SCHEME $scheme;
          uwsgi_param     SERVER_SOFTWARE    nginx/$nginx_version;
          uwsgi_pass      127.0.0.1:3041;
          uwsgi_send_timeout 120s;
          uwsgi_read_timeout 120s;
          }

      3 Uwsgi configuration
      ::

        [uwsgi]
        socket = :3041
        master = True
        stats = 127.0.0.1:9195
        vacuum = True
        pythonpath = /usr/lib/philips/uwsgi/billingapp
        module = billing_app
        callable = app

V  Database Status Updation
####################


There will be an entry in the Mongo database billing collection, in the **status_history** array field before and after uploading the CSV file to S3,  with upload status **False** (before uploading), **True** (after uploading) respectively.
In the **status_history**, history entries are maintained in ascending order based on the billing_datetime value, also the array field is holds recent 60 entries only.

That is For a site the recent billing status will be the last entry in the **status_history** field with **upload status** true.




VI Prerequites.
###############

    - The **boto** library should be installed in the Vigilant/Celery Workers
    - The **pymssql** library should be installed in the Neb
    - The **billingapp**  should be running in the Backoffice/Vigilant
    - Proper S3 configurations, should be provided  
    - The **isite_billing.py** Plugin, should be available in the Neb's **/usr/lib64/nagios/plugins** directory


VII Error Handling
###############

Following errors are been handled, all cases appropriate error message would be displayed at the Thruk view

    - Unable to establish a communication to the DB node
    - Unable to communicate with the BillingApp
    - In Both Cases Plugin will trigger CRITICAL alarm and will retry 3 times with 15minutes interval.
