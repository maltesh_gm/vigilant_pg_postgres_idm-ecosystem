..  _ibe:

==============================
IBE Integration
==============================

**HostScanner** - IBEHostScanner

**Hostgroups** - ibe-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - Application Specific Checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Rhapsody", "Product__IBE__Rhapsody__Service__Status", check_win_process!wrapper.exe
   "SQL Server", "Product__IBE__SQLServer__Service__Status", check_win_process!sqlservr.exe
   "DICOMManagerService", "Product__IBE__DICOMManagerService__Service__Status", check_win_process!Philips.IBE.DICOMManagerService.exe
   "NetTCPPortSharing", "Product__IBE__NetTCPPortSharing__Service__Status", check_win_process!SMSvcHost.exe
   "HealthecareIntegrationFoundation", "Product__IBE__HealthecareIntegrationFoundation__Service__Status", check_win_process!Philips.HIF.ServiceHost.exe
   "ICIPiXProxy", "Product__IBE__ICIPiXProxy__Service__Status", check_win_process!Philips.IBE.ICIPiXProxy.exe
   "DcomLaunch", "Product__IBE__DcomLaunch__Service__Status", check_win_service!^DcomLaunch$!_NumGood=1:
   "Dhcp", "Product__IBE__DHCPClient__Service__Status", check_win_service!^Dhcp$!_NumGood=1:
   "Dnscache", "Product__IBE__DNSClient__Service__Status", check_win_service!^Dnscache$!_NumGood=1:
   "EncryptingFileSystem", "Product__IBE__EncryptingFileSystem__Service__Status", check_win_service!^EFS$!_NumGood=1:
   "GroupPolicyClient", "Product__IBE__GroupPolicyClient__Service__Status", check_win_service!^gpsvc$!_NumGood=1:
   "NlaSvc", " Product__IBE__NetworkLocationAwareness__Service__Status", check_win_service!^NlaSvc$!_NumGood=1:
   "nsi", "Product__IBE__NetworkStoreInterfaceService__Service__Status", check_win_service!^nsi$!_NumGood=1:
   "Power", "Product__IBE__Power__Service__Status", check_win_service!^Power$!_NumGood=1:
   "TermService", "Product__IBE__RemoteDesktopServices__Service__Status", check_win_service!^TermService$!_NumGood=1:
   "RpcSs", "Product__IBE__RemoteProcedureCall__Service__Status", check_win_service!^RpcSs$!_NumGood=1:
   "SamSs", "Product__IBE__SecurityAccountsManager__Service__Status", check_win_service!^SamSs$!_NumGood=1:
   "LanmanServer", "Product__IBE__LanmanServer__Service__Status", check_win_service!^LanmanServer$!_NumGood=1:
   "SENS", "Product__IBE__SystemEventNotification__Service__Status", check_win_service!^SENS$!_NumGood=1:
   "ProfSvc", "Product__IBE__UserProfileService__Service__Status", check_win_service!^ProfSvc$!_NumGood=1:
   "EventLog", "Product__IBE__WindowsEventLog__Service__Status", check_win_service!^EventLog$!_NumGood=1:
   "MpsSvc", "Product__IBE__WindowsFirewall__Service__Status", check_win_service!^MpsSvc$!_NumGood=1:
   "Winmgmt", "Product__IBE__WMI__Service__Status", check_win_service!^Winmgmt$!_NumGood=1:
   "WAS", "Product__IBE__WAS__Service__Status", check_win_service!^WAS$!_NumGood=1:
   "wuauserv", "Product__IBE__WindowsUpdate__Service__Status", check_win_service!^wuauserv$!_NumGood=1:
   "W3SVC", "Product__IBE__W3SVC__Service__Status", check_win_service!^W3SVC$!_NumGood=1: