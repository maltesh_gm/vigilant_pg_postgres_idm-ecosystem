..  _i4prep:

==============================
I4Prep Integration
==============================

**HostScanner** - I4PrepHostScanner

**Hostgroups** - i4prep-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"


Service Checks
##############
 - Basic Checks
 - Rest Checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "NetTcpPortingSharing", "Product__IntelliSpace__I4Prep__NetTcpPortingSharing__Status", check_win_process!SMSvcHost.exe
   "PortalService", "Product__IntelliSpace__I4Prep__PortalService__Status", check_win_process!PortalService.exe
   "PmsPortalAdminService", "Product__IntelliSpace__I4Prep__PmsPortalAdminService__Status", check_win_process!PmsPortalAdmin.exe
   "PmsCTLogService", "Product__IntelliSpace__I4Prep__PmsCTLogService__Status", check_win_process!LogService.exe
   "ExtendRemotingService", "Product__IntelliSpace__I4Prep__ExtendRemotingService__Status", check_win_process!ExtendedRemotingService.exe
   "E2EService", "Product__IntelliSpace__I4Prep__E2EService__Status", check_win_process!E2E.Service.exe
   "IDSSystemService", "Product__IntelliSpace__I4Prep__IDSSystemService__Status", check_win_process!IDSSystemService.exe
   "I4LoggingService", "Product__IntelliSpace__I4Prep__I4LoggingService__Status", check_win_process!I4LoggingService.exe
   "W3SVC", "Product__IntelliSpace__I4Prep__W3SVC__Status", check_win_service!W3SVC
   "GfnApplicationService", "Product__IntelliSpace__I4Prep__GfnApplicationService__Status", check_win_process!GfnApplicationService.exe
   "DataProvider", "Product__IntelliSpace__I4Prep__DataProvider__Status", check_json_status!DataProvider/DataProvider/Monitor!Applications[*]!Status!Message!ApplicationName
   "SnapITWebService", "Product__IntelliSpace__I4Prep__SnapITWebService__Status", check_json_status!IdmServices/SnapITWebService/Monitor!Applications[*]!Status!Message!ApplicationName
   "ClinicalLabel", "Product__IntelliSpace__I4Prep__ClinicalLabel__Status", check_json_status!ClinicalLabelService/ClinicalLabel/Monitor!Applications[*]!Status!Message!ApplicationName
   "TransferService", "Product__IntelliSpace__I4Prep__TransferService__Status", check_json_status!TransferService/TransferService/Monitor!Applications[*]!Status!Message!ApplicationName
   "PreprocessingService", "Product__IntelliSpace__I4Prep__PreprocessingService__Status", check_json_status!PreProcessingService/PreprocessingService/Monitor!Applications[*]!Status!Message!ApplicationName
   "JobService", "Product__IntelliSpace__I4Prep__JobService__Status", check_json_status!JobService/JobService/Monitor!Applications[*]!Status!Message!ApplicationName
   "GfnServlets", "Product__IntelliSpace__I4Prep__GfnServlets__Status", check_json_status!IdmServices/Dashboard/Monitor!Applications[*]!Status!Message!ApplicationName
