==================
Build Requirements
==================

VMware vSphere SDK CLI
======================

Build requires: perl, perl-ExtUtils-MakeMaker, perl-URI, perl-XML-LibXML, perl-libwww-perl, perl-Net-SSLeay,
perl-Crypt-SSLeay, perl-SOAP-Lite, uuid-perl


uwsgi psgi plugin
=================

Build requires:

From pip:

uwsgi

From CentoOS/EPEL repos:

perl-ExtUtils-Embed

Ensure kernel and uwsgi versions on build server match the desired output versions.


RPM Macros
==========

RPM macros file is usually called .rpmmacros and located in the user's home directory.

For the jenkins user this would be::

    cat /home/jenkins/.rpmmacros


Abort on Unpackaged Files
=========================

Newer versions of rpm are setup to abort builds on unpackaged files, this can be managed in the specfile (and it should
be). Some of the IDM spec files are not addressing this, until they all do the macro to disable it can be used by
adding the following to .rpmmacros::

    %_unpackaged_files_terminate_build 0


GPG Signing
===========

Build requires rng-tools for GPG Key generation. The file /etc/sysconfig/rngd should include
`EXTRAOPTIONS="-r /dev/urandom"` to fill the entropy pool.
Make sure the environment variable is set `export GPG_AGENT_INFO=/home/jenkins/.gnupg/S.gpg-agent:12897:1;`

OR

Run `gpg-agent -daemon` and manually export the information. `

Run `gpg --gen-key`  using a minimin of 2048 Keys as the user who will be performing the signing (CentOS might have a
problem with `su` and key generation so ssh directly.

Set the RPM macros

::

    %_signature gpg
    %_gpg_path /home/jenkins/.gnupg
    %_gpg_name PhiAdmin (Default signing key for IDM Ecosystem) <phiadmin@isyntax.net>
    %_gpgbin /usr/bin/gpg

Add --sign to the fab file

Export out the ASCII version of the key with `gpg --armor --output IDM-GPG-KEY --export 'PhiAdmin'`

Current public singing key. Import it with rpm --import IDM-GPG-KEY

::

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Version: GnuPG v2.0.14 (GNU/Linux)

    mQENBFamnfMBCAD2tPqRJ0Nx8W+X3Unu2cm9+GM6It7Kj0ArlslYXmqlZAylqlb7
    JQgaNRRdnpTYyq2FPh5dThH3+1Wggk9xB/dZYjHwltXC5v7lw6+eDJDiXr+Eexag
    6QRbWyJqZe493m/Li3I2OsmWVh6EKAAfgijx5Ue7qmwGyHXhTKCPBJGpJi+qhbA9
    LpY469YwhI9v2i9sibQ1qrxHVry1NUeeojvutK3OIupS74GjcMZnaJSg6+pbzzZD
    GzrtTTRNph5YL51Iv9Q5mgtW9qnQ85gKjVwz+156/ovy0AYkZLtt4RRABZ7lD5AU
    aX9XL9T3x574jxqP9/gEYcalCuuQGhfA45+jABEBAAG0UklETSBFY29zeXN0ZW0g
    KERlZmF1bHQgUmVwb3NpdG9yeSBTaWduaW5nIGtleSBmb3IgSURNIEVjb3N5c3Rl
    bSkgPGlkbUBpc3ludGF4Lm5ldD6JATgEEwECACIFAlamnfMCGwMGCwkIBwMCBhUI
    AgkKCwQWAgMBAh4BAheAAAoJEHpUHUPKX0GJ31sH/1LaB8S3PeVcJ9ZZYqSoQzYk
    AjoO3qPsCfGXrpQf2UVy4xhSRzK6uTVx+0GP4fknRrr3bg3iIRDkmPvX0ZxmYQ7x
    HljEvFRwmvCfGU8ecOmUTfkIXyBuWRzCUlaVeN+vzaEupmOc9MsBiQAHkxw3FYq5
    1580mrEaOdWcmDqw1cf0LQGxXassIXqgeP8/iK6TIuIrsObiipUGA1cEDVaiLA3G
    0gn63OcKA6OrC4ziXgz7DIpD8p5M/Ilw3hd8pQvhVonEPBTcWx0+nvNRmCSBMCOq
    Ce+Q5liG2UOdDcZDlkhrQX4CtVlLdcvjPyrRjEvc4HoAISYAnVCbYFfobTf1rqC5
    AQ0EVqad8wEIAJ61nvqFoQwP8SvG6644VZdKJdO35rLT8tjcZOe2udbRWPkgLRnM
    l1vHV72e1B91lJQmh11QtR3gWwXTLuQi3hvSRPHXpKo4We3jWmPQS1EMcr0ATQBF
    i1LmB4Sz8zisrfzSjf47UN4d55QIQ6xbkCAt83eRdbcxnmPlJbWL2PTiIXEj4oer
    I5X6qOCxv+M9eFDigzjsI/FScN1QmFAqzVbI3fGzSHNnMLSuZZzDhlVVU6g1uMKS
    s46h8NH+Xsh8UvLaVodGZy0qKmkAFL2hscB+YZADsmpBZfrwoR8gdpoBFqm+VuYD
    NhLUroARlYLL7wCf1+wLHD0g9teIVLAkMJ8AEQEAAYkBHwQYAQIACQUCVqad8wIb
    DAAKCRB6VB1Dyl9Bico8B/9dMTCd/AxCdiMKJwWVWxjfsdoYAYMxADvp4EkFUa9Z
    pfQStBfiJKSgrevsuHBe7IC7SVQGkiVqhaGv/VBpPBsPSDKUkYRP9g0YTCXrD5eH
    MtMLyZE2aRg6cxJ88uGenTabNyw5UqVw9O7zeT4apP5UQ4n8962qsk8OeJek7+WS
    BckcjkGt1Xo71rvXru+r0rEbeoFkQrHeFzndFS0sxPyaKTsZ/F0DH4IdLGeDhmpR
    KXM1d/3Wtbgr7tZOh/42zviwDoRbpJWu9fqloumc9wGvK50kUmE393CqS4qYFMjU
    iwaWI7rIjqmYHhP21P96o6vP9CEy+Qtbd/xi31+weJ43
    =tLMM
    -----END PGP PUBLIC KEY BLOCK-----
