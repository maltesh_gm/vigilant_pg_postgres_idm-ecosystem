==========
Conditions
==========

High Level Overview
-------------------

Conditions are critical to the desired state configuration aspect of PCM. They determine when a product is properly
installed and that install, upgrade or remove actions do not need to be executed. Without proper conditions a product
could be installed every time during a PCM Check. The condition structure is the same no matter where in the PCM
meta-data it is, such as Pre-Condition, Installed Condition or Remove Conditions

Some good guidelines when structuring conditions for a package

- Is version specific 
- Identifies all services that need to be running for the product
- Verifies all product endpoints are available

Conditions are structured in XML with the following nodes

.. code-block:: xml

     <Condition>
      <Test></Test>
      <TestObject></TestObject>
      <TestData></TestData>
      <ExpectedResult></ExpectedResult>
    </Condition> 

- ``Test`` Specifies the condition type to be performed 
- ``TestObject`` Target of the condition.  
- ``TestData`` The data to be tested against. The specific nature varies for different ``Test``
- ``ExpectedResult`` The expected result of the test. This can vary depending on the specific ``Test``


Multiple Conditions
-------------------

Multiple conditions can be tested by listing out multiple ``TestObject`` nodes inside a condition as long as the same
``TestData`` and ``ExpectedResult`` apply to it. 

Example :

.. code-block:: xml

     <Condition>
       <Test>FolderExists</Test>
       <TestObject>[databag@SolutionRootFolder]</TestObject>
       <TestObject>[databag@SolutionRootFolder]\VLCapture\Configuration</TestObject>
       <TestObject>[databag@SolutionRootFolder]\VLCapture\Anywhere\</TestObject>
       <TestData />
       <ExpectedResult>True</ExpectedResult>
     </Condition>


File/Folder Conditions
----------------------

FolderExists
^^^^^^^^^^^^

Does a particular folder exist or not

- ``TestObject`` The folder under test  
- ``TestData`` Not used. 
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>FolderExists</Test>
      <TestObject>[databag@SolutionRootFolder]</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>
         
    <Condition>
      <Test>FolderExists</Test>
      <TestObject>[databag@SolutionRootFolder]\VLCapture\</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>

FileExists
^^^^^^^^^^

Does a particular file exist or not

- ``TestObject`` The file under test  
- ``TestData`` Not used. 
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>FileExists</Test>
      <TestObject>S:\Philips\Apps\VLCapture\Anywhere\version.html</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>
    

FileContains
^^^^^^^^^^^^

Does a particular file contain a given value. If ``FileContains`` is being used for version check, then it is highly
advisable that the ``TestData`` value is injected into the xml file via the CI build system.

- ``TestObject`` The file under test  
- ``TestData`` The value to search for
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>FileContains</Test>
      <TestObject>S:\Philips\Apps\VLCapture\Anywhere\version.html</TestObject>
      <TestData>1.1.12.0</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>

    <Condition>
      <Test>FileContains</Test>
      <TestObject>%SOLUTION_ROOT%\bin\StudyImport\Web\StudyImport\awapp\appversion.txt</TestObject>
      <TestData>BUILD_VERSION</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>    


FileVersionEquals
^^^^^^^^^^^^^^^^^

Does a particular file version attribute contain a given value. It is highly advisable that the ``TestData`` value is
injected into the xml file via the CI build system. This check is designed to verify that a specific version of software
is installed, usually the version that this PCM metadate file is associated with.

- ``TestObject`` The file under test  
- ``TestData`` The version to match
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>FileVersionEquals</Test>
      <TestObject>%SOLUTION_ROOT%\bin\InfrastructureServices\Server\DatabaseTool.exe</TestObject>
      <TestData>4.4.1077.19</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition> 
    
    <Condition>
      <Test>FileVersionEquals</Test>
      <TestObject>%SOLUTION_ROOT%\bin\InfrastructureServices\Server\DatabaseTool.exe</TestObject>
      <TestData>BUILD_VERSION</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>      

FileVersionMinimum
^^^^^^^^^^^^^^^^^^

Does a particular file version attribute meet a minimum value. This check is designed to verify that other versions of
software are installed as dependencies or pre-conditions. It is not advisable to use this to verify that the main
software is installed.

- ``TestObject`` The file under test  
- ``TestData`` The minium acceptable version
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>FileVersionEquals</Test>
      <TestObject>%SOLUTION_ROOT%\bin\InfrastructureServices\Server\DatabaseTool.exe</TestObject>
      <TestData>4.4</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition> 

EnvironmentVariables
--------------------

EnvironmentVariableExists
^^^^^^^^^^^^^^^^^^^^^^^^^

Does a particular environment variable exist

- ``TestObject`` The variable name 
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

     <Condition> 
       <Test>EnvironmentVariableExists</Test>
       <TestObject>SOLUTION_ROOT</TestObject>
       <TestData />
       <ExpectedResult>True</ExpectedResult>
     </Condition>    

EnvironmentVariableValueEquals
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Does a particular environment variable equal a given value

- ``TestObject`` The variable name 
- ``TestData`` The value
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>EnvironmentVariableValueEquals</Test>
      <TestObject>USERDOMAIN</TestObject>
      <TestData>[databag@Domain]</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>

EnvironmentVariableValueContains
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Does a particular environment variable contain a given value

- ``TestObject`` The variable name   
- ``TestData`` The value to search for
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>EnvironmentVariableValueContains</Test>
      <TestObject>PATH</TestObject>
      <TestData>SOLUTION_ROOT\DatabaseTool.exe</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>

RegistryEntry
-------------


RegistryEntryExists
^^^^^^^^^^^^^^^^^^^

Does a registry entry exist

- ``TestObject`` The registry full key name
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>RegistryEntryExists</Test>
      <TestObject>HKEY_LOCAL_MACHINE\SOFTWARE\JavaSoft\Java Runtime Environment</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>

RegistryEntryValueEquals
^^^^^^^^^^^^^^^^^^^^^^^^

Does a registry entry equal a certain value

- ``TestObject`` The registry full key name
- ``TestData`` The value to search for
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>RegistryEntryValueEquals</Test>
      <TestObject>HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Philips EICI\CheckKey\Key</TestObject>
      <TestData>2</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>

RegistryEntryValueContains
^^^^^^^^^^^^^^^^^^^^^^^^^^

Does a registry entry contain a certain sub-string

- ``TestObject`` The registry full key name 
- ``TestData`` The value to search for
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>RegistryEntryValueContains</Test>
      <TestObject>HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\svcVersion</TestObject>
      <TestData>11.0</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>

MSI 
---

MsiProductNameInstalled
^^^^^^^^^^^^^^^^^^^^^^^

Is the MSI product name installed.

- ``TestObject`` The MSI Product Name 
- ``ExpectedResult`` True or False 

Example :


MsiFileInstalled
^^^^^^^^^^^^^^^^

Is the MSI file name installed.

- ``TestObject`` The MSI File 
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>MsiFileInstalled</Test>
      <TestObject>PCMListenerInstaller.msi</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition> 

MsiProductCodeInstalled
^^^^^^^^^^^^^^^^^^^^^^^

Is the MSI product code installed.

- ``TestObject`` The file under test  
- ``TestData`` This value is not used by the method, however it is good practice to label which code is in
  the ``TestObject``
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>MsiProductCodeInstalled</Test>
      <TestObject>{B346D1A3-37E7-49DD-B4A7-248108C2362B}</TestObject>
      <TestData>InfrastructureServices</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition> 
    
    <Condition>
      <Test>MsiProductCodeInstalled</Test>
      <TestObject>{F26E9954-4201-4CF7-B749-73066DCB3DF0}</TestObject>
      <TestData>DICOMServices</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>

Services
--------

ServiceExists
^^^^^^^^^^^^^

Does a service exist

- ``TestObject`` The service name 
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>ServiceExists</Test>
      <TestObject>PCMListenerService</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>

ServiceRunning
^^^^^^^^^^^^^^

Is a service currently running.

- ``TestObject`` The service name
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>ServiceRunning</Test>
      <TestObject>PCMListenerService</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>

ServiceVersionEquals
^^^^^^^^^^^^^^^^^^^^

Does a particular service version equal a given version. It works by finding the service exe and performing a
FileVersionEquals on it. It is highly advisable that the ``TestData`` value is injected into the xml file via the CI
build system. This check is designed to verify that a specific version of software is installed, usually the version
that this PCM metadata file is associated with. This check is easier to implement than various MSI checks

- ``TestObject`` The service name 
- ``TestData`` The value to search for
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>ServiceVersionEquals</Test>
      <TestObject>PCMListenerService</TestObject>
      <TestData>BUILD_VERSION</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition> 

ServiceVersionMinimum
^^^^^^^^^^^^^^^^^^^^^

Does a particular service version attribute meet a minimum value. This check is designed to verify that other versions
of software are installed as dependencies or pre-conditions. It is not advisable to use this to verify that the main
software is installed.

- ``TestObject`` The file under test  
- ``TestData`` The value to search for
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>ServiceVersionEquals</Test>
      <TestObject>PCMListenerService</TestObject>
      <TestData>1.2</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition> 

Miscellaneous
-------------

SecurityEventOccurred
^^^^^^^^^^^^^^^^^^^^^

*Not implemented*

ProcessActive
^^^^^^^^^^^^^

Is a given process name active. It will only determine if any are found, not any details or count of them.

- ``TestObject`` The process name 
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

    <Condition>
      <Test>ProcessActive</Test>
      <TestObject>McTray</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition> 

ApplicationEventOccured
^^^^^^^^^^^^^^^^^^^^^^^

*Not implemented*

SystemEventOccurred
^^^^^^^^^^^^^^^^^^^

*Not implemented*

AccessToResource
^^^^^^^^^^^^^^^^

*Not implemented*

DllRegistered
^^^^^^^^^^^^^

*Not implemented*

DotNetFrameworkVersionInstalled
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*Not implemented*

PowershellScript
^^^^^^^^^^^^^^^^

*Not implemented*

WebURL    
^^^^^^

Checks a given URL for the designated return code

- ``TestObject`` The URL to check 
- ``TestData`` The expected HTTP return code
- ``ExpectedResult`` True or False 

Example :

.. code-block:: xml

        <Condition>
          <Test>WebUrl</Test>
          <TestObject>https://localhost/VLCapture/apppage.html</TestObject>
          <TestData>200</TestData>
          <ExpectedResult>True</ExpectedResult>
        </Condition>
