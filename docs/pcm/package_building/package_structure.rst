=================
Package Structure
=================

High Level Overview
-------------------

A PCM Package is comprised of two main items.

- A zip file wad of product, install scripts, data and all items needed to install and configure a product.
- A xml metadata file containing information about how to verify the product is installed, as well as any other packages
  the current package depends on.

Key Components of  a Good PCM Package

- ``Idempotence`` An operation that will produce the same results if executed once or multiple times. The final state of
  the machine should not be affected by running the package again.
- ``Self-Contained`` All the needed scripts and templates are in the zip. External information is limited to the site
  map or inside data bags. No manual intervention is required to bring a clean system to customer functional software.
- ``Modular`` Third party software requirements (such as OpenFire) are broken out into separate packages and listed as
  dependencies. Giant monolithic installers should be avoided when possible.

Zip File
--------

Scripts and templates should be included in the product archive and versioned along with it. An example file structure
is as such that when extracted it looks like

.. image:: ../../images/pcm/pcm_dir_example.png


Meta Data
---------

The meta-data file follows the following XML format

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8" standalone="yes"?>
    <Package PackageSchemaVersion="2.0.0.0" xmlns="http://tempuri.org/Package.xsd">
      <Name>ProductName</Name>
      <Version>0.0.0.0</Version>
      <Description>Brief Overview</Description>
      <TargetOperatingSystem>Windows</TargetOperatingSystem>
      <OwnerEmail>adrian.mulvaney@philips.com</OwnerEmail>
      <RemoveRequiresPayload>False</RemoveRequiresPayload>
      <GroupPackage>False</GroupPackage>
      <Checksum>0</Checksum>
      <ExtractFolderSizeInBytes>2763</ExtractFolderSizeInBytes>
      <Dependencies />
      <PreConditions />
      <InstallActions />
      <InstalledConditions />
      <UpgradeActions />
      <UpgradePerformsRemove>True</UpgradePerformsRemove>
      <RemoveActions />
      <RemovedConditions />
    </Package>


Package Information
^^^^^^^^^^^^^^^^^^^

- ``Name`` The product name
- ``Version`` The fully qualified version identifier. Usually 5 or 6 digits depending on the QMS. This is generally
  set by the PCM Packager step inside the build system, so should not be hand coded.
- ``Description`` A brief overview of what this package provides
- ``TargetOperatingSystem`` The target operating system. *This is a suggestion and is not enforced*
- ``OwnerEmail`` Package owner contact information
- ``RemoveRequiresPayload`` *Deprecated*
- ``GroupPackage`` A flag if this a package of packages, meaning there is no corresponding zip. 
- ``Checksum`` A checksum hash of the zip for transfer integrity. This is set by the PCM Packager step inside the build
  system, so should not be hand coded.
- ``ExtractFolderSizeInBytes`` The approximate size of the total package. *This is a suggestion and is not enforced*

Dependencies
^^^^^^^^^^^^

Dependancies are listing of other PCM packages that this package depends on. On execution, PCM attempts to satisfy the
listed dependencies (and any of the *their* dependancies) in a chain format. The version is used to match to the latest
available version of that package. If properly idempotent, they will install or report correctly installed when they are
called.

- If just a name is given such as OpenFire and no version then each PCM execution will make sure the latest available
  OpenFire software is installed. If a newer version of OpenFire is made available on the Neb node, then the next pass
  will attempt to install it.

- If a partial match is given, such as AWV 1.2, then the latest available version of that family will be installed. If a
  newer version of AWV, say 1.2.1, is made available on the Neb node, then the next pass will attempt to install it.

- If a fully qualified version is given, then that version, and only that version, will be installed for this package. 

Example :

.. code-block:: xml

  <Dependencies>
    <OtherPackage>
      <Name>OpenFire</Name>
      <Version />
    </OtherPackage>
    <OtherPackage>
      <Name>PCSC</Name>
      <Version>1.2</Version/>
    </OtherPackage>
    <OtherPackage>
      <Name>McCaffe</Name>
      <Version>1.2.5.2345</Version/>
    </OtherPackage>    
  </Dependencies>

Pre-Conditions
^^^^^^^^^^^^^^

Pre conditions are a set of condition objects that must be satisfied before even attempting to check or install the
product in question. Usually these are environmental verifications about the system in question, and making sure certain
components, like a VCE version or Deployment engine, are in place. Checking things like drive letters, system
configuration specifications can be handled here. 

Example :

.. code-block:: xml

  <PreConditions>
     <Condition>
       <Test>FileExists</Test>
       <TestObject>[databag@deploymentscripts]\deploy.ps1</TestObject>
       <TestData />
       <ExpectedResult>True</ExpectedResult>
     </Condition>
     <Condition>
       <Test>FolderExists</Test>
       <TestObject>[databag@SolutionRootFolder]</TestObject>
       <TestData />
       <ExpectedResult>True</ExpectedResult>
     </Condition>
     <Condition> 
       <Test>EnvironmentVariableExists</Test>
       <TestObject>SOLUTION_ROOT</TestObject>
       <TestData />
       <ExpectedResult>True</ExpectedResult>
     </Condition>     
     <Condition>
       <Test>EnvironmentVariableExists</Test>
       <TestObject>SHAREDDB</TestObject>
       <TestData />
       <ExpectedResult>True</ExpectedResult>
     </Condition> 
  </PreConditions>
   
InstallActions
^^^^^^^^^^^^^^
Install actions list of the steps needed to correct install and configure into a running state a given package. The goal
is not to replace existing packaging tools such as MSI, Deploy.ps1, or .zip. The actions defined here should be the
minimal steps to invoke and configure an application into a known state.

Example :

.. code-block:: xml

  <InstallActions>
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PCM\Scripts\CleanProcesses.ps1</ActionFile>
      <ActionArguments></ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>   
    <Action">
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PCM\Scripts\PlacePackages.ps1</ActionFile>
      <ActionArguments>-PlatformFolder [databag@DeploymentRootFolder] -PackageFolder ".\Server"</ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action> 
    <Action Stage="Install">
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PCM\Scripts\CreateDeploymentXML.ps1</ActionFile>
      <ActionArguments>-InstallScriptsFolder [databag@deploymentscripts] -NodeType [databag@RoleType] -LocalizationCode [databag@LocalizationCode] -ISiteServiceAccount [databag@ISiteServiceAccountUser]  -ISiteServiceAccountPassword [databag@ISiteServiceAccountPassword] [databag@Federated]</ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action> 
    <Action EffectiveUserName="[databag@DomainAdmin]" EffectiveUserPassword="[databag@DomainAdminPassword]" RetryCount="5" Stage="Install">
      <ActionType>PowershellScript</ActionType>
      <ActionFile>[databag@deploymentscripts]\deploy.ps1</ActionFile>
      <ActionArguments>-DeploymentConfigurationFile [databag@deploymentscripts]\DeploymentConfiguration.xml</ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action> 
  </InstallActions>

InstalledConditions
^^^^^^^^^^^^^^^^^^^
 
Installed Conditions are critical to the desired state configuration aspect of PCM. They determine when a product is
properly installed and is running in the proper state/configuration. Without proper conditions a product could be
installed every time during a PCM Check occurs and interrupt a potentially correctly running service. The goal of the
installed conditions is to create an idempotent package that only executes when a valid change has occurred.

Example :

.. code-block:: xml

  <InstalledConditions>
    <Condition>
      <Test>MsiProductCodeInstalled</Test>
      <TestObject>{B346D1A3-37E7-49DD-B4A7-248108C2362B}</TestObject>
      <TestData>InfrastructureServices</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>    
    <Condition>
      <Test>MsiProductCodeInstalled</Test>
      <TestObject>{F26E9954-4201-4CF7-B749-73066DCB3DF0}</TestObject>
      <TestData>DICOMServices</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>
    <Condition>
      <Test>FileVersionEquals</Test>
      <TestObject>%SOLUTION_ROOT%\bin\InfrastructureServices\Server\DatabaseTool.exe</TestObject>
      <TestData>4.4.1077.19</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>     
    <Condition>
      <Test>ServiceRunning</Test>
      <TestObject>iSiteMonitor</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>     
    <Condition>
      <Test>WebUrl</Test>
      <TestObject>https://localhost/VLCapture/apppage.html</TestObject>
      <TestData>200</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>    
  </InstalledConditions>
     
UpgradeActions
^^^^^^^^^^^^^^

Upgrade actions specify what should occur when a product is already installed but a different version has been
identified as the proper one. Generally these actions will mimic the RemoveActions_ and InstallActions_ however it is
called out as a special section to allow things like shuting down services, storing application configuration or any
pre/post actions.

Example :

.. code-block:: xml

  <UpgradeActions>
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PackageScripts\RemoveAWV.ps1</ActionFile>
      <ActionArguments></ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PackageScripts\InstallAWV.ps1</ActionFile>
      <ActionArguments></ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action> 
  </UpgradeActions>
      
RemoveActions
^^^^^^^^^^^^^

Removal actions are an oft ignored item in product development as there is a organization push to get software onto
machines not remove it. However the logic should be there for when a customer wishes a product component removed, or
when determining the logic for upgrades. Some upgrades are as simple as remove the old, install the new.

Example :

.. code-block:: xml

  <RemoveActions>
    <Action>
      <ActionType>WindowsInstaller</ActionType>
      <ActionFile>PCMListenerInstaller.msi</ActionFile>
      <ActionArguments>uninstall</ActionArguments>
      <ActionTimeout>10000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
  </RemoveActions>
        
RemovedConditions
^^^^^^^^^^^^^^^^^

Removed conditions are often the reverse of the installed conditions. However it is not enough to simply perform the
negative state of those actions. There are often subtle differences, such as a 404 code instead of 200 for a ``WebUrl``
test

Example :

.. code-block:: xml

  <RemovedConditions>
    <Condition>
      <Test>ServiceRunning</Test>
      <TestObject>iSiteMonitor</TestObject>
      <TestData />
      <ExpectedResult>False</ExpectedResult>
    </Condition>       
    <Condition>
      <Test>ServiceExists</Test>
      <TestObject>iSiteMonitor</TestObject>
      <TestData />
      <ExpectedResult>False</ExpectedResult>
    </Condition>
    <Condition>
      <Test>FileExists</Test>
      <TestObject>%SOLUTION_ROOT%\bin\InfrastructureServices\Server\DatabaseTool.exe</TestObject>
      <TestData />
      <ExpectedResult>False</ExpectedResult>
    </Condition>         
    <Condition>
      <Test>WebUrl</Test>
      <TestObject>https://localhost/VLCapture/apppage.html</TestObject>
      <TestData>404</TestData>
      <ExpectedResult>True</ExpectedResult>
    </Condition>    
  </RemovedConditions>

