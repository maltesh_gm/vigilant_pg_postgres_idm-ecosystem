============
PCM Listener
============

IDM Bootstrap is a RESTful websevice installed by SDE onto an individual node to enable to the bootstraping of the PCM
package management process. It contains a lightweight version of PCM suitable for installing the full PCM package and
initializing that with the SiteInformation.xml file so the node can auto-determine what packages it needs to gather.  

It is available as an installation option as part of the VCE solution for new installs. Existing nodes need a one time
installation effort.  

.. note:: PCM Listener is a temporary solution until Ansible 1.8 is released in the IDM solutions. Ansible 1.8 is the
          first version to support actor-less Windows actions. 

Installation
------------

Execute the msi installer. It will bind a the HTTP listener to the first IPv4 interface. If multiple interfaces are
present on a node, then manually edit the configuration file to specify the desired binding IP. 

TODO Verify this call ::

   Msiexe.exe /I PCMListener.msi /q targetdir=C:\PCMListener listen_host=HOST IP listen_port=8182


Invocation
----------

The main invocation is by placing a HTTP Post to http://host:port/pcm/bootstrap with a json blob as a package. The json
blob includes the following information. In general the defaults should be used to only download, install and bootstrap
PCM itself, and let the PCM executable handle things from there.

- ``Repository`` This is the root URI including http/https of the repository hosting packages and the siteinformation
  xml.  
- ``SiteInformation`` The full path location of the xml file with the SiteMap that should be used for this node. It is
  concatenated with Repository URI
- ``PackageRoot`` : Directory of the package repo to search for packages based on the Repository uri  
- ``Data`` The first element of this array is a string specifying a script file inside the extracted package. Knowledge
  of the package structure is required. The remaining elements are concatenated and passed as arguments to the script.
  It is not recommended to use Data and multiple packages together, as the Data elements are executed per package. 
- ``Packages`` Version qualified list of packages to download. 'Get Latest' is not supported.   
- ``Workflow`` Commands to execute on the packages. Any number may be specified and are applied to every package.
    - *download* fetch and extract package from repository
    - *install* execute the scripts specified in the Data element
    - *bootstrap* calls a resident pcm exe with the boostrap command and siteinfo.xml 
- ``Callbacks`` These are URLs to be posted to once the PCM invoked by the bootstrap is finished. They are posted with
  a json blob containing the status and list of installed packages.  {'Status': '0', 'Packages': [pcm-2.4.2.1.4.5]}

Example :

.. code-block:: json

  {
    "Repository": "http://167.81.176.102",
    "SiteInformation": "packages/siteinformation.xml",
    "PacakgeRoot": "packages",
    "Data": [
        "PackageScripts\\install-pcm.ps1"
    ],
    "Packages": [
        "pcm-0.0.0.3"
    ],
    "Workflow": [
        "download",
        "install",
        "bootstrap"
    ],
    "Callbacks" : ["http://167.81.183.32:54224"]
  }


Troubleshooting
---------------

PCMListener has a test endpoint that may be used in a standard browser to check the status of the service

- http://host:port/pcm/check (test GET url)

POSTman from Google Chrome may be used to send custom POST requests.

.. image:: ../../images/pcm/postman_example.png


Windows CURL may be used to invoke as well


.. code::

   curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" http://{{node}}/IDMBootstrap/IDMBootstrapService.svc/info -d "{ \"Repository\": \"167.81.176.102\",\"SiteInformation\" : \"siteinformation.xml\",\"PackageRoot\" : \"packages\",\"Data\": [ \"PackageScripts\\install-pcm.ps1\" ], \"Packages\": [ \"pcm-1.0.1.0\" ], \"Workflow\": [\"download\", \"install\", \"bootstrap\"],  \"Callbacks\": [\"http://167.81.183.32:54224\"] }"
   
Linux CURL

.. code::

  curl -XPOST 'http://{{0}}/IDMBootstrap/IDMBootstrapService.svc/info' -d '{
    "Repository": "http://167.81.183.36",
    "SiteInformation": "/repo/siteinformation.xml",
    "PackageRoot" : "repo",
    "Data" : [ "PackageScripts\\install-pcm.ps1" ],
    "Packages" :  ["pcm-0.0.0.4"],
    "Workflow": [
        "download",
        "install",
        "bootstrap" ],
    "Callbacks" : [ "http://167.81.183.32:54224" ] }' -H "Content-Type: application/json"   
   
