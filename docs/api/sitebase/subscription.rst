=============
Subscriptions
=============
Subscriptions registered for IDM Software distribution can be listed via web service. This web service is to be invoked
using a GET request.

The most basic request, which will get a list of all the subscriptions with no grouping in consideration is performed
by doing a GET request to the endpoint “http://<app node ip>/pma/subscription/names”

The response will be in JSON ::

    {"subscriptions": ["product1", "product2", "product3", "product4", "product5", "product6", "productX"]}

The list may be filtered by providing the subscription group, for example the subscriptions in subgroup "two" in the
"idm" group could be found with a GET request to “http://<app node ip>/pma/subscription/names/idm/two”

Subscription Groups
-------------------
The subscriptions may be organized in a group tree structure, somewhat like a filesystem directory structure.
To get the root groups a get request to “http://<app node ip>/pma/subscription/groups” would return something like: ::

    {"groups": ["idmer", "idm"]}

From there the group may be appended to get the subgroups, as in “http://<app node ip>/pma/subscription/groups/idm”
where the response will be the child groups for the “idm” group: ::

    {"groups": ["four", "new", "two"]}

The groups can be used on a get request for subscriptions, the response will contain subscriptions not only in that
group but also in the whole tree below it, for example the response from “http://<app node ip>/pma/subscriptions/idm”
Will contain subscriptions inside:
/idm
/idm/four
/idm/new
/idm/two

Subscription Properties
-----------------------
Querying for details for all subscriptions is possible by issuing a GET request to:

http://<app node ip>/pma/subscription/details

An example for the result may be::

    {"subscriptions": {[
        {
            "subscription": "sub1",
            "path": "/philips/tools",
            "countries": ["USA"],
            "rules": ["magicrule1", "multisitedeployment"]
        },
        {
            "subscription": "sub2",
            "path": "/",
            "countries": ["Russia", "Mexico"],
            "rules": ["singlesitedeployment"]
        }
    ]}}


Subscription Creation
---------------------
The endpoint "http://<app node ip>/pma/in/subscription/create" allows for creation of subscription. Details for the
subscription are to be sent in the body of a POST request as a JSON object.

There is one mandatory field: subscription

There are 3 optional fields: path, countries, rules

For example to create a subscription named "sub1", with path "/philips/tools", countries "Russia, Mexico", and rules
"magicrule1, multisitedeployment", issuing a POST request with JSON to the endpoint `/in/subscription/create` will
result in the desired subscription being created: ::

    {
        "subscription": "sub1",
        "path": "/philips/tools",
        "countries": ["Russia", "Mexico"],
        "rules": ["magicrule1", "multisitedeployment"]
    }

Subscription Update
-------------------
The endpoint "http://<app node ip>/pma/in/subscription/update" allows for update of subscription. A subscription must
exist for it to be updated. Details for the update are to be sent in the body of a POST request as a JSON object.

There is one mandatory field: subscription

There are 3 optional fields: path, countries, rules

For example to update subscription named "sub1", with path "/philips/tools/new", issuing a POST request with JSON to the
endpoint `/in/subscription/update` will result in the desired update: ::

    {
        "subscription": "sub1",
        "path": "/philips/tools/new"
    }

Subscription Submission
-----------------------
The endpoint "http://<app node ip>/pma/in/softwaredistribution/subscriptions" allows for addition of subscription to
sites, a lst of sites and the subscriptions to add to these sites is to be sent in the body of a POST request as a JSON
object.

Posting the following object to the endpoint "http://<app node ip>/pma/in/softwaredistribution/subscriptions": ::

   {"sites": ["site1", "abc01", "xyz00"], "subscriptions": ["product1", "product2"]}

Will result in "site1", "abc01" and "xyz00" getting the manifest xml files for "product1" and "product2".
