..  _api_info:

====================
Information REST API
====================
How to query the IDM for realtime information about the install base

Summary
-------
* /info/status
* /info/status/siteid/<siteid>
* /info/status/state/<state>

All Sites
---------
`/info/status`

This will return information on all the known active sites. This does not look in SVN for sites that are in deployment.

This information is collected from the State and Alive collection only. 

::

    { "info" : [ {
        "siteid" : "CHSCR",
        "last_state": "OK",
        "alive_expiration": "2015-10-08T03:37:38.066147Z",
        "hosts": 36,
        "nebnode": "10.200.151.243"
    },
    {   "siteid" : "MHC00",
        "last_state": "CRITICAL",
        "alive_expiration": "2015-09-03T09:16:54.400895Z",
        "hosts": 220,
        "nebnode": "10.42.97.243"}]
    }

If a site has never sent an alive, or discovery has not run, then that information will be missing from the document.
There will always be one host (the neb node) so any site with 1 should be treated as non discovered.
For example 

::

    { "info" : [ {
        "siteid" : "BHF00",
        "hosts": 1,
        "nebnode": "10.60.11.212"
    },
    {   "siteid" : "VCHA0",
        "last_state": "OK",
        "alive_expiration": "2015-10-08T03:39:16.706136Z",
        "hosts": 1,
        "nebnode": "142.71.72.243"}]
    }

Single Sites
------------
`/info/status/siteid/<siteid>`

This produces a filtered list of the above material. It is very useful for looking up the current neb node IP

For example `/info/status/sitedid/ROY00`

::

    { "info" : [ {
        "siteid" : "ROY00",
        "last_state": "OK",
        "alive_expiration": "2015-10-08T03:40:47.679464Z",
        "hosts": 18,
        "nebnode": "10.108.220.243" }]
    }


Single State
------------
`/info/status/state/<state>`

This produces a filtered list by state. Very useful for only showing states in the critical state

::

    { "info" : [ {
        "siteid" : "UCHHS",
        "last_state": "CRITICAL",
        "alive_expiration": "2015-10-15T04:16:54.749917Z",
        "hosts": 110,
        "nebnode": "10.165.207.7"
    },
    {   "siteid" : "GBR00",
        "last_state": "CRITICAL",
        "alive_expiration": "2015-10-15T18:11:47.306845Z",
        "hosts": 143,
        "nebnode": "10.232.232.17"}]
    }


Neb Node Address
----------------
`/info/monitor/address/siteid/<siteid>`

This produces the address for the Neb node at that site::

    {"address": "10.4.5.243"}


The address will be null if not found::

    {"address": null}


Neb Node URL
------------
`/info/monitor/url/siteid/<siteid>`

This produces the address for the Neb node at that site::

    {"url": "https://10.4.5.243/thruk"}


The url will be null if not found::

    {"url": null}

