=============
Tags REST API
=============
Provides means to query IDM for Tags related data.


All Tags
--------
To get all tags present in tags collection is by issuing a GET request to:

`/tags/`

An example for the result may be::

    {"result": [{"tag": "TAG1", "description": "D1"}]}


Saving Tag
----------
To save a tag in the system a post request with JSON information to be used for the update must be sent.
For example, to save a tag is by issuing a POST request with JSON to  the
endpoint `/tags` will result in add  tag: ::

    {
        "tag": "TAG",
        "description": "D1"
    }