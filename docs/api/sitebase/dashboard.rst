==================
Dashboard REST API
==================
Provides means to query IDM for Dashboard Data.


Query
-----
The way to query is by issuing a GET request to:

/dashboard/

Filters may be passed via query string.
Productid must be Json string of all productids.
For example querying `/dashboard/?siteid=XYZ00&service=abc__xy__zz__dd__Status&productid=[%22PID1%22,%22PID2%22]` may return::

    {"result": [
        {
            "elementid": "XYZ00-host01-abc__xy__zz__dd__Status",
            "siteid": "XYZ00",

        },
        {
            "elementid": "XYZ00-host02-abc__xy__zz__dd__Status",
            "siteid": "XYZ00"
            ...
        }
    ]}


Namespace Container
-------------------
To get a list of unique entries for a namespace container.

Issue a GET request to `/dashboard/namespace/containers/<container>`, filters may be passed via query string.

For example querying `/dashboard/namespace/containers/vendor` may return::

    {"result": ["IBM", "HP"]}


Updating Case Number
--------------------
To update case number a post request with JSON information to be used for the update must be sent.

There are 4 fields, all must be present: siteid, hostname, service, case_number

For example to update the case number for a dashboard entry for siteid "ABC01", hostname "host2.site2.isyntax.net"
and service name "OS__Microsoft__Windows__CPU__Status", issuing a POST request with JSON to  the
endpoint `/in/dashboard/case_number` will result in updated case number: ::

    {
        "siteid": "ABC01",
        "hostname": "host2.site2.isyntax.net",
        "service": "OS__Microsoft__Windows__CPU__Status",
        "case_number": "123456"
    }

All unique modules
------------------
The way to query for already installed modules present across hosts is by issuing a GET request to:

`/dashboard/modules`

An example for the result may be::

    {"result": ["MOD1", "MOD2"]}

Get overall stats
-----------------
The way to query to get overall stastics of the system is by issuing a GET request to:

`/dashboard/stats`

An example of the result may be::
    {"result": [ {"count":1,"status":"CRITICAL","sites":["site1","site2"}], "total_count":1}


Get stats by products
---------------------
The way to query to get overall stastics of the system by different products is by issuing a GET request to:

`/dashboard/stats/products`

An example of the result may be::
    {"result": [ {"unknown":1, "warning":1, "critical":1, product: ["p1","p2"]}]}


Get stats by hardware
---------------------
The way to query to get overall stastics of the system by hardware devies is by issuing a GET request to:

`/dashboard/stats/devices`

An example of the result may be::
    {"result": [ {"count":1,"status":"CRITICAL","sites":["site1","site2"} ]}

Acknowledge events to tags
--------------------------
The way to tag a dashboard event to a tag is by issuing a JSON POST request to:

`/dashboard/acknowledge`

There are 2 fields which are mandatory,For example to tag a event with a tag say "TAG1"
Payload should be::

    {
      "tag": "TAG1"
      "ids": ["ID1","ID2"]
    }

    will result in case_number changed to ACK and will add new column called tag with passed tag name.