NEB on IPV6
****************************************************************

The change currently foucuses on 2 areas.

+ How to initialize NEB in an ipv6 enabled environment
+ The way cfg files are created in a ipv6 enabled environment.

Understanding while the changes are made.

+ By default, the environment is ipv4
+ Should be able to enabled and disable ipv6 features on the basis of a flag in lhosts.yml
+ It should work on dual stack as well as ipv6 only stack

How to initialize NEB for ipv6.
-------------------------------
+ The changes are made to the same initialization.py file.
+ 4 new parameters are added to the initialization.py commandline

+--------+-------------+------------------------------------------------------+
| Short  | Long        |  Description                                         |
+========+=============+======================================================+
| -i6    | --ipv6 .    | ipv6 address of the IDM node.                        |
+--------+-------------+------------------------------------------------------+
| -n6    | --netmask6  | the netmask of the IDM node in ipv6 format           |
+--------+-------------+------------------------------------------------------+
| -g6    | --gateway6  | gateway ipv6 address                                 |
+--------+-------------+------------------------------------------------------+
| -d6    | --dns6      | DNS server ipv6 address(es)                          |
+--------+-------------+------------------------------------------------------+

All above newly added parameters has to be of ipv6 format.
    Sample ipv6 address : 2002:2050:1:1:1:1:1:50

Impact of adding these new parameters

Changes will be done /etc/sysconfig/network::

    NETWORKING_IPV6=yes

Changes will be done to /etc/sysconfig/network-scripts/ifcfg-eth0::

    IPV6INIT=yes
    IPV6ADDR=2002:2050:1:1:1:1:1:50
    IPV6_DEFAULTGW=2002:2050:1:1:1:1:1:1

Changes to be done in backoffice to enable ipv6
-----------------------------------------------

A new flag ipv6_enabled is to be given a value "True" in lhosts.yml::

    address: 192.168.180.27
    password: st3nt0r
    scanner: ISP
    username: Administrator
    ipv6_enabled: True

How things are wired up.
------------------------

+ In an ipv6 environement, everything will be fully qualififed domain name based
+ Shinken plugins currently use the ips in ipv4 format which is expanded by $HOSTADDRESS macro
+ The shinken macro takes the ip addresses from the hosts/.cfg files.
+ These files are gererated as part of discovery and will be pulled in to NEB on the next successfull ansible pull.
+ Code change is made to make the plugin pick the FQDN, rather than the ipaddress.








