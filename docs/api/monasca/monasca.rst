..  _monasca:

-----------
Monasca API
-----------

Introduction
------------

Monasca is an OpenStack project that provides an open-source multi-tenant, highly scalable, performant, fault-tolerant monitoring-as-a-service solution. Metrics can be published to the Monasca API, stored and queried. Alarms can be created and notifications, such as email, can be sent when alarms transition state.

Technologies
------------

Monasca is built on several open-source technologies including:

01. OpenStack: Monasca uses the Keystone OpenStack Identity Service for authentication.
02. Apache Kafka: A high-throughput distributed messaging system. Kafka is a central component in Monasca and provides the infranstructure for all internal communications between components.
03. Apache Storm: A free and open source distributed realtime computation system. Apache Storm is used in the Monasca Threshold Engine.
04. Apache ZooKeeper: Used by Kafka and Storm.
05. InfluxDB: An open-source distributed time series database with no external dependencies. InfluxDB is one of the supported databases for storing metrics and alarm history.
06. MySQL: A freely available open source Relational Database Management System (RDBMS) that uses Structured Query Language (SQL). MySQL is one of the supported databases for the Monasca Config Database.
07. Grafana: An open source, feature rich metrics dashboard and graph editor. Support for Monasca as a data source in Grafana has been added.

Fetching keystone token:
------------------------

Keystone token is used for authentication purpose. This token is valid for four hours.

Way to fetch Keystone through Postman API:

POST /v2.0/tokens HTTP/1.1
Host: {{  keystone_url }}:{{  keystone_port  }}
Content-Type: application/json
Cache-Control: no-cache

{
    "auth": {
        "passwordCredentials":
        { 
                "username" : "username",
            "password" : "password"
        },
        "tenantName":"admin"
    }
}

Example output:

    {
    "access": {
      "token": {
        "issued_at": "2017-04-07T06:29:19.494357Z",
        "expires": "2017-04-07T10:29:19Z",
        "id": "a19ad34951e0476396e77e1558d0f7e6",
        "tenant": {
          "description": "Bootstrap project for initializing the cloud.",
          "enabled": true,
          "id": "20dfd29aca134e9ba8778d40c77e64e7",
          "name": "admin"
        },
        "audit_ids": [
          "XYmDiJgKSXixETI2rEsr_A"
        ]
      }, ...

  Here token['id'] is the keystone token.

Deterministic or non-deterministic alarms:
-----------------------------------------

By default all alarm definitions are assumed to be non-deterministic. There are 3 possible states such alarms can transition to: OK, ALARM, UNDETERMINED. On the other hand, alarm definitions can be also deterministic. In that case alarm is allowed to transition only: OK and ALARM state.

List Alarms:
------------

GET /v2.0/alarms

  Headers

    X-Auth-Token (string, required) - Keystone auth token
    Accept (string) - application/json
  
  Path Parameters
    
    None.

  Query Parameters

    alarm_definition_id (string, optional) - Alarm definition ID to filter by.
    metric_name (string(255), optional) - Name of metric to filter by.
    metric_dimensions ({string(255): string(255)}, optional) - Dimensions of metrics to filter by specified as a comma separated array of (key, value) pairs as key1:value1,key1:value1, ..., leaving the value empty key1,key2:value2 will return all values for that key, multiple values for a key may be specified as key1:value1|value2|...,key2:value4,...
    state (string, optional) - State of alarm to filter by, either OK, ALARM or UNDETERMINED.
    severity (string, optional) - One or more severities to filter by, separated with |, ex. severity=LOW|MEDIUM.
    lifecycle_state (string(50), optional) - Lifecycle state to filter by.
    link (string(512), optional) - Link to filter by.
    state_updated_start_time (string, optional) - The start time in ISO 8601 combined date and time format in UTC.
    offset (integer, optional)
    limit (integer, optional)
    sort_by (string, optional) - Comma separated list of fields to sort by, defaults to 'alarm_id'. Fields may be followed by 'asc' or 'desc' to set the direction, ex 'severity desc' Allowed fields for sort_by are: 'alarm_id', 'alarm_definition_id', 'alarm_definition_name', 'state', 'severity', 'lifecycle_state', 'link', 'state_updated_timestamp', 'updated_timestamp', 'created_timestamp'

  Request Body

    None.

  Request Examples:

    GET /v2.0/alarms?metric_name=cpu.system_perc&metric_dimensions=hostname:devstack&state=UNDETERMINED HTTP/1.1
    Host: 192.168.10.4:8070
    Content-Type: application/json
    X-Auth-Token: 2b8882ba2ec44295bf300aecb2caa4f7
    Cache-Control: no-cache

  Response

    Status Code

      200 - OK

    Response Body

      Returns a JSON object with a 'links' array of links and an 'elements' array of alarm objects with the following fields:

        id (string) - ID of alarm.
        links ([link]) - Links to alarm.
        alarm_definition (JSON object) - Summary of alarm definition.
        metrics ({string, string(255): string(255)}) - The metrics associated with the alarm.
        state (string) - State of alarm, either OK, ALARM or UNDETERMINED.
        lifecycle_state (string) - Lifecycle state of alarm.
        link (string) - Link to an external resource related to the alarm.
        state_updated_timestamp - Timestamp in ISO 8601 combined date and time format in UTC when the state was last updated.
        updated_timestamp - Timestamp in ISO 8601 combined date and time format in UTC when any field was last updated.
        created_timestamp - Timestamp in ISO 8601 combined date and time format in UTC when the alarm was created.

    Response Examples:

      {
          "links": [
              {
                  "rel": "self",
                  "href": "http://192.168.10.4:8070/v2.0/alarms?metric_name=cpu.system_perc&metric_dimensions=hostname%3Adevstack&state=UNDETERMINED"
              },
              {
                  "rel": "next",
                  "href": "http://192.168.10.4:8070/v2.0/alarms?offset=f9935bcc-9641-4cbf-8224-0993a947ea83&metric_name=cpu.system_perc&metric_dimensions=hostname%3Adevstack&state=UNDETERMINED"
              }
          ],
          "elements": [
              {
                  "id": "f9935bcc-9641-4cbf-8224-0993a947ea83",
                  "links": [
                      {
                          "rel": "self",
                          "href": "http://192.168.10.4:8070/v2.0/alarms/f9935bcc-9641-4cbf-8224-0993a947ea83"
                      },
                      {
                          "rel": "state-history",
                          "href": "http://192.168.10.4:8070/v2.0/alarms/f9935bcc-9641-4cbf-8224-0993a947ea83/state-history"
                      }
                  ],
                  "alarm_definition": {
                      "severity": "LOW",
                      "id": "b7e5f472-7aa5-4254-a49a-463e749ae817",
                      "links": [
                          {
                              "href": "http://192.168.10.4:8070/v2.0/alarm-definitions/b7e5f472-7aa5-4254-a49a-463e749ae817",
                              "rel": "self"
                          }
                      ],
                      "name": "high cpu and load"
                  },
                  "metrics": [
                      {
                          "name": "cpu.system_perc",
                          "dimensions": {
                              "hostname": "devstack"
                          }
                      }
                  ],
                  "state": "OK",
                  "lifecycle_state":"OPEN",
                  "link":"http://somesite.com/this-alarm-info",
                  "state_updated_timestamp": "2015-03-20T21:04:49.000Z",
                  "updated_timestamp":"2015-03-20T21:04:49.000Z",
                  "created_timestamp": "2015-03-20T21:03:34.000Z"
              }
          ]
      }

Get alarm count:
---------------

GET /v2.0/alarms/count

  Headers

    X-Auth-Token (string, required) - Keystone auth token
    Content-Type (string, required) - application/json
    Accept (string) - application/json

  Query Parameters

    alarm_definition_id (string, optional) - Alarm definition ID to filter by.
    metric_name (string(255), optional) - Name of metric to filter by.
    metric_dimensions ({string(255): string(255)}, optional) - Dimensions of metrics to filter by specified as a comma separated array of (key, value) pairs as key1:value1,key1:value1,...
    state (string, optional) - State of alarm to filter by, either OK, ALARM or UNDETERMINED.
    severity (string, optional) - One or more severities to filter by, separated with |, ex. severity=LOW|MEDIUM.
    lifecycle_state (string(50), optional) - Lifecycle state to filter by.
    link (string(512), optional) - Link to filter by.
    state_updated_start_time (string, optional) - The start time in ISO 8601 combined date and time format in UTC.
    offset (integer, optional)
    limit (integer, optional)

  Request Examples

    GET /v2.0/alarms/count?metric_name=cpu.system_perc&metric_dimensions=hostname:devstack&group_by=state,lifecycle_state
    HTTP/1.1 Host: 192.168.10.4:8070
    Content-Type: application/json
    X-Auth-Token: 2b8882ba2ec44295bf300aecb2caa4f7
    Cache-Control: no-cache

  Response
  
    Status Code
  
      200 OK

    Response Body

      links ([link]) - Links to alarms count resource
      columns ([string]) - List of the column names, in the order they were returned
      counts ([array[]]) - A two dimensional array of the counts returned

    Response Example

      {
         "links": [
             {
                 "rel": "self",
                 "href": "http://192.168.10.4:8070/v2.0/alarms/count?metric_name=cpu.system_perc&metric_dimensions=hostname%3Adevstack&group_by=state,lifecycle_state"
             }
         ],
         "columns": ["count", "state", "lifecycle_state"],
         "counts": [
             [124, "ALARM", "ACKNOWLEDGED"],
             [12, "ALARM", "RESOLVED"],
             [235, "OK", "OPEN"],
             [61, "OK", "RESOLVED"],
             [13, "UNDETERMINED", "ACKNOWLEDGED"],
             [1, "UNDETERMINED", "OPEN"],
             [2, "UNDETERMINED", "RESOLVED"],
         ]
      }
