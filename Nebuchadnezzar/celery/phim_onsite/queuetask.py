from __future__ import absolute_import

from phim_onsite.celery import app
from taskmapper import make_taskmapper_step
from tbconfig import EXCHANGE_QUEUE_MAP, QUEUE_TASK_MAP

app.steps['consumer'].add(make_taskmapper_step(app, EXCHANGE_QUEUE_MAP, QUEUE_TASK_MAP))