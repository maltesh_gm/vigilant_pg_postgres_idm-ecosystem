from __future__ import absolute_import

import requests

from celery.utils.log import get_logger
from phim_onsite.celery import app
from tbconfig import SHINKEN_PASSIVE_RESULT_URL


logger = get_logger(__name__)
requests.packages.urllib3.disable_warnings()


@app.task(ignore_result=True)
def passive_service(host, service, state, output, ttime=None, url=SHINKEN_PASSIVE_RESULT_URL):
    result = dict(service=service, return_code=state, output=output, timestamp=ttime)
    if host:
        result['host'] = host
    req = requests.post(url, json=result, verify=False)
    req.raise_for_status()
