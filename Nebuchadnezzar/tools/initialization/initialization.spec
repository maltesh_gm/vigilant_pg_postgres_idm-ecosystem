# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phiminitialization %{_prefix}/lib/philips/initialization/

# Basic Information
Name:      phim-initialization
Version:   %{_phiversion}
Release:   2%{?dist}
Summary:   Script to perform initial setup of an onsite IDM instance

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Jason Malobicky <jason.malobicky@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  phimutils
Requires:  python-argparse
Requires:  python-requests

Provides:  phim-initialization = %{version}-%{release}

%description
Script to perform initial setup of an onsite IDM instance

%prep
%setup -q

%build
echo OK

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{phiminitialization}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/initialization.py %{buildroot}%{phiminitialization}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/tabula_rasa.py %{buildroot}%{phiminitialization}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/migrator.py %{buildroot}%{phiminitialization}

%pre
%{__install} -d -m 0755 %{phiminitialization}

%post
if [ ! -d %{_var}/log/philips ]; then
    %{__install} -d -m 0755 -o nagios -g nagios %{_var}/log/philips
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{phiminitialization}/initialization.py
%attr(0755,root,root) %{phiminitialization}/tabula_rasa.py
%attr(0755,root,root) %{phiminitialization}/migrator.py

%changelog
* Tue Oct 28 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Conditionally create log directory

* Thu May 15 2014 Cornell Lawrence ,cornell.lawrence@philips.com>
- Add migrator.py to the build

* Fri Mar 07 2014 Cornell Lawrence <cornell.lawrence@philips.com>
- Add tabula_rasa.py to the build

* Wed Feb 26 2014 Jason Malobicky <jason.malobicky@philips.com>
- Initial Spec File
