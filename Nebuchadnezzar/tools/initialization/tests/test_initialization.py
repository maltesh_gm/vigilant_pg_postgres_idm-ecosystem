#!/usr/bin/env python
__author__ = 'Cornell Lawrence'

import sys
import unittest
from mock import MagicMock


sys.modules['requests'] = MagicMock(name='requests')
sys.modules['ipaddr'] = MagicMock(name='ipaddr')

import initialization
import tempfile
import os


class SiteIdTestClass(unittest.TestCase):
    """
    This will test that site IDs conform to procedure PRO4.08-01 Customer Site Naming Convention.
    """
    def setUp(self):
        pass

    def test_valid_siteid(self):
        def assert_valid_siteid(siteid):
            self.assertTrue(initialization.is_valid_siteid(siteid))
        assert_valid_siteid('MDA00')
        assert_valid_siteid('OMAHA')
        assert_valid_siteid('M1234')
        assert_valid_siteid('CI02Y')

    def test_invalid_siteid(self):
        def assert_invalid_siteid(siteid):
            self.assertFalse(initialization.is_valid_siteid(siteid))
        assert_invalid_siteid('4MAUI')  # starts with number
        assert_invalid_siteid('mda00')  # invalid case
        assert_invalid_siteid('MDA001')  # too long
        assert_invalid_siteid('mDA00')  # starts with wrong case
        assert_invalid_siteid('MDA0')  # too short
        assert_invalid_siteid('MDA_1')  # non-alphanumeric character
        assert_invalid_siteid('#87DM')  # non-alphanumeric character
        assert_invalid_siteid('MD-A2')  # non-alphanumeric character


class FileContentTestClass(unittest.TestCase):
    """
    This class will test the functions that handle file content manipulation text in a file.
    """
    def setUp(self):
        self.file1 = tempfile.NamedTemporaryFile(delete=False)
        self.file2 = tempfile.NamedTemporaryFile(delete=False)
        expected_text1 = ('This is the first line.'
                          'This is the second line.',
                          'This is the third line.',
                          'This is the last line.\n'
                        )
        expected_text2 = ('Twinkle, twinkle little star,',
                          'How I wonder what you are?',
                          'Up above the world so high,',
                          'Like a diamond in the sky.'
        )
        with open(self.file1.name, 'w') as f:
            f.write('\n'.join(expected_text1))
        with open(self.file2.name, 'w') as f:
            f.write('\n'.join(expected_text2))

    def test_is_text_in_file_true(self):
        def assert_text_present(test_file, text):
            self.assertTrue(initialization.is_text_in_file(test_file, text))
        assert_text_present(self.file1.name, 'first')
        assert_text_present(self.file1.name, 'second line.')

    def test_is_text_in_file_false(self):
        def assert_text_absent(test_file, text):
            self.assertFalse(initialization.is_text_in_file(test_file, text))
        assert_text_absent(self.file1.name, 'missing')
        assert_text_absent(self.file1.name, 'the fifth')

    def test_last_line_has_newline(self):
        def assert_last_line_newline(test_file):
            self.assertTrue(initialization.is_last_line_terminated_with_newline(test_file))
        assert_last_line_newline(self.file1.name)

    def test_last_line_no_newline(self):
        def assert_last_line_newline_false(test_file):
            self.assertFalse(initialization.is_last_line_terminated_with_newline(test_file))
        assert_last_line_newline_false(self.file2.name)

    def test_append_line_to_file(self):
        def assert_appended_line(written_line, expected_line):
            self.assertEqual(written_line, expected_line)
        expected_text = 'This is the new last line.\n'
        initialization.append_line_to_file(self.file1.name, expected_text)
        with open(self.file1.name, 'rU') as f:
            content = f.read().splitlines(True)
        assert_appended_line(content[-1], expected_text)

    def test_replace_line_in_file(self):
        def assert_replaced_line(test_line, file_string):
            self.assertTrue(test_line in file_string)

        def assert_not_replaced_line(test_line, file_string):
            self.assertFalse(test_line in file_string)
        replace = 'wonder'
        newline = 'I know what you are.\n'
        with open(self.file2.name, 'rU') as f:
            old_content = f.read()
        initialization.replace_line_in_file(self.file2.name, replace, newline)
        with open(self.file2.name, 'rU') as f:
            content = f.read()
        assert_replaced_line(newline, content)
        assert_not_replaced_line(newline, old_content)

    def test_replace_file_contents(self):
        def assert_replace_file_test(new_contents, read_contents):
            self.assertEqual(new_contents, read_contents)

        def assert_not_replace_file_test(new_contents, old_contents):
            self.assertNotEqual(new_contents, old_contents)
        file_content = ['Bah, Bah, Black Sheep', 'Have you any wool?\n']
        content = '\n'.join(file_content)
        with open(self.file2.name, 'rU') as f:
            old_content = f.read()
        initialization.replace_file_content(content, self.file2.name)
        with open(self.file2.name, 'rU') as f:
            read_content = f.read()
        assert_replace_file_test(content, read_content)
        assert_not_replace_file_test(content, old_content)

    def tearDown(self):
        os.remove(self.file1.name)
        os.remove(self.file2.name)


if __name__ == '__main__':
    unittest.main()
