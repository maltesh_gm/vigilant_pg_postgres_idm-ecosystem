#!/usr/bin/env python
import sys
import argparse
import email
import logging

sys.path.append('/usr/lib/celery/')
import phim_onsite.transport


# the following requires python-argparse to be installed
def check_arg():
    parser = argparse.ArgumentParser(description='Postfix filter to transport legacy HB Messages through http pipeline')
    parser.add_argument('--smtpto', required=True,
                        help='The "to" address to use for the message',
                        default='hbeat@stentor.com')
    parser.add_argument('--smtpfrom', required=True,
                        help='The "from" address to use for the message',
                        default='isyntaxserver@stentor.com')
    results = parser.parse_args()
    
    return results.smtpto, results.smtpfrom

    
def execute_post(smtpto, smtpfrom):
    logger = logging.getLogger(__name__)

    try:
        inmail = sys.stdin.read()
        msg = email.message_from_string(inmail)
        mailbody = msg.get_payload()
        phim_onsite.transport.do_hb_msg.delay(smtpto, smtpfrom, mailbody)
    except Exception:
        logger.exception('Problem intercepting email')

    # reinject using sendmail (smtplib may cause a loop)
    # Only necessary if needing to resend the message in email format
    #'''mailer = subprocess.Popen(['/usr/sbin/sendmail', '-G', '-i', '-f', smtpfrom, '--', smtpto], stdin = subprocess.PIPE)
    #mailer.stdin.write(inmail)'''


def main():
    execute_post(*check_arg())

if __name__ == '__main__':
    main()
