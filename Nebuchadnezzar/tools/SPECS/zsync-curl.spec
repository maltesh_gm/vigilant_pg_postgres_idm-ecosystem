# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the module
%global module_name zsync_curl

# Name of the module directory
%global moduledir /usr/local/bin/

# Basic Information
Name:      zsync-curl
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Download files over http(s)

Group:     none
License:   GPL
URL:       https://launchpad.net/~ce-infrastructure/+archive/ubuntu/zsync-curl/
# Packager Information
Packager:  Naveen Penumala <naveen.penumala@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source:    zsync-curl-%{version}.tar.gz

# libcurl is required
Requires:  libcurl

%description
Downloads a file over HTTP(S). zsync uses a control file to determine whether any blocks in the file are already known to the downloader, and only downloads the new blocks.

%prep
%setup -q -c

%build
echo ok

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{_builddir}
%{__install} -d -m 0755 %{buildroot}/%{moduledir}
%{__cp} -r %{_builddir}/%{name}-%{version}/%{name}-%{version}/%{module_name} %{buildroot}/%{moduledir}/%{module_name}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{moduledir}/%{module_name}

%changelog
* Mon Apr 24 2017 Naveen Penumala <naveen.penumala@philips.com>
- Initial Spec File for zsync_curl
