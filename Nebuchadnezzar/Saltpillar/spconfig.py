CONFIGURATION_PATH = '/usr/lib/celery'
CONFIGURATION_OBJECT = 'celeryconfig'
TASK_NAME = 'phim_onsite.operate.gateway'

REDIS = {
    'HOST': 'localhost',
    'PORT': '6379',
    'STATE_PREFIX': 'phistate#',
    'STATE_SET': 'phistate_keys'
}

PCM = {
    'KEY': 'Administrative__Philips__Host__Inforamtion__PCM__State',
    'INSERT_KEY': 'Administrative__Philips__Host__Inforamtion__PCM',
    'DEPLOYMENT_KEY': 'Administrative__Philips__Host__Information__DeploymentId',
}

CANNED1 = {
    'KEYS': [
        'Administrative__Philips__Host__Information__HostName',
        'Virtualization__VMware__Guest__Information__Object_Name',
        'Virtualization__VMware__Guest__Information__Cluster_Name',
        'Virtualization__VMware__Guest__Information__Disks',
    ],
    'MAP': {
        'Administrative__Philips__Host__Information__HostName': 'Hostname',
        'Virtualization__VMware__Guest__Information__Object_Name': 'VMObjectName',
        'Virtualization__VMware__Guest__Information__Cluster_Name': 'VMClusterName',
        'Virtualization__VMware__Guest__Information__Disks': 'VMDK'
    },
    'MULTIPLEX_KEY': 'VMDK'
}

DISK_MOVE_CSV_FIELDS = ['source_vm', 'source_disk', 'source_cluster', 'stack_id','destination_vm', 'destination_disk', 'destination_cluster']

