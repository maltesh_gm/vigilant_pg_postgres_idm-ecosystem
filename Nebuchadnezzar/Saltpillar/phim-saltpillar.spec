# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimuwsgi %{_prefix}/lib/philips/uwsgi/

# Basic Information
Name:      phim-saltpillar
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   phim-saltpillar setup

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:


Requires:  phimutils
Requires:  phim-celery-tasks-client
Requires:  phim-uwsgi
Provides:  phim-saltpillar
Conflicts: phim-gateway

%description
phim-saltpillar setup.


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/uwsgi
%{__cp} %{_builddir}/%{name}-%{version}/uwsgi.saltpillar.ini %{buildroot}%{_sysconfdir}/uwsgi/saltpillar.ini
%{__install} -d -m 0755 %{buildroot}%{phimuwsgi}/saltpillar/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/saltpillar.py %{buildroot}%{phimuwsgi}/saltpillar/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/spconfig.py %{buildroot}%{phimuwsgi}/saltpillar/
%{__cp} -R %{_builddir}/%{name}-%{version}/views %{buildroot}%{phimuwsgi}/saltpillar/


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sysconfdir}/uwsgi/saltpillar.ini
%{phimuwsgi}/saltpillar/


%changelog
* Tue Apr 07 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added uwsgi ini to be picked up by uwsgi emperor

* Tue Mar 03 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added views directory

* Tue Jan 20 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
