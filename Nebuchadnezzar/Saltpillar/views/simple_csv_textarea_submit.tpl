<html>
<head>
    <title>{{title|title}}</title>
</head>
<body style="text-align:center">
<form method="POST" accept="text/csv">
{{name|capitalize}}:<br>
<textarea name="{{name|lower}}" maxlength="{{maxlength|default('1000')}}" cols="{{cols|default('25')}}" rows="{{rows|default('6')}}"></textarea>
<br><br>
<input type="submit" value="Submit">
</form>
</body>
</html>
