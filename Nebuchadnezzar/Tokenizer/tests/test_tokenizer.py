import unittest
from mock import MagicMock, patch


class TokenizerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_bottle = MagicMock(name='bottle')
        self.mock_bottle_redis = MagicMock(name='bottle_redis')
        self.mock_tokenizer = MagicMock(name='tokenizer')
        self.mock_rdb = MagicMock(name='rdb')
        self.mock_request = MagicMock(name='request')
        self.mock_log = MagicMock(name='log')
        self.mock_validator = MagicMock(name='Validator')
        self.mock_token_mgr = MagicMock(name='TokenManager')

        modules = {
            'bottle': self.mock_bottle,
            'tokenizer': self.mock_tokenizer,
            'bottle_redis': self.mock_bottle_redis,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import tokenizer
        self.tokenizer = tokenizer

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_health(self):
        self.tokenizer.health.return_value = 'OK'
        self.assertEqual(self.tokenizer.health(), 'OK')

    def test_register_empty_json(self):
        self.mock_request.json = None
        self.mock_request.body.read.return_value = ''
        self.tokenizer.register.return_value = '{"description": "Invalid json"}'
        #assert self.tokenizer.register(self.mock_rdb) == '{"description": "Invalid json"}'
        self.assertEquals(
            self.tokenizer.register(self.mock_rdb),
            '{"description": "Invalid json"}'
        )

    def test_register_with_all_true(self):
        self.mock_request_json = '''
        {
            "hostname": "0.0.0.0",
            "siteid": "SIT001"
        }
        '''
        self.mock_validator(self.mock_rdb).validate_hs.return_value = (True, None)
        self.mock_validator(self.mock_rdb,
                            T_Mgr=self.mock_token_mgr(self.mock_rdb)
                            ).register_api.return_value = (True,
                                                           "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")
        self.tokenizer.register.return_value = '{"key": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"}'
        self.assertEquals(
            self.tokenizer.register(self.mock_rdb),
            '{"key": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"}'
        )

    def test_get_token_with_all_true(self):
        self.mock_request_json = '''
        {
            "key": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        }
        '''
        self.mock_validator(self.mock_rdb).validate_hs.return_value = (True, None)
        self.mock_token_mgr(self.mock_rdb).get_token.return_value = '''
                        {"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9-tkn"}
                        '''
        self.tokenizer.get_token.return_value = '{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9-tkn"}'
        self.assertEquals(
            self.tokenizer.get_token(self.mock_rdb),
            '{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9-tkn"}'
        )

    def test_post_metrics_data_with_all_true(self):
        mock_attributes = MagicMock()
        mock_celery     = MagicMock()
        self.mock_request_json = '''
        {
            "key": "my-custom-data",
            "timestamp": "my-datetime-in-ISO-format"
        }
        '''
        self.mock_request.headers['Content/Type'] = "application/json"
        self.mock_request.headers['Authorization'] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9-tkn"
        self.mock_validator(self.mock_rdb, T_Mgr=self.mock_token_mgr).validate_token.return_value = (True, ("0.0.0.0","SIT001"))
        mock_attributes["payload"]["hostname"] = "0.0.0.0"
        mock_attributes["payload"]["siteid"] = "SIT001"
        mock_celery.send_task('METRICS_TASK_NAME',[], mock_attributes)
        self.tokenizer.metricsdata_action.return_value = '200 OK'

        self.assertEquals(
            self.tokenizer.metricsdata_action(self.mock_rdb),
            '200 OK'
        )
if __name__ == '__main__':
    unittest.main()
