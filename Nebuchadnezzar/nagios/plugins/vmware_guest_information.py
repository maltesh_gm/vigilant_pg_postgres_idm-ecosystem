#!/usr/bin/env python

import linecache
import uuid
import argparse
import atexit
import sys
import json
from pyVim.connect import SmartConnect, Disconnect
from phimutils.perfdata import INFO_MARKER
from requests.exceptions import ConnectionError

STATES = {0: 'OK', 1: 'WARNING', 2: 'CRITICAL', 3: 'UNKNOWN'}


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-s', '--servers', required=True, help='The server list to connect to')
    parser.add_argument('-u', '--user', required=True, help='The user to connect to')
    parser.add_argument('-p', '--password', required=True, help='The password to connect to')
    parser.add_argument('-H', '--hostaddress', help='The address')
    parser.add_argument('-a', '--hostname', help='The name ')
    parser.add_argument('-U', '--uuid', help='The uuid')
    parser.add_argument('--windows', action='store_true')

    results = parser.parse_args(args)

    return (results.servers,
            results.user,
            results.password,
            results.hostaddress,
            results.hostname,
            results.uuid,
            results.windows)


def get_server(servers):
    if servers.startswith('@'):
        return linecache.getline(servers[1:], 1).strip()
    else:
        return servers.split(',')[0]


def gen_uuid(hostuuid, windows=False):
    try:
        result = uuid.UUID(hostuuid)
    except ValueError:
        return None

    if windows:
        result = uuid.UUID(bytes_le=result.bytes)
    return str(result)


def get_vm(si, hostname, hostaddress, hostuuid, windows=False):
    vm = None
    if hostuuid:
        clean_uuid = gen_uuid(hostuuid, windows)
        vm = si.content.searchIndex.FindByUuid(None, clean_uuid, True, False)
        if vm:
            return vm

    if hostaddress:
        vm = si.content.searchIndex.FindByIp(None, hostaddress, True)
        if vm:
            return vm

    if hostname:
        vm = si.content.searchIndex.FindByDnsName(None, hostname, True)
        if vm:
            return vm


def get_information(vm):
    try:
        cluster_name = vm.runtime.host.parent.name
    except AttributeError:
        cluster_name = ''
    vmdks = json.dumps([disk.diskFile[0] for disk in vm.layout.disk])
    info_template = '''{marker} Object_Name="{object_name}"; Cluster_Name="{cluster_name}"; Disks='{vmdks}' '''
    return info_template.format(marker=INFO_MARKER, object_name=vm.name, cluster_name=cluster_name, vmdks=vmdks)


def vmware_guest_information(servers, user, password, hostaddress, hostname, uuid, windows):
    server = get_server(servers)
    si = None
    try:
        si = SmartConnect(host=server, user=user, pwd=password)
        atexit.register(Disconnect, si)
    except (IOError, AttributeError, ConnectionError):
        pass

    if not si:
        return 3, 'Problem establishing connection to server "%s"' % server

    vm = get_vm(si, hostname, hostaddress, uuid, windows)
    if not vm:
        return 3, 'Could not find VM.'

    return 0, get_information(vm)


def main():
    try:
        state, output = vmware_guest_information(*check_arg())
    except Exception:
        state, output = 3, 'Error.'
    output_template = '{state} - {output}'
    print(output_template.format(state=STATES[state], output=output))
    sys.exit(state)


if __name__ == '__main__':
    main()
