#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post
# This plugin is uesful to get status of heartbeat of object storage.

import argparse
import requests
import sys
import os
import time
from scanline.utilities.hsdp_obs import OBJSTConnection


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script get status of IIS Application Pool')
    parser.add_argument('-c', '--container_name', default='IDM_TEST',
                        help='The name of the container')
    parser.add_argument('-f', '--filename',
                        help='The name of the file', default='idm_check.txt')
    results = parser.parse_args(args)

    return (
        results.container_name,
        results.filename)


def get_stored_data():
    try:
        obj = OBJSTConnection()
        return obj.to_dict()
    except Exception:
        return {}


def create_file_to_post(file_name):
    with open(file_name, 'w+') as f:
        f.write('This is a sample file, posted by IDM to check OBS HeartBeat.')


def delete_file_from_disk(file_name):
    try:
        os.remove(file_name)
    except OSError:
        pass


def create_container(stored_data, container_name):
    try:
        url = "{0}/{1}/".format(stored_data['obs_url'], container_name)
        headers = {
            'x-auth-token': stored_data['token_id'],
            'content-type': "application/json",}
        response = requests.get(url, headers=headers, verify=False)
        if response.status_code == 404:
            put_response = requests.put(url, headers=headers, verify=False)
            if put_response.status_code in [200, 201, 204]:
                return True, 0, 'Container created successfully.'
        elif response.status_code in [200, 201, 204]:
            return True, 0, 'Container created successfully.'
        else:
            return False, 2, 'Could not create container, status code: ' \
                             '{0}'.format(response.status_code)
    except Exception:
        return False, 2, 'Invalid put request, could not create container.'


def put_file_to_container(stored_data, file_name, container_name, container_created):
    if container_created:
        url = '{0}/{1}/{2}'.format(stored_data['obs_url'], container_name, file_name)
        headers = {
            'x-auth-token': stored_data['token_id'],
            'application-type': 'REST',}
        with open(file_name,'rb') as payload:
            response = requests.put(url, data=payload, verify=False, headers=headers)
            if response.status_code == 201:
                return True, 0, 'File posted successfully.'
            else:
                return False, 2, 'Could not post file, status code: ' \
                                 '{0}'.format(response.status_code)
    else:
        return False, 2, 'Invalid put request, could not post file.'


def manipulate_file_on_obs(stored_data, file_posted, container_name, file_name, method, file_found):
    url = '{0}/{1}/{2}'.format(stored_data['obs_url'], container_name, file_name)
    headers = {'x-auth-token': stored_data['token_id'],}
    time.sleep(10)
    response = method(url, headers=headers, verify=False)
    if method.__name__ == 'get':
        if file_posted:
            if response.status_code == 200:
                return True, 0, 'File found on OBS.'
            else:
                return False, 2, 'File not found on OBS, status code: ' \
                                 '{0}'.format(response.status_code)
        else:
            return False, 2, 'Invalid get request, could not find file on OBS.'
    elif method.__name__ == 'delete':
        if file_found:
            if response.status_code == 204:
                return True, 0, 'File successfully deleted.'
            else:
                return False, 2, 'Could not delete file, status code: ' \
                                 '{0}'.format(response.status_code)
        else:
            return False, 2, 'Invalid delete request, could not delete file from OBS.'


def delete_container(stored_data, file_deleted, container_name):
    if file_deleted:
        url = '{0}/{1}'.format(stored_data['obs_url'], container_name)
        headers = {'x-auth-token': stored_data['token_id'],}
        response = requests.delete(url, headers=headers, verify=False)
        if response.status_code == 204:
            return 0, 'Object Store Heartbeat is up.'
        else:
            return 2, 'Could not delete container, status code: ' \
                      '{0}'.format(response.status_code)
    else:
        return 2, 'Invalid delete request, could not delete container.'


def main():
    container_name, file_name = check_args()
    stored_data = get_stored_data()
    if stored_data:
        create_file_to_post(file_name)
        time.sleep(10)
        container_created, state, message = create_container(stored_data, container_name)
        if not container_created:
            print message
            sys.exit(state)
        file_posted, state, message = put_file_to_container(stored_data, file_name,
                                                            container_name,
                                                            container_created)
        if not file_posted:
            print message
            sys.exit(state)
        file_found, state, message = manipulate_file_on_obs(stored_data,
                                                            file_posted,
                                                            container_name,
                                                            file_name,
                                                            requests.get, False)
        if not file_found:
            print message
            sys.exit(state)
        file_deleted, state, message = manipulate_file_on_obs(stored_data,
                                                              file_posted,
                                                              container_name,
                                                              file_name,
                                                              requests.delete, file_found)
        if not file_deleted:
            print message
            sys.exit(state)
        time.sleep(10)
        state, message = delete_container(stored_data, file_deleted, container_name)
        delete_file_from_disk(file_name)
        print message
        sys.exit(state)
    else:
        print 'Could not connect UDM Database.'
        sys.exit(2)


if __name__ == '__main__':
    main()
