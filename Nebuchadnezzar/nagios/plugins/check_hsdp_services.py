#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import requests
import argparse
import hashlib
from scanline.utilities.hsdp_obs import KEY_HEX_ARY, IV_HEX_ARY, HSDPConnection, TIERConnection
from scanline.utilities.iamauth import UserLogin
from scanline.utilities.encryption import AESCipher
from scanline.utilities.proxy import get_proxy

PAYLOAD_PART_HELP = 'A label and JSON string to be used as part of the payload'
STATE_CHOICES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-s', '--service', required=True, help='argument to post')
    results = parser.parse_args(args)
    return results.service

def iam_status():
    hsdp_obj = HSDPConnection()
    hsdp_keys = hsdp_obj.to_dict()
    user = hsdp_keys['UserName']
    password = hsdp_keys['PassKey']
    iam_url = hsdp_keys['CredentialStoreURL']
    secret_key = hsdp_keys['Secretkey']
    shared_key = hsdp_keys['SharedKey']

    aes = AESCipher(
        key_byte_str = AESCipher.hex_ary_to_byte_str(KEY_HEX_ARY),
        iv_byte_str  = AESCipher.hex_ary_to_byte_str(IV_HEX_ARY)
    )
    password = aes.decrypt(password)
    obj = UserLogin(iam_url, secret_key, shared_key, user, password)
    obj.post()
    return obj.get_status(), obj.get_headers(), obj.get_response()

def get_svc_status(service, urltype='ServiceUrl'):
    request_session = requests.session()
    tier_obj = TIERConnection()
    tierconfig_keys = tier_obj.to_dict()
    uri = tierconfig_keys[service][urltype]

    status, headers, response = iam_status()
    access_token = response['exchange']['authentication']['accessToken']
    headers['x-token'] = access_token
    proxy = get_proxy()
    if proxy:
        request_session.proxies.update(proxy)
    res = request_session.options(uri, headers=headers, verify=False)
    res.raise_for_status()
    return res.status_code, res.headers, res.content

def sap_status():
    return get_svc_status('CFT')

def backup_status():
    return get_svc_status('CBT')

def archive_status():
    return get_svc_status('CDT')

def ssm_status():
    return get_svc_status('CFT', urltype='ProxyUrl')

def get_s3_status(service, urltype='ServiceUrl'):
    request_session = requests.session()
    data = 'Object Content'
    md5 = hashlib.md5(data).hexdigest()
    tier_obj = TIERConnection()
    tierconfig_keys = tier_obj.to_dict()
    uri = tierconfig_keys[service][urltype] + '/object/testObject'
    status, headers, response = iam_status()
    headers.pop('Content-Type')
    headers.pop('Accept')
    access_token = response['exchange']['authentication']['accessToken']
    headers['x-token'] = str(access_token)
    headers['phs-dp-content-checksum'] = md5
    headers['content-type'] = 'text/plain'
    headers['authorization'] = 'notSupportedYet'
    headers['location'] = 'Location'
    headers['if-Match'] = md5
    proxy = get_proxy()
    if proxy:
        request_session.proxies.update(proxy)
    res = request_session.post(uri, data=data , headers=headers)
    res.raise_for_status()
    res = request_session.delete(uri, data=data , headers=headers)
    res.raise_for_status()
    return res.status_code, res.headers, res.content

def s3object_status():
    return get_s3_status('CFT')

def get_hsdp_result(argument):
    hsdp_service = {'IAM': iam_status,
                    'SAP': sap_status,
                    'SAPBACKUP': backup_status,
                    'SAPARCHIVE': archive_status,
                    'SSM': ssm_status,
                    'S3':s3object_status}
    if not argument:
        return 2, 'Could not post due to missing arguments'
    try:
        res, header, body = hsdp_service[argument]()
        status = 0
        outmsg = '{res} - Service is up'.format(res=res)
    except Exception:
        outmsg = 'could not post due to request error'
        status = 2
    return status, outmsg

def main():
    arg = check_arg()
    state, msg = get_hsdp_result(arg)
    print(msg)
    exit(state)


if __name__ == '__main__':
    main()
