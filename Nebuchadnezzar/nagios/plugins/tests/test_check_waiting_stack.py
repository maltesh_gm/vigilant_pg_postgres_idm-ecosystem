import unittest
from requests.exceptions import HTTPError
from mock import MagicMock, patch


class CheckTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_dbquery = MagicMock(name='test')
        self.mock_query = MagicMock(name='test')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'requests': self.mock_request,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.db': self.mock_scanline.utilities.db,
            'scanline.utilities.db.QueryMgr': self.mock_query,
            'scanline.utilities': self.mock_scanline.utilities,
            'check_waiting_stack.DBQueryMgr': self.mock_dbquery,
            'phimutils.resource': self.mock_phimutils.resource,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_waiting_stack
        self.check_waiting_stack = check_waiting_stack
        from check_waiting_stack import get_waiting_stack
        self.get_waiting_stack = get_waiting_stack
        from check_waiting_stack import DBQueryMgr
        self.DBQueryMgr = DBQueryMgr()
        from scanline.utilities.db import QueryMgr
        self.QueryMgr = QueryMgr()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('check_waiting_stack.DBQueryMgr')
    def test_waiting_stack(self, mock_db):
        data = ['''6AUAAHic1VRLboMwEN1X6h24AIGEUEoUIUUlCxbJIp8D+DOJrDqmJSZqbl8DJjaQtLtK3S
                  DNmzczz8+D51tZIAnH61suDuxYqoDlInl+cpz5opR5ChwktKQaV5ktFBcoVjmFxJ97VtTm
                  NX93/YBkqhh2rDmpnpWJFROlhHPVagjWSrwHUuaLC2IcYd7CTFdYGnRsIWt0gmQLnyUIyR
                  A38urEgL4U1QCajA2xhQbc6oBNe6ma58Wo8WZk5I3M4LvWd826ryjl/JchlFvHaumDPpmQ
                  UBwQgWT5BUTZbWpMStvpDXbgZ383eSnoJsdM/LG/ZvA/9ldt/IPNtlcekffButeoLUWFe0
                  YTHGL6giPsTgiE7jSKAhdFh9iNY0BBGAeYTEglQtNvDbJzjWWC8JICzW6uVvf1ONmvXxDJ
                  LmDVaKDP24Bkhbp5/0ZskT5zL8pzh6gBc/T6RyOwLk8Yiuah6iBdk2pBSjxN1WfH1H36r5
                  4/9ib+OHL8eBZMZ2GkHRpyu7205jbreLZ645Z51Zpj3M3098NccGdJ9C4YXvdB/waxZfMy''']
        mock_db.return_value.execute.return_value = data
        self.assertEqual(self.check_waiting_stack.waiting_stack(), 0)

    @patch('check_waiting_stack.DBQueryMgr')
    def test_waiting_stack_warning(self, mock_db):
        data = ['''xgUAAHic1VRLboMwEN33FLkAgYRQSoSQopIFi2SRzwH8mURWiWmJiZrb12DAxqHtrlI3SPPm
                  eeb5zeB4L0ok4Hx/LfiJnSsZsIInT5NJvKpEkUIOAjpODcvEHsoblJuCQuLFrhG16ZZ9uL9D
                  spAEM1aUtO2T8Q3jlYBrXegRrFW44zLi1Q2xHOG8Q5niG/1VaABbdIFkDx8VcMFQrpU1CZu9
                  5nV1msw0r4Nsan0zVVzI0kU5VZ5MtbSpbjtq+NClUT1pnv/Sg+bGnTq6XSbjAsoTIpCsP4FI
                  m/URnVJGuvbcfzR2V1Sc7grM+J8aq9v+U2Plho+vsrniiLzZ692AhgoZHRlNcIDpMw6xMycQ
                  OIsw9B0UniInigD5QeRjMie1gJbenc+uDZRxklcUaNa7WY/p+6R1fEUEu4FxpAUs2g4EK+W4
                  vZ7XIRbxyKur2pQh0N+6+asIbKsLhlK9RgNkYE8jRuqmqfwcmJyh9+J6M3fuzcKJFy39xTII
                  W28euYNSrd4uOXEN5dom/XSpq45mrJXoxzrYCzV/zbJe6y9Ri/F2''']
        mock_db.return_value.execute.return_value = data
        self.assertEqual(self.check_waiting_stack.waiting_stack(), 1)

    @patch('check_waiting_stack.DBQueryMgr')
    def test_waiting_stack_exception(self, mock_db):
        data = 123
        mock_db.return_value.execute.return_value = data
        self.assertEqual(self.check_waiting_stack.get_waiting_stack(), (3, 'Could not connect to UDM Database.'))

if __name__ == '__main__':
    unittest.main()

