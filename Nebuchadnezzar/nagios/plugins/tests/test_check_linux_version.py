import sys
import unittest
from mock import MagicMock, patch

sys.modules['platform'] = MagicMock(name='platform')
sys.modules['phimutils.perfdata'] = MagicMock(name='phimutils.perfdata')
sys.modules['phimutils'] = MagicMock(name='phimutils')

import check_linux_version


class PostNotificationTest(unittest.TestCase):
    def setUp(self):
        sys.modules['phimutils.perfdata'].INFODATA = '++#I'
        unittest.TestCase.setUp(self)

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    @patch('check_linux_version.platform')
    def test_check_xmlelement_request_exception(self, mock_platform):
        mock_platform.linux_distribution.side_effect = Exception('some rror')
        self.assertEqual(
            check_linux_version.check_linux_version(),
            (3, 'UNKNOWN'))

    @patch('check_linux_version.platform')
    def test_check_xmlelement_request_exception(self, mock_platform):
        mock_platform.linux_distribution.return_value = ('UberOS', '4.4', 'Turbo')
        self.assertEqual(
            check_linux_version.check_linux_version(),
            (0, 'UberOS 4.4 (Turbo)'))


if __name__ == '__main__':
    unittest.main()

