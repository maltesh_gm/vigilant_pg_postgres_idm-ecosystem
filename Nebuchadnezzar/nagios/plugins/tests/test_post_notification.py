import unittest
from mock import MagicMock, patch


class PostNotificationTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'requests': self.mock_request,
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'phimutils.argument': self.mock_phimutils.argument,
            'phimutils.collection': self.mock_phimutils.collection,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import post_notification
        self.post_notification = post_notification

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('post_notification.requests')
    def test_post_notification_request_exception(self, mock_requests):
        class MyException(Exception):
            pass
        mock_requests.exceptions.RequestException = MyException
        mock_requests.post.side_effect = MyException('some http error')
        self.assertEqual(
            self.post_notification.post_notification(
                'url',
                'hostname',
                'hostaddress',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                'siteid',
                'siteid_file'
            ),
            (0, 'But could not post due to request error')
        )
        self.post_notification.Message.assert_called_once_with(
            epoch_time='timestamp',
            hostaddress='hostaddress',
            hostname='hostname',
            payload={'output': 'output'},
            perfdata='perfdata',
            service='description',
            siteid='siteid',
            state='state',
            type='notificationtype'
        )
        mock_requests.post.assert_called_once_with(
            'url',
            json=self.post_notification.Message.return_value.to_dict.return_value,
            verify=True
        )

    @patch('post_notification.linecache')
    def test_post_notification_no_siteid(self, mock_linecache):
        mock_linecache.getline.return_value = ''
        self.assertEqual(
            self.post_notification.post_notification(
                'url',
                'hostname',
                'hostaddress',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                '',
                'siteid_file'
            ),
            (0, 'But could not post due to missing siteid')
        )

    @patch('post_notification.requests')
    def test_post_notification_post_ok(self, mock_requests):
        self.assertEqual(
            self.post_notification.post_notification(
                'url',
                'hostname',
                'hostaddress',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                'siteid',
                'siteid_file'
            ),
            (0, 'Posted')
        )
        self.post_notification.Message.assert_called_once_with(
            epoch_time='timestamp',
            hostaddress='hostaddress',
            hostname='hostname',
            payload={'output': 'output'},
            perfdata='perfdata',
            service='description',
            siteid='siteid',
            state='state',
            type='notificationtype'
        )
        mock_requests.post.assert_called_once_with(
            'url',
            json=self.post_notification.Message.return_value.to_dict.return_value,
            verify=True
        )


if __name__ == '__main__':
    unittest.main()

