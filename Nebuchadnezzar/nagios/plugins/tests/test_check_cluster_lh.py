import unittest
from mock import patch
from collections import defaultdict


import check_cluster_lh


class CheckClusterLHOverLevelTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.items = defaultdict(list)

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_empty(self):
        self.assertEqual(check_cluster_lh.over_level(self.items, 1), (0, ''))

    def test_empty_2(self):
        self.assertEqual(check_cluster_lh.over_level(self.items, 2), (0, ''))

    def test_one_warning_level_critical(self):
        self.items[1].append('service1')
        self.assertEqual(check_cluster_lh.over_level(self.items, 2), (0, ''))

    def test_one_warning_level_warning(self):
        self.items[1].append('service1')
        self.assertEqual(check_cluster_lh.over_level(self.items, 1), (1, '[ service1 ]'))

    def test_one_critical_level_warning(self):
        self.items[2].append('service1')
        self.assertEqual(check_cluster_lh.over_level(self.items, 1), (1, '[ service1 ]'))

    def test_two_critical_level_warning(self):
        self.items[2] = ['service1', 'service2']
        self.assertEqual(check_cluster_lh.over_level(self.items, 1), (2, '[ service1, service2 ]'))

    def test_two_critical_level_critical(self):
        self.items[2] = ['service1', 'service2']
        self.assertEqual(check_cluster_lh.over_level(self.items, 2), (2, '[ service1, service2 ]'))

    def test_two_each_level_critical(self):
        self.items[0] = ['service1', 'service2']
        self.items[1] = ['service3', 'service4']
        self.items[2] = ['service5', 'service6']
        self.items[3] = ['service7', 'service8']
        self.assertEqual(check_cluster_lh.over_level(self.items, 2), (2, '[ service5, service6 ]'))

    def test_two_each_level_warning(self):
        self.items[0] = ['service1', 'service2']
        self.items[1] = ['service3', 'service4']
        self.items[2] = ['service5', 'service6']
        self.items[3] = ['service7', 'service8']
        self.assertEqual(check_cluster_lh.over_level(self.items, 1), (4, '[ service5, service6, service3, service4 ]'))


class CheckClusterLHLevelGroupsFromListTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_empty(self):
        self.assertEqual(check_cluster_lh.level_groups_from_list([]), {})

    def test_two_groups(self):
        self.assertEqual(
            check_cluster_lh.level_groups_from_list([(0, 'service1'), (3, 'service2')]),
            {0: ['service1'], 3: ['service2']}
        )

    def test_one_of_each_groups(self):
        self.assertEqual(
            check_cluster_lh.level_groups_from_list(
                [(0, 'service1'), (3, 'service2'), (1, 'service3'), (2, 'service4')]
            ),
            {0: ['service1'], 1: ['service3'], 2: ['service4'], 3: ['service2']}
        )

    def test_two_of_each_groups(self):
        self.assertEqual(
            check_cluster_lh.level_groups_from_list(
                [(0, 'service1'), (3, 'service2'), (1, 'service3'), (2, 'service4'),
                 (0, 'service5'), (3, 'service6'), (1, 'service7'), (2, 'service8')]
            ),
            {0: ['service1', 'service5'], 1: ['service3', 'service7'],
             2: ['service4', 'service8'], 3: ['service2', 'service6']}
        )


class CheckClusterLHOverviewMessageTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.items = defaultdict(list)

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_empty(self):
        self.assertEqual(check_cluster_lh.overview_msg(self.items), '0 ok, 0 warning, 0 unknown, 0 critical')

    def test_one(self):
        self.items[1].append('service1')
        self.assertEqual(check_cluster_lh.overview_msg(self.items), '0 ok, 1 warning, 0 unknown, 0 critical')

    def test_two_of_each(self):
        self.items[0] = ['service1', 'service2']
        self.items[1] = ['service3', 'service4']
        self.items[2] = ['service5', 'service6']
        self.items[3] = ['service7', 'service8']
        self.assertEqual(check_cluster_lh.overview_msg(self.items), '2 ok, 2 warning, 2 unknown, 2 critical')


class CheckClusterLHTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.patch_over_level = patch('check_cluster_lh.over_level')
        self.filter_over_mock = self.patch_over_level.start()
        self.patch_overview = patch('check_cluster_lh.overview_msg')
        self.filter_overview = self.patch_overview.start()
        self.filter_overview.return_value = '#OVERVIEW#'
        self.patch_level_groups = patch('check_cluster_lh.level_groups_from_list')
        self.filter_level_groups = self.patch_level_groups.start()
        self.filter_level_groups.return_value = '#LEVEL_GROUPS#'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.patch_over_level.stop()
        self.patch_overview.stop()
        self.patch_level_groups.stop()

    def test_no_thresholds(self):
        self.filter_over_mock.return_value = 8, 'CRITICAL'
        out = check_cluster_lh.check_cluster_lh('label x', None, None, 1, [])
        self.assertEqual(out, (0, 'label x: #OVERVIEW# CRITICAL'))

    def test_no_critical(self):
        self.filter_over_mock.return_value = 0, ''
        out = check_cluster_lh.check_cluster_lh('label x', 0, 2, 1, [])
        self.assertEqual(out, (0, 'label x: #OVERVIEW#'))

    def test_critical_not_greater_warning_greater(self):
        self.filter_over_mock.return_value = 2, 'CRITICAL'
        out = check_cluster_lh.check_cluster_lh('', 0, 2, 1, [])
        self.assertEqual(out, (1, '#OVERVIEW# CRITICAL'))

    def test_critical_greater(self):
        self.filter_over_mock.return_value = 4, 'CRITICAL'
        out = check_cluster_lh.check_cluster_lh('', None, 3, 1, [])
        self.assertEqual(out, (2, '#OVERVIEW# CRITICAL'))

    def test_warning(self):
        self.filter_over_mock.return_value = 3, 'CRITICAL'
        out = check_cluster_lh.check_cluster_lh('', 2, None, 1, [])
        self.assertEqual(out, (1, '#OVERVIEW# CRITICAL'))

    def test_critical_over_warning(self):
        self.filter_over_mock.return_value = 5, 'CRITICAL'
        out = check_cluster_lh.check_cluster_lh('', 2, 4, 1, [])
        self.assertEqual(out, (2, '#OVERVIEW# CRITICAL'))


if __name__ == '__main__':
    unittest.main()
