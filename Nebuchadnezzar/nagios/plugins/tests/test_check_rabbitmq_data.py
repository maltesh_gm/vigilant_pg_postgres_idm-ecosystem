import unittest
import requests
from mock import MagicMock, patch, Mock


class CheckRabbitmqDataTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_json = MagicMock(name='json')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'requests': self.mock_request,
            'phimutils': self.mock_phimutils,
            'json': self.mock_json,
            'phimutils.message': self.mock_phimutils.message,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rabbitmq_data
        self.check_rabbitmq_data = check_rabbitmq_data
        from check_rabbitmq_data import get_service_status
        from check_rabbitmq_data import get_watermark_status
        from check_rabbitmq_data import get_channel_obj_status
        from check_rabbitmq_data import get_partitions_status
        self.get_service_status = get_service_status
        self.get_watermark_status = get_watermark_status
        self.get_channel_obj_status = get_channel_obj_status
        self.get_partitions_status = get_partitions_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('check_rabbitmq_data.json')
    def test_get_service_status_channel(self, mock_json):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        mock_json.loads.return_value = content
        self.assertEqual(self.get_service_status('localhost', 15671, 'guest',
                                                 'guest', 'https', 'channels',
                                                 None, 10, 20, 15),
                         (0, 'Gathered Object Counts: channel=1'))

    @patch('check_rabbitmq_data.json')
    def test_get_service_status_watermark(self, mock_json):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        mock_json.loads.return_value = content
        self.assertEqual(self.get_service_status('localhost', 15671, 'guest',
                                                 'guest', 'https', 'nodes',
                                                 'watermark', 10, 20, 15),
                         (0, 'Watermark is OK, disk alarm and memory alarm are False.'))

    @patch('check_rabbitmq_data.json')
    def test_get_service_status_partitions(self, mock_json):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        mock_json.loads.return_value = content
        self.assertEqual(self.get_service_status('localhost', 15671, 'guest', 'guest',
                                                 'https', 'nodes', 'partitions',
                                                 10, 20, 15),
                         (0, 'No network partitions.'))

    @patch('check_rabbitmq_data.requests.get')
    def test_get_service_status_exception(self, mock_get):
        mock_get.side_effect = requests.exceptions.ConnectionError
        self.assertEqual(self.get_service_status('localhost', 15671, 'guest', 'guest',
                                                 'https', 'nodes', 'partitions',
                                                 10, 20, 15),
                         (2, 'Could not connect to RabbitMQ server.'))

    def test_get_watermark_status_mem_alarm(self):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": True,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        self.assertEqual(self.get_watermark_status(content),
                         (2, 'Watermark is critical, memory alarm is True.'))

    def test_get_watermark_status_disk_free_alarm(self):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": True,
                "proc_used": 200
              }
            ]
        self.assertEqual(self.get_watermark_status(content),
                         (2, 'Watermark is critical, disk alarm is True.'))

    def test_get_watermark_status_ok(self):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        self.assertEqual(self.get_watermark_status(content),
                         (0, 'Watermark is OK, disk alarm and memory alarm are False.'))

    def test_get_channel_obj_status_ok(self):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        self.assertEqual(self.get_channel_obj_status(content, 5, 10),
                         (0, 'Gathered Object Counts: channel=1'))

    def test_get_channel_obj_status_warn(self):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        self.assertEqual(self.get_channel_obj_status(content, 0, 5),
                         (1, 'Gathered Object Counts: channel=1'))

    def test_get_channel_obj_status_critical(self):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        self.assertEqual(self.get_channel_obj_status(content, 0, 1),
                         (2, 'Gathered Object Counts: channel=1'))

    def test_get_partitions_status_ok(self):
        content = [
              {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        self.assertEqual(self.get_partitions_status(content),
                         (0, 'No network partitions.'))

    def test_get_partitions_status_critical(self):
        content = [
              {
                "partitions": [1],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
              }
            ]
        self.assertEqual(self.get_partitions_status(content),
                         (2, 'Network partitions detected: 1'))


if __name__ == '__main__':
    unittest.main()
