>>> import hb_keepalived
>>> import minimock

>>> minimock.mock('hb_keepalived.ifaddresses', 
...     returns={
...         17: [{'broadcast': 'ff:ff:ff:ff:ff:ff', 'addr': '00:50:56:92:22:f4'}], 
...         2: [{'broadcast': '1.2.34.255', 'netmask': '255.255.255.0', 'addr': '1.2.34.5'}], 
...         10: [{'netmask': 'ffff:ffff:ffff:ffff::', 'addr': 'fe80::250:56ff:fe92:22f4%eth0'}]
...     })

>>> hb_keepalived.get_ip_address('eth0')
Called hb_keepalived.ifaddresses('eth0')
'1.2.34.5'


>>> minimock.mock('hb_keepalived.ifaddresses', 
...     returns={
...         17: [{'broadcast': 'ff:ff:ff:ff:ff:ff', 'addr': '00:50:56:92:22:f4'}], 
...         10: [{'netmask': 'ffff:ffff:ffff:ffff::', 'addr': 'fe80::250:56ff:fe92:22f4%eth0'}]
...     })

>>> hb_keepalived.get_ip_address('eth0')
Called hb_keepalived.ifaddresses('eth0')
'fe80::250:56ff:fe92:22f4%eth0'


>>> minimock.mock('hb_keepalived.ifaddresses', raises=ValueError)
>>> hb_keepalived.get_ip_address('eth0')
Called hb_keepalived.ifaddresses('eth0')
interface not found
'0.0.0.0'


>>> minimock.restore()


>>> print hb_keepalived.create_msg('2013-03-23', '1.2.3.3', 'tester', 'none')
<?xml version="1.0" encoding="utf-16"?>
<MESSAGE_ROOT>
    <VERSION>1.0</VERSION>
    <CATEGORY>LM_HEARTBEAT</CATEGORY>
    <COMPONENT>HeartBeat</COMPONENT>
    <SITE_NAME> tester </SITE_NAME>
    <LOCAL_HOST> none </LOCAL_HOST>
    <CLUSTER>NO</CLUSTER>
    <ISYNTAX_SERVER_VERSION>1,1,1,1</ISYNTAX_SERVER_VERSION>
    <FILTER_CONFIG_VERSION>1.0.1.0</FILTER_CONFIG_VERSION>
    <LOCATION>Main Location</LOCATION>
    <BODY>
        <HB_MESSAGE>
            <SITE_NAME> tester </SITE_NAME>
            <SITE_STATUS>0000</SITE_STATUS>
            <REPORTING_INTERVAL>15</REPORTING_INTERVAL>
            <BEAT_NUMBER>1</BEAT_NUMBER>
            <TIMESTAMP> 2013-03-23 </TIMESTAMP>
            <MODULES>
                <MODULE>
                    <TYPE>NAGIOS</TYPE>
                    <HOSTNAME> none </HOSTNAME>
                    <MONITOR_RESULTS></MONITOR_RESULTS>
                    <QUEUE_STATS></QUEUE_STATS>
                    <DRIVE_SPACE></DRIVE_SPACE>
                    <IP_ADDRESSES> 1.2.3.3 </IP_ADDRESSES>
                    <STATUS>0000</STATUS>
                </MODULE>
            </MODULES>
        </HB_MESSAGE>
    </BODY>
</MESSAGE_ROOT>


>>> minimock.mock('hb_keepalived.smtplib.SMTP', returns=minimock.Mock('smtp_connection'))
>>> msg = hb_keepalived.create_msg('2014-04-24', '10.2.3.34', 'som tin wong', 'site04')
>>> hb_keepalived.send_message(msg, 'test@fakehost.example.com', 'someone@fakehost.example.com', 'superfake.example.com')
Called hb_keepalived.smtplib.SMTP('superfake.example.com')
Called smtp_connection.sendmail(
    'test@fakehost.example.com',
    'someone@fakehost.example.com',
    'Content-Type: text/plain; charset="us-ascii"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\nFrom: test@fakehost.example.com\nTo: someone@fakehost.example.com\n\n<?xml version="1.0" encoding="utf-16"?>\n<MESSAGE_ROOT>\n    <VERSION>1.0</VERSION>\n    <CATEGORY>LM_HEARTBEAT</CATEGORY>\n    <COMPONENT>HeartBeat</COMPONENT>\n    <SITE_NAME> som tin wong </SITE_NAME>\n    <LOCAL_HOST> site04 </LOCAL_HOST>\n    <CLUSTER>NO</CLUSTER>\n    <ISYNTAX_SERVER_VERSION>1,1,1,1</ISYNTAX_SERVER_VERSION>\n    <FILTER_CONFIG_VERSION>1.0.1.0</FILTER_CONFIG_VERSION>\n    <LOCATION>Main Location</LOCATION>\n    <BODY>\n        <HB_MESSAGE>\n            <SITE_NAME> som tin wong </SITE_NAME>\n            <SITE_STATUS>0000</SITE_STATUS>\n            <REPORTING_INTERVAL>15</REPORTING_INTERVAL>\n            <BEAT_NUMBER>1</BEAT_NUMBER>\n            <TIMESTAMP> 2014-04-24 </TIMESTAMP>\n            <MODULES>\n                <MODULE>\n                    <TYPE>NAGIOS</TYPE>\n                    <HOSTNAME> site04 </HOSTNAME>\n                    <MONITOR_RESULTS></MONITOR_RESULTS>\n                    <QUEUE_STATS></QUEUE_STATS>\n                    <DRIVE_SPACE></DRIVE_SPACE>\n                    <IP_ADDRESSES> 10.2.3.34 </IP_ADDRESSES>\n                    <STATUS>0000</STATUS>\n                </MODULE>\n            </MODULES>\n        </HB_MESSAGE>\n    </BODY>\n</MESSAGE_ROOT>')
Called smtp_connection.quit()



