import unittest
from requests.exceptions import HTTPError
from mock import MagicMock, patch
from scanline.utilities.iamauth import UserLogin
from scanline.utilities.encryption import AESCipher


class CheckTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_time = MagicMock(name='time')
        self.mock_base64 = MagicMock(name='base64')
        self.mock_hmac = MagicMock(name='hmac')
        self.mock_hashlib = MagicMock(name='hashlib')
        self.mock_scanline = MagicMock(name='scanline')
        #self.UserMock = MagicMock(name='cde')
        modules = {
            'time': self.mock_time,
            'requests': self.mock_request,
            'scanline': self.mock_scanline,
            #'scanline.utilities': self.mock_scanline.utilities,
            #'scanline.utilities.iamauth': self.mock_scanline.iamauth,
            #'scanline.utilities.iamauth.UserLogin': self.UserMock
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import iamauth
        self.module = iamauth
        self.UserLogin = self.module.UserLogin
        from scanline.utilities.iamauth import UserLogin
        secret_key = 'UT/7eVRccYe9IOrna7EJdIwNSZZ6X55QLb+daeGcSA8='
        shared_key = 'yluKLXeeKKfX/Xuce5g2P7Cpl/wSDjfcONbYiGFkZ38='
        self.user_login = UserLogin('http://test', secret_key, shared_key, 'test', 'auth')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_payload(self):
        self.assertEqual(self.user_login.get_payload(), {"password": 'auth', "loginId": 'test'})

    def test_get_headers(self):
        self.module.time.strftime = MagicMock('strftime',return_value='2017-09-01T06:23:09Z')
        self.user_login.sig_header = MagicMock('sig_header', return_value='test')
        head = {
                'hsdp-api-signature': 'test',
                'Content-Type': 'application/json',
                'SignedDate': '2017-09-01T06:23:09Z',
                'Accept': 'application/json'
               }
        self.assertEqual(self.user_login.get_headers(), head)

    def test_get_url(self):
        self.assertEqual(self.user_login.get_url(), 'http://test/security/authentication/token/login')

    def test_create_hmac(self):
        data = 'test'
        secret = 'abc'
        dig_val = '\xd7\x96W\x9a\xed\x12>{t<\xca\xf5\xb1P\xaf\xfa\x12#\xe3\x1e\xcb\xa8\xb8\x8c\x9d\xa9\xcc\xf7\xad^\x05\x94'
        self.assertEqual(self.user_login.create_hmac(secret, data), dig_val)

    @patch("scanline.utilities.iamauth.HSDPIAMUser.request")
    def test_get(self, mock_post):
        mock_post.post.json.return_value = {'name': 'test'}
        self.assertEqual(mock_post.post.json.return_value, {'name': 'test'})


if __name__ == '__main__':
    unittest.main()