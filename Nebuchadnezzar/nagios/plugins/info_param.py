#!/usr/bin/env python

import argparse
import sys
from subprocess import call
from phimutils.perfdata import info2dict


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to run a command replacing parameters with information parsed from phim style information strings')
    parser.add_argument('-I', '--information', help='the input to parse for information', required=True)
    parser.add_argument('-C', '--command', help='the command to run', nargs=argparse.REMAINDER, required=True)
    results = parser.parse_args(args)
    return results.information, results.command


def info_param(information, command):
    infos = info2dict(information)
    state = 3
    try:
        command = [x.format(**infos) for x in command]
        state = call(command)
        output = None
    except KeyError:
        output = 'UNKNOWN - Error building command.'
    except OSError:
        output = 'UNKNOWN - Error running command.'
    return state, output


def main():
    state, output = info_param(*check_arg())
    if output:
        print(output)
    sys.exit(state)

if __name__ == '__main__':
    main()
