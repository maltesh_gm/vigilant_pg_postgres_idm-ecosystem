# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_rabbitmq_server

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-rabbitmq_server
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - set of scripts to monitor RabbitMQ Servers via the http interface.

Group:     none
License:   GPL
URL:       https://github.com/jamesc/nagios-plugins-rabbitmq
# Packager Information
Packager:  Jason Malobicky <jason.malobicky@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins
Requires:  perl-JSON

Provides:  nagios-plugins-rabbitmq_server = %{version}-%{release}

%description
This is a set of scripts to monitor RabbitMQ Servers via the http interface.

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_aliveness %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_connections %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_objects %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_overview %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_partition %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_queue %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_server %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_shovels %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_rabbitmq_watermark %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_aliveness
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_connections
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_objects
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_overview
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_partition
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_queue
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_server
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_shovels
%attr(0755,root,root) %{nagiospluginsdir}/check_rabbitmq_watermark

%changelog
* Mon Aug 05 2014 Jason Malobicky <jason.malobicky@philips.com>
- Initial Spec File