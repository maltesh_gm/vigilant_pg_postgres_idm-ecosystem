# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_mssql_health

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-mssql_health
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - check_mssql_health is a plugin which checks several metrics of MS SQL Server.

Group:     none
License:   GPL
URL:       https://exchange.nagios.org/directory/Plugins/Databases/SQLServer/check_mssql_health/details
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Do not auto build requirements
AutoReqProv:  no

# Owns the nagios plugins directory
Requires:  nagios-plugins
# Rpmbuild doesn't find these perl dependencies
Requires:  perl(Getopt::Long)
Requires:  perl(Data::Dumper) >= 2.125
Requires:  perl(Nagios::Plugin) perl(File::Basename)

Provides:  nagios-plugins-check-mssql_health = %{version}-%{release}

%description
This plugin checks all basic and advance check related to mssql.

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_mssql_health.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_mssql_health.pl

%changelog
* Thu Nov 03 2016 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Updated to version 2.6.4.7
* Thu Nov 03 2016 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File