# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_dcm

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-dcm
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugin to monitor STORE SCP

Group:     none
License:   GPL
URL:       http://bn0.github.com/check_dcm/
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins python dcmtk python-argparse

Provides:  nagios-plugins-check-dcm = %{version}-%{release}

%description
The script is a wrapper for the dcmtk echoscu command to monitor STORE SCP

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_dcm.py %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_dcm.py

%changelog
* Tue Jun 18 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Update to version 0.2.0
* Thu Aug 09 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File