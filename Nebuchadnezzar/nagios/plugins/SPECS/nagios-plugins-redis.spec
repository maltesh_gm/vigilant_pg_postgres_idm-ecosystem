# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_redis

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-redis
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - script to monitor a Redis datastore

Group:     none
License:   GPL
URL:       http://exchange.nagios.org/directory/Plugins/Databases/check_redis-2Epl/details
# Packager Information
Packager:  Jason Malobicky <jason.malobicky@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins
Requires:  perl-Redis

Provides:  nagios-plugins-redis = %{version}-%{release}

%description
This is a script to monitor a Redis datastore.

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_redis.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_redis.pl

%changelog
* Wed Aug 06 2014 Jason Malobicky <jason.malobicky@philips.com>
- Initial Spec File