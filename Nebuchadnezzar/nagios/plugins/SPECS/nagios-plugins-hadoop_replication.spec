# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the check
%global checkname hadoop_replication
# Name of the plugin
%global plugin check_%{checkname}

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-%{checkname}
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - Plugin to check Hadoop HDFS replication via NameNode JMX

Group:     none
License:   GPL
URL:       https://github.com/harisekhon
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins
# Owns harisekhon directory
Requires:  nagios-plugins-harisekhon
Requires:  perl-JSON-XS

Provides:  nagios-plugins-%{checkname} = %{version}-%{release}

%description
Plugin to check Hadoop HDFS replication via NameNode JMX

Raises Critical on any missing or corrupt blocks, with configurable thresholds for under-replicated blocks.
Also reports excess blocks and blocks pending replication


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}/harisekhon
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_hadoop_replication.pl %{buildroot}%{nagiospluginsdir}/harisekhon/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/harisekhon/check_hadoop_replication.pl

%changelog
* Fri Jan 02 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File