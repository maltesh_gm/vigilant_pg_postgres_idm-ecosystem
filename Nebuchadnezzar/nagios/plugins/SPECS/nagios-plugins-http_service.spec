# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_http_service

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-http_service
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - Plugin to check http service response code and alert

Group:     none
License:   GPL
URL:       https://exchange.nagios.org/directory/Plugins/Network-Protocols/HTTP/check_http-perl-script/details
# Packager Information
Packager:  Jayaraj Singh <jayaraj.singh@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Do not auto build requirements
AutoReqProv:  no

# Owns the nagios plugins directory
Requires:  nagios-plugins

Provides:  nagios-plugins-http_service = %{version}-%{release}

%description
This plugin sets alert based on http service response code. The standard check_http plugins doesn't allow to set warning or critical status depending on the page content.
Using this plugin you can set multiple conditions for warning and/or critical status.

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_http.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_http.pl

%changelog
* Fri Jul 01 2016 Jayaraj Singh <jayaraj.singh@philips.com>
- Initial Spec File
