# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_ilo2_health

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-ilo2_health
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - script to monitor HP Servers via ILO2.

Group:     none
License:   GPL
URL:       https://exchange.nagios.org/directory/Plugins/Hardware/Server-Hardware/HP-(Compaq)/check_ilo2_health/details
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins
Requires:  perl-XML-Simple
Requires:  perl-IO-Socket-SSL

Provides:  nagios-plugins-check-ilo2_health = %{version}-%{release}

%description
Nagios plugin using the Nagios::Plugin module and the
HP Lights-Out XML PERL Scripting Sample from
ftp://ftp.hp.com/pub/softlib2/software1/pubsw-linux/p391992567/v60711/linux-LOsamplescripts3.00.0-2.tgz
checks if all sensors are ok, returns warning on high temperatures and
fan failures and critical on overall health failure


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_ilo2_health.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_ilo2_health.pl

%changelog
* Tue Nov 17 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File