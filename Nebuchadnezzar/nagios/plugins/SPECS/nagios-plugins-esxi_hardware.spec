# spec file for package 'nagios-plugins-esxi_hardware' (version '20120501')
#
# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_esxi_hardware

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-esxi_hardware
Version:   20120501
Release:   1%{?dist}
Summary:   Nagios plugin for checking global health of host running VMware ESX/ESXi.

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
Requires:  python >= 2.6
Requires:  pywbem

Provides:  nagios-plugins-check-esxi_hardware = %{version}-%{release}

%description
Nagios plugin for checking global health of host running VMware ESX/ESXi.


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_esxi_hardware.py %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_esxi_hardware.py

%changelog
* Thu Jul 26 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File