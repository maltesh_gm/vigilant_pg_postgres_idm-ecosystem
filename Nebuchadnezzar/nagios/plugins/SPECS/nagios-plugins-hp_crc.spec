# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_hp_crc

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-hp_crc
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugin to monitor HP ProCurve Switches.

Group:     none
License:   GPL
URL:       https://exchange.nagios.org/directory/Plugins/Hardware/Network-Gear/HP/HP-ProCurve-Switches-CRC-2FPackets-Check/details
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
Requires:  net-snmp-utils
Requires:  gawk

Provides:  nagios-plugins-check-hp_crc = %{version}-%{release}

%description
This Script checks HP ProCurve Switches (and maybe all others Switches with common MIBs) on every Interface for CRC
Errors & Packet Errors

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_hp_crc.sh %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_hp_crc.sh

%changelog
* Tue Nov 17 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
