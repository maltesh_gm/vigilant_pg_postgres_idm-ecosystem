# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_ltm_pools

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-ltm_pools
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   F5 Big-IP Local Traffic Manager Pool Monitor for Nagios.

Group:     none
License:   GPL
URL:       http://www.toms-blog.com/f5-big-ip-local-traffic-manager-pool-monitor-for-nagios/
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   http://www.toms-blog.com/wp-content/uploads/2012/11/%{plugin}-v%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins php

Provides:  nagios-plugins-check-ltm_pools = %{version}-%{release}

%description
This script monitors an F5 Local Traffic Manager’s pools and pool members to make sure 
that they are in the Available state. It uses SNMP to communicate with the LTM and get the 
States of the Pool Members. You can also pass in a comma separated string of exceptions to 
exclude certain pools from the check. It currently only supports SNMP v2 and has only been 
tested with Big-IP 10.2.2

%prep
%setup -q -c -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_ltm_pools.php %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_ltm_pools.php

%changelog
* Tue Jun 17 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
