# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_wmi_lh

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-wmi_lh
Version:   %{_phiversion}
Release:   2%{?dist}
Summary:   Nagios plugins to perform various WMI related actions.

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins
Requires:  wmic
Requires:  samba-common
Requires:  nagios-plugins-check-wmi_plus
Requires:  nagios-plugins-post_notification
Requires:  phimutils
Requires:  python-argparse
Requires:  perl(Config::IniFiles) >= 2.58
Requires:  perl >= 5.01
Requires:  perl(Getopt::Long) >= 2.38
Requires:  perl(Data::Dumper) >= 2.125
Requires:  perl(Storable) >= 2.22
Requires:  perl(Scalar::Util) >= 1.22

# This was added as perl(Storable) >= 2.22 seems to be satisfied with perl(Storable) = 2.20
Requires:  local-perl-Storable >= 2.22
Requires:  perl(DateTime) >= 0.66

Provides:  nagios-plugins-check-wmi_lh = %{version}-%{release}

%description
Check WMI lh is a client-less Nagios plugin for checking subfolder count on Windows systems.

check_wmi_lh.pl is a plugin to monitor Windows subfolder count via WMI
created from tearing check_wmi_plus to pieces just to get one query to run,
because I could not get it to work via ini file.

filter_perfdata.py is a plugin to filter performance data, it makes it possible for a check to report filtered
information based on the output of another test.

check_cluster_lh.py is a plugin to aggregate the status of other tests, combining multiple output already captured and
reporting which tests have failed.

short_c.py is a plugin to wrap a command call, deciding whether to run depending on a condition parameter.

restart_win_service_and_notify.py is an event handler to restart a Windows service using net rpc from samba and send a
backoffice notification, it makes it possible to restart on attempts passed in a list.

f5_pool_handler_and_notify.py is an event handler to disable or enable Dicom node from f5 load balancer pool and send a
back office notification.

wmi_time_wrapper.py is a plugin that wraps a command passing a timestamp in WMI format as last parameter.


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}/eventhandlers
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_wmi_lh.pl %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/filter_perfdata.py %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/short_c.py %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_wmi_access.py %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_cluster_lh.py %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/wmi_time_wrapper.py %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/info_param.py %{buildroot}%{nagiospluginsdir}/
%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}/check_wmi_plus.d
%{__install} -Dp -m 0644 %{_builddir}/%{plugin}-%{version}/philipsce.ini %{buildroot}%{nagiospluginsdir}/check_wmi_plus.d/
%{__sed} -i 's#\(use\s*lib\).*plugins[^;]*#\1 "%{nagiospluginsdir}"#g' %{buildroot}%{nagiospluginsdir}/check_wmi_lh.pl
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/restart_win_service_and_notify.py %{buildroot}%{nagiospluginsdir}/eventhandlers/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/f5_pool_handler_and_notify.py %{buildroot}%{nagiospluginsdir}/eventhandlers/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_wmi_lh.pl
%attr(0755,root,root) %{nagiospluginsdir}/filter_perfdata.py
%attr(0755,root,root) %{nagiospluginsdir}/short_c.py
%attr(0755,root,root) %{nagiospluginsdir}/check_wmi_access.py
%attr(0755,root,root) %{nagiospluginsdir}/check_cluster_lh.py
%attr(0755,root,root) %{nagiospluginsdir}/wmi_time_wrapper.py
%attr(0755,root,root) %{nagiospluginsdir}/info_param.py
%{nagiospluginsdir}/check_wmi_plus.d/philipsce.ini
%attr(0755,root,root) %{nagiospluginsdir}/eventhandlers/restart_win_service_and_notify.py
%attr(0755,root,root) %{nagiospluginsdir}/eventhandlers/f5_pool_handler_and_notify.py

%changelog
* Thu Aug 10 2017 Vijay N <Vijay.n@philips.com>
- Added f5_pool_handler_and_notify event handler
* Thu Mar 05 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added info_param
* Thu Feb 05 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added short_c and check_wmi_access
* Fri Nov 07 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added restart_win_service_and_notify event handler
- Added wmi_time_wrapper plugin
* Wed Nov 05 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added check_cluster_lh plugin
* Fri May 02 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added filter_perfdata script
* Wed Jan 08 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added query for age of files in a directory
* Fri Dec 27 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added query for process executable win 2k compatible
* Tue Oct 15 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added philipsce.ini file
* Fri Jun 14 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
