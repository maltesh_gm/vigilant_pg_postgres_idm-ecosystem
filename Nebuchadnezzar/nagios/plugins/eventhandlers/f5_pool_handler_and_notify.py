#!/usr/bin/env python
# This Plugin will execute only on the following scenarios
#    1) STATE is CRITICAL  and STATETYPE is HARD
#         then disable the proc node in the Load balacer pool
#    2) STATE changes from CRITICAL to OK
#         Then enable the proc node in the Load balancer pool
#    3) It will disable or enable if pool members are greater than 1
import linecache
import json
import sys
import yaml
import argparse
import requests
import subprocess
from phimutils.collection import DictNoEmpty
from phimutils.message import Message
from phimutils.argument import from_json

# Event STATE Types
SOFT = 'SOFT'
HARD = 'HARD'

DISCOVERY_FILE = '/etc/philips/discovery.yml'

PAYLOAD_HELP = 'A label and JSON string to be used as part of the payload'
STATE_CHOICES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


class JSONAttributeAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not (len(values) == 2):
            raise argparse.ArgumentError(
                self, '%s takes 2 values, %d given' % (option_string, len(values)))
        label, json_string = values
        try:
            value = from_json(json_string)
        except argparse.ArgumentTypeError:
            raise argparse.ArgumentError(
                self, '%s takes valid JSON, %s given' % (option_string, json_string))
        destination = getattr(namespace, self.dest) or {}
        destination[label] = value
        setattr(namespace, self.dest, destination)


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to send a message via HTTP post')
    parser.add_argument('-u', '--url', required=True,
                        help='The URL address to post to')
    parser.add_argument('-H', '--hostaddress', required=True,
                        help='The address of the server')
    parser.add_argument('-a', '--hostname', required=True,
                        help='The name of the server')
    parser.add_argument('-Y', '--statetype',
                        choices=(SOFT, HARD), help='The state type for the event')
    parser.add_argument('-d', '--description', required=True,
                        help='The description of the entity for the message')
    parser.add_argument('-t', '--timestamp',
                        help='The timestamp for the message, expected in UNIX epoch time')
    parser.add_argument('-o', '--output',
                        help='The output for the message')
    parser.add_argument('-T', '--notificationtype',
                        help='The notification type for the message')
    parser.add_argument('-s', '--state', choices=STATE_CHOICES,
                        help='The state for the message')
    parser.add_argument('-p', '--perfdata',
                        help='The performance data type for the message')
    parser.add_argument('-P', '--payload',
                        action=JSONAttributeAction, help=PAYLOAD_HELP, nargs=2)
    parser.add_argument('-k', '--insecure',
                        help='do not verify ssl', action='store_true')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--siteid_file',
                       help='A file containing the site id', default='/etc/siteid')
    group.add_argument('--siteid', help='The site id')
    results = parser.parse_args(args)

    return (
        results.url,
        results.hostaddress,
        results.hostname,
        results.statetype,
        results.description,
        results.notificationtype,
        results.state,
        results.timestamp,
        results.output,
        results.perfdata,
        results.siteid,
        results.siteid_file,
        results.payload,
        results.insecure)


def get_f5_credentials(file_name):
    try:
        address, username, password = None, None, None
        with open(file_name, 'r') as fd:
            for credentials in yaml.load(fd):
                if credentials.get('f5'):
                    address, username, password = credentials['f5']['address'], credentials['f5']['username'], \
                                                  credentials['f5']['password']
    except Exception:
        pass
    finally:
        return address, username, password


class PoolMemeber:
    """
        Member which can be enabled or disabled
        object of this class will be an attribute to F5LoadBalancer
    """

    UP = 'up'
    DOWN = 'user-down'
    ENABLE = 'user-enabled'

    def __init__(self, address, name, state, **kwars):
        self.address = address
        self.name = name
        self.state = state
        self.other = kwars

    def enable(self, url, user, password, data, headers):
        if not self.state == PoolMemeber.UP:
            response = requests.put(url, auth=(user, password), verify=False, data=json.dumps(data), headers=headers)
            response.raise_for_status()
            return response

    def disable(self, url, user, password, data, headers):
        if not self.state == PoolMemeber.DOWN:
            response = requests.put(url, auth=(user, password), verify=False, data=json.dumps(data), headers=headers)
            response.raise_for_status()
            return response


class F5LoadBalancer:
    """
        Helps to communicate with F5 API for enabling and disabling Proc Node
        from the load balancer pool

    """
    MEMBER_URL = 'https://{0}/mgmt/tm/ltm/pool/~Common~DICOM_IN_104/members'
    STATUS_URL = 'https://{0}/mgmt/tm/ltm/pool/~Common~DICOM_IN_104/members/~Common~{1}'
    DISABLE = {'state': 'user-down', 'session': 'user-disabled'}
    ENABLE = {'session': 'user-enabled', 'state': 'user-up'}
    HEADERS = {'Content-Type': 'application/json'}

    def __init__(self, address, user, password, member_ip):
        self.address = address
        self.user = user
        self._password = password
        self._member_ip = member_ip
        self._pmembers = None
        self._pool_member = None
        self._v_nos_nodes = None

    def _STATUS_URL(self):
        return self.STATUS_URL.format(self.address, self.member.name)

    @property
    def nos_nodes(self):
        # number of nodes in the pool.
        return len(self.pool_members)

    @property
    def member(self):
        if not self._pool_member:
            if self.nos_nodes > 1:
                for pmember in self.pool_members:
                    if pmember['address'] == self._member_ip:
                        self._pool_member = PoolMemeber(**pmember)
                        break
                else:
                    msg = 'Member {0} does not exist in the pool'.format(
                        self._member_ip)
                    raise Exception(msg)
            else:
                msg = 'Number of pool members are {0},  not greater than 1'.format(
                    len(self.pool_members['items']))
                raise Exception(msg)
        return self._pool_member

    @property
    def pool_members(self):
        if not self._pmembers:
            pool_rsp = requests.get(self.MEMBER_URL.format(self.address), auth=(
                self.user, self._password), verify=False)
            pool_rsp.raise_for_status()
            self._pmembers = pool_rsp.json().get('items', [])
        return self._pmembers

    def enable_member(self):
        self.member.enable(self._STATUS_URL(), self.user,
                           self._password, self.ENABLE, self.HEADERS)

    def verify_poolmember_state(self):
        for member in self.pool_members:
            if not member['address'] == self.member.address:
                if member['state'] == PoolMemeber.UP:
                    return True
        return False

    def disable_member(self):
        self.member.disable(self._STATUS_URL(), self.user, self._password, self.DISABLE, self.HEADERS)


def verify_state(state, st_type):
    if state == STATE_CHOICES[0] or (state == STATE_CHOICES[2] and st_type == HARD):
        return True
    return False


def post_notification(
        url,
        hostaddress,
        hostname,
        statetype,
        description,
        notificationtype,
        state,
        timestamp,
        output,
        perfdata,
        siteid,
        siteid_file,
        payload=None,
        insecure=False):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
        if not siteid:
            return 2, 'Could not post due to missing siteid'
    try:
        if verify_state(state, statetype):
            f5_address, f5_user, f5_pwd = get_f5_credentials(DISCOVERY_FILE)
            if f5_address and f5_user and f5_pwd:
                f5 = F5LoadBalancer(f5_address, f5_user, f5_pwd, hostaddress)
                if f5.nos_nodes > 1:
                    if state == STATE_CHOICES[0]:
                        try:
                            f5.enable_member()
                            state, notificationtype, output = 0, 'RECOVERY', 'Node Enabled successfully in F5 LB Pool.'
                        except Exception:
                            state, notificationtype, output = 2, 'PROBLEM', 'Unable to enable Node in F5 LB Dicom Pool.'
                    elif state == STATE_CHOICES[2] and statetype == HARD:
                        try:
                            if f5.verify_poolmember_state():
                                f5.disable_member()
                                state, notificationtype, output = 0, 'RECOVERY', 'Node Disabled successfully in F5 LB '\
                                                                                 'Pool.'
                            else:
                                state, notificationtype, output = 2, 'PROBLEM', 'Only one node in the pool with the ' \
                                                                             'state UP, hence not disabling the node.'
                        except Exception:
                            state, notificationtype, output = 2, 'PROBLEM', 'Unable to disable Node in F5 LB Pool.'
                else:
                    state, notificationtype, output = 2, 'PROBLEM', 'Number of nodes in the pool is not more than one.'
            else:
                state, notificationtype, output = 2, 'PROBLEM', 'Unable to find F5 credentials from discovery.yml.'
        else:
            state, output = 2, 'Either State should be `OK`, OR State should be `CRITICAL`  and State Type `HARD`'
    except Exception:
        state, notificationtype, output = 2, 'PROBLEM', 'F5 Login Credentials not available or invalid F5 LB MGMT IP ' \
                                                        'or invalid credentials to connect, do verify.'
    payload = DictNoEmpty(payload or {}, output=output)
    msg = Message(
            siteid=siteid,
            epoch_time=timestamp,
            hostname=hostname,
            hostaddress=hostaddress,
            service=description,
            type=notificationtype,
            state=STATE_CHOICES[state],
            payload=payload,
            perfdata=perfdata
        )
    r = requests.post(url, json=msg.to_dict(), verify=not insecure)
    r.raise_for_status()
    return state, output


def main():
    state, msg = post_notification(*check_arg())
    print msg
    sys.exit(state)


if __name__ == '__main__':
    main()
