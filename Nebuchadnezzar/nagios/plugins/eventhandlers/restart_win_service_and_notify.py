#!/usr/bin/env python

import os
import sys
from functools import partial
from subprocess import call
from time import sleep

import argparse
from phimutils.argument import positive_int

# Append parent dir (where plugins live)
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import post_notification


ATTEMPT_NUM_HELP = 'The attempt numbers where to execute handler'
USERNAME_HELP = 'The username for the server domain may be included Eg: user@domain.com'
OK = 'OK'
WARNING = 'WARNING'
CRITICAL = 'CRITICAL'
UNKNOWN = 'UNKNOWN'
STATES = (OK, WARNING, CRITICAL, UNKNOWN)
SOFT = 'SOFT'
HARD = 'HARD'
HARD_MODIFIER = '+'
FAILURE_MESSAGE = 'service not recovered after maximum restart attempts'
RECOVERY_MESSAGE = 'service recovered'
RESTART_MESSAGE = 'service restart attempted'


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Restart windows service and notify event handler',
        epilog='Restart Attempts are only made on CRITICAL State'
    )
    parser.add_argument('-H', '--hostaddress', required=True, help='The address of the server originating the event')
    parser.add_argument('-u', '--username', required=True, help=USERNAME_HELP)
    parser.add_argument('-p', '--password', required=True, help='The password for the server')
    parser.add_argument('-N', '--service', required=True, help='The name of the Windows service to restart')
    parser.add_argument('-U', '--url', required=True, help='The URL address to post to')
    parser.add_argument('-a', '--hostname', required=True, help='The name of the server originating the event')
    parser.add_argument('-d', '--description', required=True, help='The description of the entity for the event')
    parser.add_argument('--notificationtype', default='HANDLER', help='The notification type for the event')
    parser.add_argument('-s', '--state', choices=STATES, help='The state for the event')
    parser.add_argument('-o', '--output', help='The output for the event')
    parser.add_argument('--siteid_file', help='A file containing the site id', default='/etc/siteid')
    parser.add_argument('-n', '--attempt_numbers', required=True, nargs='+', type=positive_int, help=ATTEMPT_NUM_HELP)
    parser.add_argument('--hard', action='store_true', default=False, help='Execute handler on final/hard state')
    parser.add_argument('-A', '--attempt', type=positive_int, required=True, help='The current attempt number')
    parser.add_argument('-M', '--maxattempt', type=positive_int, required=True, help='The maximum attempt number')
    parser.add_argument('-Y', '--statetype', required=True, choices=(SOFT, HARD), help='The state type for the event')
    parser.add_argument('--timeout', type=int, default=60, help='The time to wait for the stop/start call complete')
    parser.add_argument('--sleep', type=int, default=40, help='The time to wait between service stop and service start')
    parser.add_argument('-k', '--insecure', help='do not verify ssl', action='store_true')
    results = parser.parse_args(args)
    return results


class HandlerEvent(object):
    def __init__(self, service, output, attempt, attempt_numbers, state, statetype, hard):
        self.service = service
        self.attempt = attempt
        self.output = output
        self.maxattempt = len(attempt_numbers)
        self.attempt_numbers = sorted(set([0] + attempt_numbers))
        self.state = state
        self.statetype = statetype
        self.hard = hard
        self._previous_attempt = None
        self.messages = None
        self.restartable = False
        self.decide()

    @property
    def previous_attempt(self):
        if self._previous_attempt is None:
            previous_attempts = filter(lambda x: x < self.attempt, self.attempt_numbers)
            self._previous_attempt = self.attempt_numbers.index(max(previous_attempts))
        return self._previous_attempt

    def is_restart_event(self):
        return self.state == CRITICAL and self.statetype == SOFT and self.attempt in self.attempt_numbers

    def is_failure_event(self):
        return self.state == CRITICAL and self.statetype == HARD

    def is_recovery_event(self):
        return self.state == OK and self.statetype == SOFT and self.previous_attempt

    def get_output(self, message, attempt, modifier=''):
        return '{service} {message} - {output} [{attempt}{modifier}/{maxattempt}]'.format(
            service=self.service,
            message=message,
            output=self.output,
            attempt=attempt,
            modifier=modifier,
            maxattempt=self.maxattempt
        )

    def add_message(self, text, attempt_number, state=None, modifier=''):
        state = state or self.state
        self.messages.append({'state': state, 'output': self.get_output(text, attempt_number, modifier)})

    def decide(self):
        self.messages = []
        if self.is_restart_event():
            self.restartable = True
            self.add_message(RESTART_MESSAGE, self.attempt_numbers.index(self.attempt))
        elif self.is_recovery_event():
            self.add_message(RECOVERY_MESSAGE, self.previous_attempt)
        elif self.is_failure_event():
            self.add_message(FAILURE_MESSAGE, self.maxattempt, state=WARNING)
            if self.hard:
                self.restartable = True
                self.add_message(RESTART_MESSAGE, self.maxattempt, modifier=HARD_MODIFIER)


class HandlerMessenger(object):
    def __init__(self, url, hostname, hostaddress, description, notificationtype, siteid_file, insecure):
        self.url = url
        self.hostname = hostname
        self.hostaddress = hostaddress
        self.description = description
        self.notificationtype = notificationtype
        self.siteid_file = siteid_file
        self.perfdata = None
        self.siteid = None
        self.timestamp = None
        self.insecure = insecure

    def send_message(self, state, output):
        _, post_message = post_notification.post_notification(state=state, output=output, **vars(self))
        return post_message


class WindowsServiceOperator(object):
    def __init__(self, hostaddress, service, username, password, sleep_before_start, timeout):
        user, _, domain = username.partition('@')
        self.hostaddress = hostaddress
        self.service = service
        self.user = user
        self.domain = domain
        self.password = password
        self.sleep_before_start = sleep_before_start
        self.timeout = timeout


    def caller(self):
        return partial(call, timeout=self.timeout)

    @property
    def user_pass(self):
        return '{user}%{password}'.format(user=self.user, password=self.password)

    def get_command(self, action):
        command = [
            '/usr/bin/net', 'rpc', 'service', action, self.service, '-U', self.user_pass, '-I', self.hostaddress
        ]
        if self.domain:
            command.extend(['-W', self.domain])
        return command

    def restart(self):
        self.caller()(self.get_command('stop'))
        sleep(self.sleep_before_start)
        self.caller()(self.get_command('start'))


def restart_win_service_and_notify(arguments):
    event = HandlerEvent(
        service=arguments.service,
        output=arguments.output,
        attempt=arguments.attempt,
        attempt_numbers=arguments.attempt_numbers,
        state=arguments.state,
        statetype=arguments.statetype,
        hard=arguments.hard
    )
    messenger = HandlerMessenger(
        url=arguments.url,
        hostname=arguments.hostname,
        hostaddress=arguments.hostaddress,
        description=arguments.description,
        notificationtype=arguments.notificationtype,
        siteid_file=arguments.siteid_file,
        insecure=arguments.insecure
    )
    for message in event.messages:
        print(messenger.send_message(**message))

    if event.restartable:
        WindowsServiceOperator(
            hostaddress=arguments.hostaddress,
            service=arguments.service,
            username=arguments.username,
            password=arguments.password,
            sleep_before_start=arguments.sleep,
            timeout=arguments.timeout
        ).restart()


def main():
    restart_win_service_and_notify(check_arg())


if __name__ == "__main__":
    main()
