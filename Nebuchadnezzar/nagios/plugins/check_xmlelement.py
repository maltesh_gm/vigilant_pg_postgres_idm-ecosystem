#!/usr/bin/env python
#This script will check for a status element returned in xml from an url
import sys
import argparse
import libxml2
import requests


DESCRIPTION_HELP = 'the xml element containing the description to return'
STATES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


# the following requires python-argparse to be installed
def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to check an XML element given an URL')
    parser.add_argument('-u', '--url', help='url to check for', required=True)
    parser.add_argument('-e', '--element', help='xml element to check for', default='Status')
    parser.add_argument('-t', '--text', help='the expected text for success', default='Pass')
    parser.add_argument('-m', '--description', help=DESCRIPTION_HELP, default='StatusDescription')
    parser.add_argument('-k', '--insecure', help='do not verify ssl', action='store_true')
    results = parser.parse_args(args)
    return results.url, results.element, results.text, results.description, results.insecure


def get_element_content(doc, element):
    """Returns the content of an element if its found, None otherwise"""
    elem_node = doc.xpathEval('//{element}'.format(element=element))
    return elem_node.pop().getContent().strip() if elem_node else None

    
def check_xmlelement(url, element, text, description, insecure):
    message = 'Status could not be determined'
    state = 3

    try:
        req = requests.get(url, verify=not insecure)
        req.raise_for_status()
        doc = libxml2.parseDoc(req.content)
        elem_text = get_element_content(doc, element)
        if elem_text:
            description_text = get_element_content(doc, description)
            state = 0 if (elem_text == text) else 2
            message = '{element} : {description}'.format(element=elem_text, description=description_text)
    except requests.exceptions.RequestException, err:
        message = "Address '{url}' could not be read: {msg}".format(url=url, msg=err)
        state = 2
    except libxml2.parserError:
        message = 'Could not parse xml' 

    return state, message


def main():
    state, message = check_xmlelement(*check_arg())
    print '{state} - {message}'.format(state=STATES[state], message=message)
    sys.exit(state)


if __name__ == '__main__':
    main()
