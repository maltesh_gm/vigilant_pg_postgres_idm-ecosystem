#!/usr/bin/env python

import argparse
import requests
import sys
from jsonpath_rw import parse


QUERY_HELP = 'JSONPath query to get the elements to check for information'
STATES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')
STATE_MAP = dict(zip(STATES, range(4)))


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to check status from JSON object given an URL')
    parser.add_argument('-u', '--url', help='url to check', required=True)
    parser.add_argument('-q', '--query', help=QUERY_HELP, required=True)
    parser.add_argument('-s', '--state_key', help='the key to check or state', required=True)
    parser.add_argument('-m', '--message_key', help='the key to grab the message from')
    parser.add_argument('-n', '--label_key', help='the key to grab the label from')
    parser.add_argument('-k', '--insecure', help='do not verify ssl', action='store_true')
    results = parser.parse_args(args)
    return results.url, results.query, results.state_key, results.message_key, results.label_key, results.insecure


def get_json(url, verify):
    req = requests.get(url, verify=verify)
    req.raise_for_status()
    return req.json()


def get_query(query):
    try:
        parsed_query = parse(query)
    except Exception:
        raise AttributeError('Could not parse query: "%s" ' % query)
    return parsed_query


def nagios_state(x):
    return 4 if x == 2 else x


def get_entry(datum, state_key, message_key, label_key):
    state = 3
    #if any specified information is not found in reply, then its UNKNOWN
    entry_label = ''
    entry_message = ''
    entry_state = datum.get(state_key, STATES[state]).upper()
    state = STATE_MAP.get(entry_state, state)
    try:
        if message_key:
            entry_message = datum[message_key]
        if label_key:
            entry_label = datum[label_key]
    except KeyError:
        entry_message = 'missing key'
        state = max(state, 3, key=nagios_state)
    return state, "[%s] %s: %s" % (entry_state, entry_label, entry_message)


def check_json_status(url, query, state_key, message_key, label_key, insecure):
    data = get_json(url, not insecure)
    parsed_query = get_query(query)
    jp_results = parsed_query.find(data)

    kv_results = [get_entry(item.value, state_key, message_key, label_key) for item in jp_results]
    states, messages = zip(*kv_results)
    return max(states, key=nagios_state), '; '.join(messages)


def main():
    try:
        #state, msg = check_json_status('http://167.81.178.69/iex/health', 'Applications[*]', 'Status', 'Message', 'ApplicationName')
        state, msg = check_json_status(*check_arg())
    except requests.exceptions.RequestException, e:
        state = 2
        msg = e
    except Exception, e:
        state = 3
        msg = e
    print("%(state)s - %(msg)s" % {"state": STATES[state], "msg": msg})
    sys.exit(state)


if __name__ == '__main__':
    main()
