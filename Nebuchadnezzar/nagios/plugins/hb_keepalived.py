#!/usr/bin/python
import datetime
import socket
import smtplib
import argparse
import linecache
import sys
from netifaces import ifaddresses, AF_INET, AF_INET6
from email.mime.text import MIMEText


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a HB I am alive message via SMTP')
    parser.add_argument('--smtpfrom', 
                        help='The "from" address to use for the message',
                        default='isyntaxserver@stentor.com')
    parser.add_argument('--smtpto', 
                        help='The "to" address to use for the message',
                        default='hbeat@stentor.com')
    parser.add_argument('--smtpserver', 
                        help='The SMTP server',
                        default='localhost')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--siteid_file', help='A file containing the site id', default='/etc/siteid')
    group.add_argument('--siteid', help='The site id')
    
    results = parser.parse_args(args)

    return (results.smtpfrom,
            results.smtpto,
            results.smtpserver,
            results.siteid,
            results.siteid_file)


def get_ip_address(ifname):
    """Returns ip address for device specified by ifname"""
    ip = '0.0.0.0'
    try:
        links = ifaddresses(ifname)
        if AF_INET in links:
            ip = links[AF_INET].pop()['addr']
        elif AF_INET6 in links:
            ip = links[AF_INET6].pop()['addr']
    except ValueError:
        print 'interface not found'
    except KeyError:
        print 'problem finding address'
    return ip

    
## to test # def send_message(message,smtpfrom="isyntaxserver@stentor.com",smtpto="root@localhost"):
def send_message(message, smtpfrom, smtpto, smtpserver):
    """Sends email via SMTP"""
    msg = MIMEText(message)

    #msg['Subject'] = 'none?' 
    msg['From'] = smtpfrom
    msg['To'] = smtpto

    # Send the message via SMTP server, but don't include the envelope header.
    s = smtplib.SMTP(smtpserver)
    s.sendmail(smtpfrom, smtpto, msg.as_string())
    s.quit()

    
def create_msg(timestamp, localip, siteid, hostname):
    """Returns a message in the format HeartBeat expects"""
    msg_string = """\
<?xml version="1.0" encoding="utf-16"?>
<MESSAGE_ROOT>
    <VERSION>1.0</VERSION>
    <CATEGORY>LM_HEARTBEAT</CATEGORY>
    <COMPONENT>HeartBeat</COMPONENT>
    <SITE_NAME> {siteid} </SITE_NAME>
    <LOCAL_HOST> {hostname} </LOCAL_HOST>
    <CLUSTER>NO</CLUSTER>
    <ISYNTAX_SERVER_VERSION>1,1,1,1</ISYNTAX_SERVER_VERSION>
    <FILTER_CONFIG_VERSION>1.0.1.0</FILTER_CONFIG_VERSION>
    <LOCATION>Main Location</LOCATION>
    <BODY>
        <HB_MESSAGE>
            <SITE_NAME> {siteid} </SITE_NAME>
            <SITE_STATUS>0000</SITE_STATUS>
            <REPORTING_INTERVAL>15</REPORTING_INTERVAL>
            <BEAT_NUMBER>1</BEAT_NUMBER>
            <TIMESTAMP> {timestamp} </TIMESTAMP>
            <MODULES>
                <MODULE>
                    <TYPE>NAGIOS</TYPE>
                    <HOSTNAME> {hostname} </HOSTNAME>
                    <MONITOR_RESULTS></MONITOR_RESULTS>
                    <QUEUE_STATS></QUEUE_STATS>
                    <DRIVE_SPACE></DRIVE_SPACE>
                    <IP_ADDRESSES> {localip} </IP_ADDRESSES>
                    <STATUS>0000</STATUS>
                </MODULE>
            </MODULES>
        </HB_MESSAGE>
    </BODY>
</MESSAGE_ROOT>"""
    return msg_string.format(**locals())


def hb_keepalived(smtpfrom, smtpto, smtpserver, siteid, siteid_file):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1)
        if not siteid:
            print 'OK - But could not post due to missing siteid'
            return
    now = datetime.datetime.now()
    message = create_msg(
        timestamp = now.strftime("%m/%d/%Y %H:%M:%S.0000"),
        localip = get_ip_address("eth0"),
        siteid = siteid,
        hostname = socket.gethostname())
    send_message(message, smtpfrom, smtpto, smtpserver)
    print 'OK - Message sent'
    
    
def main():
    hb_keepalived(*check_arg())

    
if __name__ == "__main__":
    main()


# Changelog
#
# Version: 0.2
# Date: 2013-07-25
# Owner: Leonardo Ruiz
# Filename: hb_keepalived.py
# Description: Used to send heartbeat formatted message to be used as a keepalive
#
# End of file - hb_keepalived.py
