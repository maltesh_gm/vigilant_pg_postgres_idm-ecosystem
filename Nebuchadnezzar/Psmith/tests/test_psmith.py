import sys
import unittest
from mock import MagicMock
from lxml import etree
from blist import sortedset

from psmith import PSmith


sys.modules['phimutils.plogging'] = MagicMock(name='phimutils.plogging')
sys.modules['phimutils'] = MagicMock(name='phimutils')


class PSmithTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.xml_text = """<manifest>
    <dir name="complex">
        <dir name="1">
            <dir name="e">
                <file name='co.txt' />
            </dir>
        </dir>
    </dir>
    <file name='etc.exe' />
    <dir name="Installers3.6">
        <file name='install.msi' />
        <file name='lang1.rsr' />
    </dir>
    <dir name="Installers4.4">
        <file name='install.msi' />
        <file name='patch1.exe' />
        <file name='patch2.exe' />
    </dir>
</manifest>"""
        self.tree = etree.fromstring(self.xml_text)

    def test__sortedset_from_xml_dirs(self):
        expected = sortedset([
            'Installers3.6',
            'Installers4.4',
            'complex',
            'complex/1', 'complex/1/e'
        ])
        self.assertEqual(PSmith._sortedset_from_xml(self.tree, 'dir'), expected)

    def test__sortedset_from_xml_files(self):
        expected = sortedset([
            'Installers3.6/install.msi',
            'Installers3.6/lang1.rsr',
            'Installers4.4/install.msi',
            'Installers4.4/patch1.exe',
            'Installers4.4/patch2.exe',
            'complex/1/e/co.txt',
            'etc.exe'
        ])
        self.assertEqual(PSmith._sortedset_from_xml(self.tree, 'file'), expected)


if __name__ == '__main__':
    unittest.main()
