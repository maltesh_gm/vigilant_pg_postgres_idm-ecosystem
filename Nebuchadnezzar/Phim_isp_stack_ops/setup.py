from distutils.core import setup
import os
phiversion = os.environ['phiversion']
setup(name='phim-isp-stack-ops',
      version=phiversion,
      description='Philips Stack Operation Utilities',
      author='Cornell Lawrence',
      author_email='cornell.lawrence@philips.com',
      #py_modules=['phimutils.timestamp'],
      packages=['phim_isp_stack_ops']
      )
