#!/usr/bin/env python

import os
import json
from collections import defaultdict

import argparse
from lxml import objectify, etree
import redis
from phimutils.stateredis import StateRedis
import sys
sys.path.append('/usr/lib/celery/')
from tbconfig import REDIS, PCM


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Dynamic inventory from a SiteInfo.xml')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--list', action='store_true', help='Get the inventory list')
    group.add_argument('--host', help='The address of the server')
    results = parser.parse_args(args)
    return results.host, results.list


def get_inventory_from_site_info():
    rc = redis.StrictRedis.from_url(REDIS['URL'])
    inv = defaultdict(list)
    inv['local'] = ['localhost']

    site_info_xml = rc.hget(PCM['HASH'],PCM['SITE_INFO_XML'])
    if not os.path.exists(site_info_xml):
        return inv

    parsed_site_info = objectify.parse(site_info_xml)
    site_info_root = parsed_site_info.getroot()
    package_read = False
    for role_element in site_info_root.Roles.Role:
        # All non-sequence should go last
        seq = role_element.get('Sequence', '9999')
        inv["Sequence{0}".format(seq)].append(str(role_element.FQDN))
        package_info = role_element.Packages.PackageInfo
        if not package_read:
            rc.hset(PCM['HASH'], PCM['KEY'], package_info.get('Name'))
            rc.hset(PCM['HASH'], PCM['VERSION'], package_info.get('Version'))
            package_read = True
    return inv


def dinventory(host, g_list):
    # not really doing host variables, just return empty dict
    if host:
        print(json.dumps({}))
        return
    print json.dumps(get_inventory_from_site_info())


def main():
    dinventory(*check_arg())


if __name__ == "__main__":
    main()
