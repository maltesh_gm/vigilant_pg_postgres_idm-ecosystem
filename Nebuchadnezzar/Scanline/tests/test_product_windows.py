import unittest
from mock import MagicMock, patch


class ScanlineWindowsBasedProductScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_host = MagicMock()
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.host.windows': self.mock_host.windows
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.windows import WindowsBasedProductScanner
        self.WindowsBasedProductScanner = WindowsBasedProductScanner
        self.win_scanner = self.WindowsBasedProductScanner('Win', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], product_id='PID')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('scanline.product.windows.WindowsHostScanner')
    def test_scan_host(self, mock_windows_scanner):
        mock_windows_scanner.return_value.to_dict.return_value = {
            'address': '10.4.5.6',
            'Windows': {'version': 'Windows 2k', 'patch': 'please'}
        }
        self.win_scanner.host_scanner = MagicMock()
        self.win_scanner.host_scanner.return_value.to_dict.return_value = {'ModR': {'key1': 'val1'}}
        self.assertEqual(
            self.win_scanner.scan_host('host1'),
            {
                'Windows': {'version': 'Windows 2k', 'patch': 'please'},
                'ModR': {'key1': 'val1'},
                'address': '10.4.5.6',
                'product_id': 'PID'
            }
        )
        mock_windows_scanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'Win', 'address': '10.220.3.1'},
            hostname='host1',
            password='CryptThis',
            tags=['Prod'],
            username='tester'
        )
        self.win_scanner.host_scanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'Win', 'address': '10.220.3.1'},
            hostname='host1',
            password='CryptThis',
            tags=['Prod'],
            username='tester'
        )


if __name__ == '__main__':
    unittest.main()
