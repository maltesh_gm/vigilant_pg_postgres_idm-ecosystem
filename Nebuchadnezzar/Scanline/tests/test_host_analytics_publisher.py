import unittest
from mock import MagicMock, patch


class ScanlineHostAnalyticsPublisherTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.analytics_publisher import AnalyticsPublisherHostScanner
        self.AnalyticsPublisherHostScanner = AnalyticsPublisherHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_properties_present(self):
        scanner = self.AnalyticsPublisherHostScanner('host1', '167.81.183.99', username='user', password='secret',
                                                     tags=['x', 'y'])
        mock_scanner = MagicMock(spec=scanner)
        for prop in self.AnalyticsPublisherHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.AnalyticsPublisherHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))


if __name__ == '__main__':
    unittest.main()
