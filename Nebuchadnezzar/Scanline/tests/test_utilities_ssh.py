import unittest
from mock import MagicMock, patch
from StringIO import StringIO


class ScanlineSSHTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_paramiko = MagicMock(name='paramiko')
        modules = {
            'logging': MagicMock(name='logging'),
            'paramiko': self.mock_paramiko
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities.ssh import SSHConnection
        self.SSHConnection = SSHConnection
        self.ssh_conn = SSHConnection('127.0.0.1', 'Foo', 'Bar')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_connected_client(self):
        mock_client = self.mock_paramiko.SSHClient.return_value
        self.assertEqual(self.ssh_conn.get_connected_client(), mock_client)
        mock_client.set_missing_host_key_policy.assert_called_once_with(
            self.mock_paramiko.AutoAddPolicy.return_value
        )
        mock_client.connect.assert_called_once_with('127.0.0.1', username='Foo', password='Bar', timeout=5)

    def test_exec_output(self):
        self.ssh_conn.get_connected_client = MagicMock()
        client = self.ssh_conn.get_connected_client.return_value
        client.exec_command.return_value = (None, StringIO('it works'), StringIO())
        self.assertEqual(self.ssh_conn.exec_output('true'), 'it works')
        client.exec_command.assert_called_once_with('true')
        client.close.assert_called_once_with()

    def test_exec_output_stderr(self):
        self.ssh_conn.get_connected_client = MagicMock()
        client = self.ssh_conn.get_connected_client.return_value
        client.exec_command.return_value = (None, StringIO('out too'), StringIO('xx'))
        self.assertEqual(self.ssh_conn.exec_output('false'), None)
        client.exec_command.assert_called_once_with('false')
        client.close.assert_called_once_with()


if __name__ == '__main__':
    unittest.main()
