import unittest
from mock import MagicMock, patch


class ScanlineVCenterHostScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_pysphere = MagicMock(name='pysphere')
        self.mock_utilities = MagicMock()
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'pysphere': self.mock_pysphere,
            'scanline.utilities.vmware': self.mock_utilities
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.vmware import vCenterHostScanner
        self.vCenterHostScanner = vCenterHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_basic_check(self):
        scanner = self.vCenterHostScanner('host2', '167.81.183.99', 'tester', 'CryptThis', tags=['x', 'y'])
        expected = {'clusters': {}, 'datacenters': {}, 'hosts': {}, 'resource_pools': {}, 'vms': []}
        self.assertEqual(scanner.get_facts(), expected)


if __name__ == '__main__':
    unittest.main()
