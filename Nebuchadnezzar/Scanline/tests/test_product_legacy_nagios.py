import unittest
from mock import MagicMock, patch, PropertyMock


class ScanlineLegacyNagiosScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_pynag = MagicMock(name='pynag')
        self.mock_utilities = MagicMock()
        self.mock_host = MagicMock()
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'pynag': self.mock_pynag,
            'pynag.Parsers': self.mock_pynag.Parsers,
            'scanline.utilities.ssh': self.mock_utilities.ssh,
            'from scanline.host.legacy_nagios': self.mock_host.legacy_nagios
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.legacy_nagios import LegacyNagiosProductScanner
        self.LegacyNagiosProductScanner = LegacyNagiosProductScanner
        self.mock_host_scanner = MagicMock(name='HostScanner')
        self.ln_scanner = self.LegacyNagiosProductScanner('LN', '167.81.187.93', 'admin', 'password', tags=['N'], product_id='PID')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_raw_config_command(self):
        self.assertEqual(
            self.ln_scanner.get_raw_config_read_command(),
            "GLOBIGNORE='/etc/nagiosql/hosts/check_mk_templates.cfg'; cat /etc/nagiosql/hosts/*.cfg"
        )

    @patch('scanline.product.legacy_nagios.SSHConnection')
    def test_get_raw_config(self, mock_ssh):
        self.ln_scanner.get_raw_config_read_command = MagicMock()
        mock_ssh.return_value.exec_output.return_value = 'bla'
        self.assertEqual(self.ln_scanner.get_raw_config(), 'bla')
        mock_ssh.assert_called_once_with('167.81.187.93', 'admin', 'password')
        mock_ssh.return_value.exec_output.assert_called_once_with(
            self.ln_scanner.get_raw_config_read_command.return_value
        )

    @patch('scanline.product.legacy_nagios.LegacyNagiosProductScanner.raw_config', new_callable=PropertyMock)
    def test_get_parsed_config(self, mock_raw_config):
        mock_config = self.mock_pynag.Parsers.config
        self.assertEqual(self.ln_scanner.get_parsed_config(), mock_config.return_value.parse_string.return_value)
        mock_config.return_value.parse_string.assert_called_once_with(mock_raw_config.return_value)

    @patch('scanline.product.legacy_nagios.LegacyNagiosProductScanner.parsed_config', new_callable=PropertyMock)
    def test_get_hosts(self, mock_parsed_config):
        mock_parsed_config.return_value = [
            {'meta': {'object_type': 'host'}, 'host_name': 'one'},
            {'meta': {'object_type': 'host'}, 'host_name': 'localhost'},
            {'name': 'two'},
            {'meta': {'object_type': 'service'}, 'host_name': 'three'},
            {'meta': {'object_type': 'service'}, 'host_name': 'three'},
            {'meta': {'object_type': 'host'}, 'host_name': 'four'},
            {'meta': {}, 'host_name': 'five'},
        ]
        self.assertEqual(
            list(self.ln_scanner.get_hosts()),
            [
                {'meta': {'object_type': 'host'}, 'host_name': 'one'},
                {'meta': {'object_type': 'host'}, 'host_name': 'four'}
            ]
        )

    def test_get_hostname(self):
        self.assertEqual(self.ln_scanner.get_hostname({'host_name': 'host1'}), 'host1')

    @patch('scanline.product.legacy_nagios.LegacyNagiosHostScanner')
    def test_scan_host(self, mock_ln_host_scanner):
        mock_ln_host_scanner.return_value.to_dict.return_value = {
            'address': '10.4.5.6',
            'LN': {'alias': 'HOST1', 'use': 'fake-servers'}
        }
        self.ln_scanner.host_scanner = MagicMock()
        self.ln_scanner.host_scanner.return_value.to_dict.return_value = {'ModR': {'key1': 'val1'}}
        self.assertEqual(
            self.ln_scanner.scan_host({'host_name': 'host1'}),
            {
                'LN': {'alias': 'HOST1', 'use': 'fake-servers'},
                'ModR': {'key1': 'val1'},
                'product_id': 'PID',
                'address': '10.4.5.6'
            }
        )


if __name__ == '__main__':
    unittest.main()
