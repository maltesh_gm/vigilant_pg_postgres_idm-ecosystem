import unittest
from mock import MagicMock, patch


class ScanlineVMwareProductScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_host = MagicMock(name='scanline_host')
        self.mock_product = MagicMock(name='scanline_product')
        modules = {
            'logging': MagicMock(name='logging'),
            'scanline.product.esxi': self.mock_product.esxi,
            'scanline.host.vmware': self.mock_host.vmware
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.vmware import VMwareProductScanner
        self.VMwareProductScanner = VMwareProductScanner
        self.vmware_scanner = self.VMwareProductScanner('vCenter', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'],  product_id='PID')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_site_facts(self):
        self.vmware_scanner.vcenter_scanner = MagicMock(name='vcenter_scanner')
        self.vmware_scanner.vcenter_scanner.site_facts.return_value = {
            'host2': {'key1': 'val1'}
        }
        self.vmware_scanner.esxi_scanner.site_facts.return_value = {
            'host1': {'key1': 'val4'}
        }
        self.assertEqual(
            self.vmware_scanner.site_facts(),
            {'host2': {'key1': 'val1'}, 'host1': {'key1': 'val4'}, 'product_id': 'PID'}
        )

if __name__ == '__main__':
    unittest.main()
