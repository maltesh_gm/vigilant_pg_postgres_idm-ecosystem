import unittest
from mock import MagicMock, patch


class ScanlineHostSANTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
            modules = {
                'cached_property': self.mock_cached_property,
                'logging': MagicMock(name='logging'),
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()

        def test_properties_present(self):
            mock_scanner = MagicMock(spec=self.scanner_instance)
            for prop in self.scanner.module_properties:
                self.assertTrue(getattr(mock_scanner, prop))
            for prop in self.scanner.general_properties:
                self.assertTrue(getattr(mock_scanner, prop))


class ScanlineHostHPMSASANTestCase(ScanlineHostSANTest.TestCase):
    def setUp(self):
        ScanlineHostSANTest.TestCase.setUp(self)
        from scanline.host.san import HPMSASANHostScanner
        self.scanner = HPMSASANHostScanner
        self.scanner_instance = self.scanner(
            'host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y']
        )

    def tearDown(self):
        ScanlineHostSANTest.TestCase.tearDown(self)


class ScanlineHostDellEqualLogicSANTestCase(ScanlineHostSANTest.TestCase):
    def setUp(self):
        ScanlineHostSANTest.TestCase.setUp(self)
        from scanline.host.san import DellEqualLogicSANHostScanner
        self.scanner = DellEqualLogicSANHostScanner
        self.scanner_instance = self.scanner(
            'host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y']
        )

    def tearDown(self):
        ScanlineHostSANTest.TestCase.tearDown(self)


class ScanlineHostIBMStorwizeSANTestCase(ScanlineHostSANTest.TestCase):
    def setUp(self):
        ScanlineHostSANTest.TestCase.setUp(self)
        from scanline.host.san import IBMStorwizeSANHostScanner
        self.scanner = IBMStorwizeSANHostScanner
        self.scanner_instance = self.scanner(
            'host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y']
        )

    def tearDown(self):
        ScanlineHostSANTest.TestCase.tearDown(self)


if __name__ == '__main__':
    unittest.main()
