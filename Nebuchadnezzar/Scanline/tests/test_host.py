import unittest

from mock import MagicMock, patch, call


FoundComponent = MagicMock(**{'scan.return_value': True})
NotFoundComponent = MagicMock(**{'scan.return_value': False})
NoAccess = MagicMock(**{'scan.return_value': False})


class ScanlineHostTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_utilities = MagicMock()
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.utilities.dns': self.mock_utilities.dns
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host import HostScanner
        self.HostScanner = HostScanner
        self.host_scanner = self.HostScanner('host1', {'address': '10.5.6.7', 'scanner': 'scan1'}, tags=['x', 'l'])

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_component_flags(self):
        components = {
            'anywhere': FoundComponent,
            'vlcapture': FoundComponent,
            'cca': NotFoundComponent
        }
        self.host_scanner.get_components = MagicMock(return_value=components)
        self.assertEqual(sorted(self.host_scanner.flags), sorted(['vlcapture', 'anywhere']))

    def test_get_component_flags_non_found(self):
        components = {
            'anywhere': NotFoundComponent,
            'vlcapture': NotFoundComponent,
            'cca': NotFoundComponent
        }
        self.host_scanner.get_components = MagicMock(return_value=components)
        self.assertEqual(sorted(self.host_scanner.flags), [])

    def test_get_component_flags_no_components(self):
        self.assertEqual(sorted(self.host_scanner.flags), [])

    def test_get_address(self):
        self.assertEqual(self.host_scanner.address, self.mock_utilities.dns.get_address.return_value)
        self.mock_utilities.dns.get_address.assert_called_once_with('host1', False)

    def test_get_found_properties(self):
        self.host_scanner.prop1 = 'one'
        self.host_scanner.prop2 = 'two'
        self.assertEqual(
            self.host_scanner.get_found_properties(['prop1', 'prop2']),
            {'prop1': 'one', 'prop2': 'two'}
        )

    def test_get_found_properties_missing_prop(self):
        self.host_scanner.prop1 = 'one'
        self.assertEqual(
            self.host_scanner.get_found_properties(['prop1', 'prop2']),
            {'prop1': 'one'}
        )

    def test_get_found_properties_prop_with_none(self):
        self.host_scanner.prop1 = None
        self.host_scanner.prop2 = 'two'
        self.assertEqual(
            self.host_scanner.get_found_properties(['prop1', 'prop2']),
            {'prop2': 'two'}
        )

    def test_to_dict(self):
        expected_gen_props = {'g_prop1': 'g_val_1', 'g_prop2': 'g_val_2'}
        expected_mod_props = {'m_prop1': 'm_val_1', 'm_prop2': 'm_val_2'}
        self.HostScanner.general_properties = expected_gen_props.keys()
        self.HostScanner.module_properties = expected_mod_props.keys()
        mock_found_props = MagicMock(name='get_found_properties')
        mock_found_props.side_effect = [expected_gen_props, expected_mod_props]
        self.host_scanner.get_found_properties = mock_found_props
        self.assertEqual(
            self.host_scanner.to_dict(),
            {
                'g_prop1': 'g_val_1',
                'g_prop2': 'g_val_2',
                self.HostScanner.module_name: {
                    'm_prop2': 'm_val_2',
                    'm_prop1': 'm_val_1'
                }
            }
        )
        mock_found_props.assert_has_calls([call(['g_prop1', 'g_prop2']), call(['m_prop2', 'm_prop1'])])

    def test_to_dict_without_access(self):
        expected_gen_props = {'g_prop1': 'g_val_1', 'g_prop2': 'g_val_2'}
        expected_mod_props = {'m_prop1': 'm_val_1', 'm_prop2': 'm_val_2'}
        self.HostScanner.general_properties = expected_gen_props.keys()
        self.HostScanner.module_properties = expected_mod_props.keys()
        mock_found_props = MagicMock(name='get_found_properties')
        mock_found_props.side_effect = [expected_gen_props, expected_mod_props]
        self.host_scanner.get_found_properties = mock_found_props
        self.host_scanner.to_dict =MagicMock(name='to_dict', return_value= {})
        self.assertEqual(self.host_scanner.to_dict(), {})


if __name__ == '__main__':
    unittest.main()
