import base64
from Crypto.Cipher import AES

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[:-ord(s[len(s)-1:])]


class AESCipher(object):
    def __init__(self, key_byte_str='', iv_byte_str=''):
        '''
        key_byte_str = '\xa6Ft\xe2\xfd\xde:\x85t,\x18\x11\xfc\xc3\xfb\x10H\x0e\xc3\xc6\xe8L\x93;m\x8d\x1ch\xf9Zw\xa0'
        iv_byte_str  = '6\x1b\x9fS\xb2M`V\xe12-\xe6&X\x02K'
        '''

        self.key_byte_str = key_byte_str
        self.iv_byte_str  = iv_byte_str

    def encrypt(self, raw):
        raw = pad(raw)
        cipher = AES.new(self.key_byte_str, AES.MODE_CBC, self.iv_byte_str)
        return base64.b64encode(cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        cipher = AES.new(self.key_byte_str, AES.MODE_CBC, self.iv_byte_str)
        return unpad(cipher.decrypt(enc))

    @classmethod
    def hex_ary_to_byte_str(cls, hex_array):
        return ''.join(chr(x) for x in hex_array)


if __name__  == '__main__':
    '''
       key_hex_ary and iv_hex_ary are taken from AESEncryptor.cs file
       from Stentor.Server.Security.Cryptography namespace of UDM
    '''

    key_hex_ary = [0xA6, 0x46, 0x74, 0xE2, 0xFD, 0xDE, 0x3A, 0x85,
                   0x74, 0x2C, 0x18, 0x11, 0xFC, 0xC3, 0xFB, 0x10,
                   0x48, 0x0E, 0xC3, 0xC6, 0xE8, 0x4C, 0x93, 0x3B,
                   0x6D, 0x8D, 0x1C, 0x68, 0xF9, 0x5A, 0x77, 0xA0]
    iv_hex_ary =  [0x36, 0x1B, 0x9F, 0x53, 0xB2, 0x4D, 0x60, 0x56,
                   0xE1, 0x32, 0x2D, 0xE6, 0x26, 0x58, 0x02, 0x4B]

    aes = AESCipher(
        key_byte_str = AESCipher.hex_ary_to_byte_str(key_hex_ary),
        iv_byte_str  = AESCipher.hex_ary_to_byte_str(iv_hex_ary)
    )

    to_encrypt = 'P@ssw0rd'
    print 'To encrypt : %s' % to_encrypt
    enc = aes.encrypt(to_encrypt)
    print 'Encrypted : %s' % enc
    dec = aes.decrypt(enc)
    print 'Decrypted : %s' % dec

    to_encrypt = 'phcblrudm01'
    print 'To encrypt : %s' % to_encrypt
    enc = aes.encrypt(to_encrypt)
    print 'Encrypted : %s' % enc
    dec = aes.decrypt(enc)
    print 'Decrypted : %s' % dec
