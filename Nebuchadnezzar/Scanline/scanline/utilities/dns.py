import socket
import logging


logger = logging.getLogger(__name__)


def get_address(hostname, ipv6_enabled=False):
    try:
        return str(hostname) if ipv6_enabled else socket.gethostbyname(str(hostname))
    except socket.gaierror:
        logger.exception('Could not find address for %s', hostname)
