import logging
from contextlib import contextmanager
from pysphere import VIServer


logger = logging.getLogger(__name__)


@contextmanager
def create_vi_server(host, username, password):
    server = VIServer()
    try:
        logger.debug('creating connection to %s', host)
        server.connect(host, username, password)  # sock_timeout
        logger.debug('connected to %s', host)
        yield server
    finally:
        logger.debug('closing connection to %s', host)
        if server:
            server.disconnect()
        logger.debug('closed %s', host)
