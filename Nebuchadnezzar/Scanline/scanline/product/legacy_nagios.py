import logging
from pynag.Parsers import config
from cached_property import cached_property
from scanline.utilities.ssh import SSHConnection
from scanline.product import ProductScanner
from scanline.host.legacy_nagios import LegacyNagiosHostScanner


logger = logging.getLogger(__name__)


class LegacyNagiosProductScanner(ProductScanner):
    CAT_FILES = "GLOBIGNORE='{globignore}'; cat {fileglob}"
    GLOBIGNORE = '/etc/nagiosql/hosts/check_mk_templates.cfg'
    FILEGLOB = '/etc/nagiosql/hosts/*.cfg'

    def __init__(self, scanner, address, username, password, tags=None, host_scanner=None, product_id=None, **kwargs):
        super(LegacyNagiosProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, product_id=product_id, **kwargs)
        self.username = username
        self.password = password
        self.product_id = product_id

    @cached_property
    def raw_config(self):
        return self.get_raw_config()

    @cached_property
    def parsed_config(self):
        return self.get_parsed_config()

    def get_raw_config_read_command(self):
        return self.CAT_FILES.format(globignore=self.GLOBIGNORE, fileglob=self.FILEGLOB)

    def get_raw_config(self):
        ssh = SSHConnection(self.address, self.username, self.password)
        raw_config = ssh.exec_output(self.get_raw_config_read_command())
        if raw_config is None:
            logger.error('Could not read raw config from %s.', self.address)
            return ''
        return raw_config

    def get_parsed_config(self):
        parser = config()
        return parser.parse_string(self.raw_config)

    def get_hosts(self):
        for config_object in self.parsed_config:
            try:
                if config_object['meta']['object_type'] == 'host':
                    if self.get_hostname(config_object) == 'localhost':
                        continue
                    yield config_object
            except KeyError:
                pass

    def get_hostname(self, host):
        return host['host_name']

    def scan_host(self, host):
        scanner = LegacyNagiosHostScanner(
            endpoint=self.endpoint,
            host_config=host,
            tags=self.tags
        )
        result = scanner.to_dict()
        if self.host_scanner is not None:
            other_scanner = self.host_scanner(
                hostname=self.get_hostname(host),
                endpoint=self.endpoint,
                username=self.username,
                password=self.password,
                tags=self.tags
            )
            result.update({'product_id': self.product_id})
            result.update(other_scanner.to_dict())
        return result
