import logging
from pyVim import connect
from pyVmomi import vmodl
from pyVmomi import vim
from scanline.product import ProductScanner
from scanline.host.esxi import ESXiHostScanner


logger = logging.getLogger(__name__)


class ESXiProductScanner(ProductScanner):
    def __init__(self, scanner, address, username, password, tags=None, host_scanner=None, product_id=None, **kwargs):
        super(ESXiProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, product_id=product_id, **kwargs)
        self.username = username
        self.password = password
        self.product_id = product_id

    def get_hosts(self):
        service_instance = None
        try:
            service_instance = connect.SmartConnect(host=self.address, user=self.username, pwd=self.password)
            content = service_instance.RetrieveContent()
            container_view = content.viewManager.CreateContainerView(
                container=content.rootFolder, type=[vim.HostSystem], recursive=True
            )
            for child in container_view.view:
                yield child
        except vmodl.MethodFault as error:
            logger.error('Caught vmodl fault : %s', error.msg)
        finally:
            connect.Disconnect(service_instance)

    def get_hostname(self, host):
        return ESXiHostScanner.get_hostname(host)

    def scan_host(self, host):
        esxi_scanner = ESXiHostScanner(vmware_host=host, endpoint=self.endpoint, tags=self.tags)
        result = esxi_scanner.to_dict()
        if self.host_scanner is not None:
            params = dict(
                hostname=self.get_hostname(host),
                endpoint=self.endpoint,
                username=self.username,
                password=self.password,
                tags=self.tags
            )
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
            result.update({"product_id": self.product_id})
        return result
