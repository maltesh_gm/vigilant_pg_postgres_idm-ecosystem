import logging
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class IECGHostScanner(WindowsHostScanner):
    module_name = 'iECG'
    def __init__(self, hostname, endpoint, tags=None, product_id='iECG',
                 product_name='iECG', product_version='NA', role='NA', **kwargs):
        super(IECGHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, role=role, **kwargs)
