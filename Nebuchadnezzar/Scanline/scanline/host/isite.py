import logging
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class ISiteHostScanner(WindowsHostScanner):
    module_name = 'iSite'

    def __init__(self, hostname, endpoint, tags=None, product_id='iSite',
                 product_name='iStite Billing', product_version='NA', **kwargs):
        super(ISiteHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, **kwargs)
