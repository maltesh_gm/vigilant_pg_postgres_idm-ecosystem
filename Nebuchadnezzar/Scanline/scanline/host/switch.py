import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class CiscoSwitchHostScanner(HostScanner):
    module_name = 'CiscoSwitch'


class HPSwitchHostScanner(HostScanner):
    module_name = 'HPSwitch'
