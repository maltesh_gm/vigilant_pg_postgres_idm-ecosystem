import logging
from cached_property import cached_property
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class LegacyNagiosHostScanner(HostScanner):
    module_name = 'LN'
    module_properties = HostScanner.module_properties.union(['alias', 'use'])

    def __init__(self, host_config, endpoint, tags=None, **kwargs):
        super(LegacyNagiosHostScanner, self).__init__(host_config['host_name'], endpoint, tags=tags, **kwargs)
        self.host_config_object = host_config

    @cached_property
    def address(self):
        return self.host_config_object.get('address')

    @cached_property
    def alias(self):
        return self.host_config_object.get('alias')

    @cached_property
    def use(self):
        return self.host_config_object.get('use')
