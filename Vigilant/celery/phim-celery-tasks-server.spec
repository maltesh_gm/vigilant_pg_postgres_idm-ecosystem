# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      phim-celery-tasks-server
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Phim celery tasks for server

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  phim-celery-tasks-taskbase
Requires:  phimutils
Requires:  poperator
Provides:  phim-celery-tasks-server
Conflicts: phim-celery-tasks-client

%description
Phim celery tasks for server


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_prefix}/lib/celery
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/celeryconfig.py %{buildroot}%{_prefix}/lib/celery/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/tbconfig.py %{buildroot}%{_prefix}/lib/celery/
%{__install} -d -m 0755 %{buildroot}%{_prefix}/lib/celery/phim_backoffice
%{__cp} %{_builddir}/%{name}-%{version}/phim_backoffice/*.py %{buildroot}%{_prefix}/lib/celery/phim_backoffice/
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/default
%{__cp} %{_builddir}/%{name}-%{version}/celeryd-default %{buildroot}%{_sysconfdir}/default/celeryd

%post
%{__install} -d -m 0775 -o celery -g celery %{_var}/philips/operator

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/default/celeryd
%attr(0755,root,root) %{_prefix}/lib/celery/celeryconfig.py
%attr(0755,root,root) %{_prefix}/lib/celery/tbconfig.py
%attr(0755,root,root) %{_prefix}/lib/celery/phim_backoffice

%changelog
* Thu Nov 06 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added operator work folder creation

* Thu Jun 26 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Changed package file structure

* Wed Feb 26 2014 Jason Malobicky <jason.malobicky@philips.com>
- Added tasks for hbbeat mail transport

* Wed Feb 19 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added tasks for discovery

* Thu Jan 30 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added a config file to map sites to email addresses

* Wed Dec 11 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added email output for stale alive

* Fri Oct 25 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
