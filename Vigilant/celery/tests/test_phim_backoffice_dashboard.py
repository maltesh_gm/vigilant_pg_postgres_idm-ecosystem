import unittest
from mock import MagicMock, patch, DEFAULT, call


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class PBDashboardTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_tbconfig = MagicMock(name='tbconfig')
            self.mock_pb = MagicMock(name='phim_backoffice')
            self.mock_pb.celery.app.task = fakeorator_arg
            self.mock_celery = MagicMock(name='celery')
            modules = {
                'tbconfig': self.mock_tbconfig,
                'phim_backoffice.datastore': self.mock_pb.datastore,
                'phim_backoffice.kvstore': self.mock_pb.kvstore,
                'phim_backoffice.celery': self.mock_pb.celery,
                'celery': self.mock_celery,
                'celery.utils': self.mock_celery.utils,
                'celery.utils.log': self.mock_celery.utils.log
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import phim_backoffice.dashboard
            self.dashboard = phim_backoffice.dashboard
            self.dashboard.DASHBOARD = {}
            self.dashboard.DASHBOARD['UNREACHABLE_SITE_KEY'] = 'key1'
            self.dashboard.DASHBOARD['UNREACHABLE_HOST_KEY'] = 'key2'
            self.dashboard.UNREACHABLE_KEYS = ['key1', 'key2']
            self.dashboard.DASHBOARD['UNREACHABLE_SITES'] = 'unreachable_s'
            self.dashboard.DASHBOARD['PROBLEM_SITES'] = 'problem_sites'

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class DashboardTestCase(PBDashboardTest.TestCase):
    def setUp(self):
        PBDashboardTest.TestCase.setUp(self)

    def tearDown(self):
        PBDashboardTest.TestCase.tearDown(self)

    def test_guess_component_version(self):
        component = 'IntelliSpace PACS'
        self.dashboard.aggregate.return_value = iter([{'version': '4,4,4,4'}])
        self.assertEqual(self.dashboard.guess_component_version('XYZ00', component),
                         '4,4,4,4')
        self.dashboard.aggregate.assert_called_once_with(
            'FACTS',
            [
                {'$match': {'Components.name': component, 'siteid': 'XYZ00'}},
                {'$limit': 1},
                {'$unwind': '$Components'},
                {'$match': {'Components.name': component}},
                {'$sort': {'Components.timestamp': -1 }},
                {'$project': {'_id': 0, 'version': '$Components.version'}}
            ]
        )

    def test_guess_component_version_no_result(self):
        component = 'IntelliSpace PACS'
        self.dashboard.aggregate.return_value = iter([])
        self.assertEqual(self.dashboard.guess_component_version('XYZ00', component),
                         None)
        self.dashboard.aggregate.assert_called_once_with(
            'FACTS',
            [
                {'$match': {'Components.name': component, 'siteid': 'XYZ00'}},
                {'$limit': 1},
                {'$unwind': '$Components'},
                {'$match': {'Components.name': component}},
                {'$sort': {'Components.timestamp': -1 }},
                {'$project': {'_id': 0, 'version': '$Components.version'}}
            ]
        )

    def test_get_product_version(self):
        self.dashboard.aggregate.return_value = iter([{'version': '4,4,4,4'}])
        self.assertEqual(self.dashboard.get_product_version('XYZ00', 'host01'),
                         '4,4,4,4')
        self.dashboard.aggregate.assert_called_once_with(
            'FACTS',
            [
                {'$match': {'_id': 'XYZ00-host01'}},
                {'$project': {'_id': 0, 'version': '$product_version'}}
            ]
        )

    def test_get_product_version_no_result(self):
        self.dashboard.aggregate.return_value = iter([])
        self.assertEqual(self.dashboard.get_product_version('XYZ00', 'host01'),
                         None)
        self.dashboard.aggregate.assert_called_once_with(
            'FACTS',
            [
                {'$match': {'_id': 'XYZ00-host01'}},
                {'$project': {'_id': 0, 'version': '$product_version'}}
            ]
        )

    def test_get_site_info(self):
        self.dashboard.DASHBOARD = {'DEFAULT_SITENAME': 'default_site'}
        self.dashboard.notifying_get_data = MagicMock(
            name='notifying_get_data', return_value={'name': 'some site 1'}
        )
        self.dashboard.guess_component_version = MagicMock(
            name='guess_component_version', return_value='4,4,4,4'
        )
        self.assertEqual(
            self.dashboard.get_site_info('site01', 'host01'),
            {'name': 'some site 1', 'pacs_version': '4,4,4,4'}
        )
        self.dashboard.notifying_get_data.assert_called_once_with('site', 'SITES',
                                                                  'site01',
                                                                  {'name': 'default_site'})
        self.dashboard.guess_component_version.assert_called_once_with('site01',
                                                                       'IntelliSpace PACS')

    def test_get_site_info_no_pacs_version(self):
        self.dashboard.DASHBOARD = {'DEFAULT_SITENAME': 'default_site'}
        self.dashboard.notifying_get_data = MagicMock(
            name='notifying_get_data', return_value={'name': 'some site 1'}
        )
        self.dashboard.guess_component_version = MagicMock(
            name='guess_component_version', return_value=None
        )
        self.dashboard.get_product_version = MagicMock(
            name='get_product_version', return_value=None
        )
        self.assertEqual(self.dashboard.get_site_info('site01',
                                                      'ops25if1.ops25.isyntax.net'),
                         {'name': 'some site 1'})
        self.dashboard.notifying_get_data.assert_called_once_with('site', 'SITES',
                                                                  'site01',
                                                                  {'name': 'default_site'})
        self.dashboard.guess_component_version.assert_called_once_with('site01',
                                                                       'IntelliSpace PACS')

    def test_create_exception(self):
        self.dashboard.create_exception('servicename', 'service_1')
        self.dashboard.upsert_one_by_id.assert_called_once_with('EXCEPTIONS',
                                                                'service_1',
                                                                {'servicename': 'service_1'})

    def test_notifying_get_data_default(self):
        self.dashboard.create_exception = MagicMock(name='create_exception')
        self.dashboard.find_one.return_value = None
        self.assertEqual(
            self.dashboard.notifying_get_data('siteid', 'TESTS', 'site01', {'xkey': 'xval'}),
            {'xkey': 'xval'}
        )
        self.dashboard.find_one.assert_called_once_with('TESTS', 'site01', {'_id': 0})
        self.dashboard.create_exception.assert_called_once_with('siteid', 'site01')

    def test_notifying_get_data(self):
        self.dashboard.create_exception = MagicMock(name='create_exception')
        self.dashboard.find_one.return_value = {'ykey': 'yval'}
        self.assertEqual(
            self.dashboard.notifying_get_data('siteid', 'TESTS', 'site01', {'xkey': 'xval'}),
            {'ykey': 'yval'}
        )
        self.dashboard.find_one.assert_called_once_with('TESTS', 'site01', {'_id': 0})
        self.assertFalse(self.dashboard.create_exception.called)


class DashboardHandleDashboardTestCase(PBDashboardTest.TestCase):
    def setUp(self):
        PBDashboardTest.TestCase.setUp(self)
        self.data = {
            'siteid': 'site01',
            'hostname': 'host01',
            'service': 'x__y__z',
            'type': 'PROBLEM',
            'timestamp': '2013-09-10T22:03:00.456000Z',
            'namespace' : {
                'category' : 'n',
                'attribute' : None,
                'object' : 'z',
                'vendor' : 'y',
                'dimension' : None
            },
        }

    def test_get_identifier(self):
        self.assertEqual(self.dashboard.get_identifier(self.data), 'site01-host01-x__y__z')

    def test_handle_dashboard_no_action(self):
        self.dashboard.handle_problem = MagicMock(name='problem')
        self.dashboard.handle_recovery = MagicMock(name='recovery')
        self.data['type'] = 'HANDLER'
        self.dashboard.handle_dashboard(self.data)
        self.assertFalse(self.dashboard.handle_problem.called)
        self.assertFalse(self.dashboard.handle_recovery.called)

    def test_handle_dashboard_recovery(self):
        self.dashboard.handle_problem = MagicMock(name='problem')
        self.dashboard.handle_recovery = MagicMock(name='recovery')
        self.data['type'] = 'RECOVERY'
        self.dashboard.handle_dashboard(self.data)
        self.assertFalse(self.dashboard.handle_problem.called)
        self.dashboard.handle_recovery.assert_called_once_with(self.data)

    def test_handle_recovery(self):
        self.data['type'] = 'RECOVERY'
        self.dashboard.remove_from_problems = MagicMock(name='remove_from_problems')
        self.dashboard.handle_recovery(self.data)
        self.dashboard.delete_one_by_id.assert_called_once_with(
            'DASHBOARD', 'site01-host01-x__y__z'
        )
        self.dashboard.remove_from_problems.assert_called_once_with('site01', 'host01', 'x__y__z')

    def test_get_dashboard_document_initial_insertion_reachable(self):
        self.dashboard.get_site_info = MagicMock(name='get_site_info')
        self.dashboard.is_unreachable_service = MagicMock(name='is_unreachable_service',
                                                          return_value=False)
        self.dashboard.id_exists.return_value = False
        self.assertEqual(
            self.dashboard.get_dashboard_document('site01-host01-x__y__z', self.data),
            {
                'service': 'x__y__z',
                'timestamp': '2013-09-10T22:03:00.456000Z',
                'hostname': 'host01',
                'siteid': 'site01',
                'site': self.dashboard.get_site_info.return_value,
                'initial_timestamp': '2013-09-10T22:03:00.456000Z',
                'type': 'PROBLEM',
                'namespace': {'category': 'n', 'attribute': None, 'object': 'z',
                              'vendor': 'y', 'dimension': None}
            }
        )
        self.dashboard.get_site_info.assert_called_once_with('site01', 'host01')
        self.dashboard.is_unreachable_service.assert_called_once_with('site01', 'host01', 'x__y__z')

    def test_get_dashboard_document_existing_entry_unreachable_service(self):
        self.dashboard.get_site_info = MagicMock(name='get_site_info')
        self.dashboard.is_unreachable_service = MagicMock(name='is_unreachable_service',
                                                          return_value=True)
        self.dashboard.id_exists.return_value = True
        self.assertEqual(
            self.dashboard.get_dashboard_document('site01-host01-x__y__z', self.data),
            {
                'service': 'x__y__z',
                'timestamp': '2013-09-10T22:03:00.456000Z',
                'hostname': 'host01',
                'siteid': 'site01',
                'site': self.dashboard.get_site_info.return_value,
                'type': 'PROBLEM',
                'unreachable': True,
                'namespace': {'category': 'n', 'attribute': None, 'object': 'z',
                              'vendor': 'y', 'dimension': None}
            }
        )
        self.dashboard.get_site_info.assert_called_once_with('site01', 'host01')
        self.dashboard.is_unreachable_service.assert_called_once_with('site01',
                                                                      'host01',
                                                                      'x__y__z')

    def test_get_unreachable_hosts_key(self):
        self.dashboard.DASHBOARD['UNREACHABLE_HOSTS'] = 'unreachable_hosts'
        self.assertEqual(self.dashboard.get_unreachable_hosts_key('XYZ01'),
                         'unreachable_hosts-XYZ01')

    def test_handle_problem(self):
        self.dashboard.get_identifier = MagicMock(name='get_identifier')
        self.dashboard.add_to_problems = MagicMock(name='add_to_problems')
        self.dashboard.get_dashboard_document = MagicMock(name='get_dashboard_document')
        self.dashboard.handle_problem(self.data)
        self.dashboard.add_to_problems.assert_called_once_with('site01', 'host01', 'x__y__z')
        self.dashboard.upsert_one_by_id.assert_called_once_with(
            'DASHBOARD',
            self.dashboard.get_identifier.return_value,
            self.dashboard.get_dashboard_document.return_value
        )

    def test_set_services_unreachable(self):
        self.dashboard.set_services_unreachable('site1', hostname={'$ne': 'badhost'})
        self.dashboard.update_many.assert_called_once_with(
            'DASHBOARD',
            {'hostname': {'$ne': 'badhost'}, 'siteid': 'site1'},
            {'$set': {'unreachable': True}}
        )

    def test_set_host_services_unreachable(self):
        self.dashboard.set_services_unreachable = MagicMock(name='set_services_unreachable')
        self.dashboard.set_host_services_unreachable('abc01', 'host01')
        self.dashboard.set_services_unreachable.assert_called_once_with(
            hostname='host01', service={'$nin': ['key1', 'key2']}, siteid='abc01'
        )

    def test_set_site_services_unreachable(self):
        self.dashboard.set_services_unreachable = MagicMock(name='set_services_unreachable')
        self.dashboard.set_site_services_unreachable('abc01')
        self.dashboard.set_services_unreachable.assert_called_once_with(
            service={'$ne': 'key1'}, siteid='abc01'
        )

    def test_set_services_reachable(self):
        self.dashboard.set_services_reachable('site1', hostname='host01')
        self.dashboard.update_many.assert_called_once_with(
            'DASHBOARD', {'hostname': 'host01', 'siteid': 'site1'}, {'$unset':
                                                                         {'unreachable': None}}
        )

    def test_set_host_services_reachable(self):
        self.dashboard.set_services_reachable = MagicMock(name='set_services_unreachable')
        self.dashboard.set_host_services_reachable('abc01', 'host01')
        self.dashboard.set_services_reachable.assert_called_once_with(
            hostname='host01', siteid='abc01'
        )

    def test_set_site_services_reachable(self):
        self.dashboard.get_members_of_set.return_value = set(['XYZ00', 'ABC02'])
        self.dashboard.get_unreachable_hosts_key = MagicMock(name='get_unreachable_hosts_key')
        self.dashboard.set_services_reachable = MagicMock(name='set_services_unreachable')
        self.dashboard.set_site_services_reachable('site1')
        self.dashboard.get_unreachable_hosts_key.assert_called_once_with('site1')
        self.dashboard.get_members_of_set.assert_called_once_with(
            self.dashboard.get_unreachable_hosts_key.return_value
        )
        self.assertEqual(
            self.dashboard.set_services_reachable.mock_calls,
            [call(service='key2', siteid='site1'), call(hostname={'$nin':
                                                                      ['XYZ00',
                                                                       'ABC02']},
                                                        siteid='site1')]
        )

    def test_rebuild_problems(self):
        self.dashboard.add_to_set = MagicMock(name='add_to_set')
        self.dashboard.delete_key = MagicMock(name='delete_key')
        self.dashboard.distinct.return_value = ['site1', 'site2']
        self.dashboard.rebuild_problem_sites()
        self.dashboard.distinct.assert_called_once_with('DASHBOARD', 'siteid')
        self.dashboard.delete_key.assert_called_once_with('problem_sites')
        self.dashboard.add_to_set.assert_called_once_with('problem_sites', 'site1', 'site2')

    def test_is_problem_site(self):
        self.dashboard.is_member_of_set = MagicMock(name='is_member_of_set')
        self.assertEqual(
            self.dashboard.is_problem_site('site1'),
            self.dashboard.is_member_of_set.return_value
        )
        self.dashboard.is_member_of_set.assert_called_once_with('problem_sites', 'site1')

    def test_is_unreachable_service_excluded_service(self):
        self.assertFalse(self.dashboard.is_unreachable_service('XYZ01', 'host01', 'key1'))
        self.assertFalse(self.dashboard.is_unreachable_service('XYZ01', 'host01', 'key2'))

    def test_is_unreachable_service_reachable(self):
        self.dashboard.get_unreachable_hosts_key = MagicMock(
            name='get_unreachable_hosts_key', return_value='unreachable-site1'
        )
        self.dashboard.is_member_of_set = MagicMock(name='is_member_of_set',
                                                    return_value=False)
        self.assertFalse(self.dashboard.is_unreachable_service('XYZ01', 'host01', 'key3'))
        self.assertEqual(
            self.dashboard.is_member_of_set.mock_calls,
            [call('unreachable_s', 'XYZ01'), call('unreachable-site1', 'host01')]
        )

    def test_get_problem_services_by_host(self):
        self.assertEqual(
            self.dashboard.get_problem_services_by_host('XYZ00'),
            self.dashboard.aggregate.return_value
        )
        self.dashboard.aggregate.assert_called_once_with(
            'DASHBOARD',
            [
                {'$match': {'siteid': 'XYZ00'}},
                {'$group': {'services': {'$push': '$service'}, '_id': '$hostname'}},
                {'$project': {'services': 1, '_id': 0, 'hostname': '$_id'}}
            ]
        )

    def test_update_case_number(self):
        self.dashboard.get_identifier = MagicMock(name='get_identifier')
        self.dashboard.update_case_number('XYZ00', 'host01', 'service__one', '45667')
        self.dashboard.get_identifier.assert_called_once_with(
            {'hostname': 'host01', 'siteid': 'XYZ00', 'service': 'service__one', 'case_number': '45667'}
        )
        self.dashboard.update_one_by_id.assert_called_once_with(
            'DASHBOARD', self.dashboard.get_identifier.return_value, {'case_number': '45667'}
        )


class DashboardManageProblemsTestCase(PBDashboardTest.TestCase):
    def setUp(self):
        PBDashboardTest.TestCase.setUp(self)
        self.dashboard.add_to_set = MagicMock(name='add_to_set')
        self.dashboard.get_unreachable_hosts_key = MagicMock(name='get_unreachable_hosts_key')
        self.dashboard.set_site_services_unreachable = MagicMock(name='set_site_services_unreachable')
        self.dashboard.set_host_services_unreachable = MagicMock(name='set_host_services_unreachable')
        self.dashboard.rebuild_problem_sites = MagicMock(name='rebuild_problem_sites')
        self.dashboard.remove_from_set = MagicMock(name='remove_from_set')
        self.dashboard.set_site_services_reachable = MagicMock(name='set_site_services_reachable')
        self.dashboard.set_host_services_reachable = MagicMock(name='set_host_services_reachable')

    def tearDown(self):
        PBDashboardTest.TestCase.tearDown(self)

    def test_add_to_problems(self):
        self.dashboard.add_to_problems('site1', 'host01', 'service__one__status')
        self.dashboard.add_to_set.assert_called_once_with('problem_sites', 'site1')
        self.assertFalse(self.dashboard.set_site_services_unreachable.called)
        self.assertFalse(self.dashboard.set_host_services_unreachable.called)

    def test_add_to_problems_unreachable_site_key(self):
        self.dashboard.add_to_problems('site1', 'host01', 'key1')
        self.assertEqual(
            self.dashboard.add_to_set.mock_calls,
            [call('problem_sites', 'site1'), call('unreachable_s', 'site1')]
        )
        self.dashboard.set_site_services_unreachable.assert_called_once_with('site1')

    def test_add_to_problems_unreachable_host_key(self):
        self.dashboard.add_to_problems('site1', 'host01', 'key2')
        self.assertEqual(
            self.dashboard.add_to_set.mock_calls,
            [call('problem_sites', 'site1'), call(self.dashboard.get_unreachable_hosts_key.return_value, 'host01')]
        )
        self.dashboard.get_unreachable_hosts_key.assert_called_once_with('site1')
        self.dashboard.set_host_services_unreachable.assert_called_once_with('site1', 'host01')

    def test_remove_from_problems(self):
        self.dashboard.remove_from_problems('site1', 'host01', 'service__one__status')
        self.dashboard.rebuild_problem_sites.assert_called_once_with()
        self.assertFalse(self.dashboard.set_site_services_reachable.called)
        self.assertFalse(self.dashboard.set_host_services_reachable.called)
        self.assertFalse(self.dashboard.remove_from_set.called)

    def test_remove_from_problems_unreachable_site_key(self):
        self.dashboard.remove_from_problems('site1', 'host01', 'key1')
        self.dashboard.rebuild_problem_sites.assert_called_once_with()
        self.dashboard.remove_from_set.assert_called_once_with('unreachable_s', 'site1')
        self.dashboard.set_site_services_reachable.assert_called_once_with('site1')

    def test_remove_from_problems_unreachable_host_key(self):
        self.dashboard.remove_from_problems('site1', 'host01', 'key2')
        self.dashboard.rebuild_problem_sites.assert_called_once_with()
        self.dashboard.get_unreachable_hosts_key.assert_called_once_with('site1')
        self.dashboard.remove_from_set.assert_called_once_with(
            self.dashboard.get_unreachable_hosts_key.return_value, 'host01'
        )
        self.dashboard.set_host_services_reachable.assert_called_once_with('site1', 'host01')


if __name__ == '__main__':
    unittest.main()

