import unittest
from mock import MagicMock, patch, DEFAULT, call


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class PersistTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        self.mock_temporary = MagicMock(name='temporary')
        self.mock_poperator = MagicMock(name='poperator')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.celery': self.mock_pb.celery,
            'phim_backoffice.helpers': self.mock_pb.helpers,
            'temporary': self.mock_temporary,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
            'poperator': self.mock_poperator,
            'poperator.configwrite': self.mock_poperator.configwrite
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.persist
        self.persist = phim_backoffice.persist
        self.persist.POPERATOR_TYPE_CLASS_MAP = {
            'Nagios': {
                'module': 'poperator.configgenNagios',
                'class': 'ConfiggenNagios'
            },
            'SWD': {
                'module': 'poperator.configgenSWD',
                'class': 'ConfiggenSWD'
            },
            'PCM': {
                'module': 'poperator.configgenPCM',
                'class': 'ConfiggenPCM'
            }
        }
        self.persist.IBC = {
            'SVN_URL': 'http://sc.phim.isyntax.net/svn/ibc/sites/',
            'SVN_USERNAME': 'operator',
            'SVN_PASSWORD': 'superPassword',
            'WORKING_DIRECTORY': '/var/philips/operator'
        }

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_poperator_gen_no_class(self):
        self.persist.poperator_gen('site1', 'XY', {})
        self.persist.get_class.assert_not_called()

    def test_poperator_gen(self):
        self.persist.poperator_gen('site1', 'Nagios', {'key1': 'val1'})
        self.persist.get_class.assert_called_once_with('poperator.configgenNagios', 'ConfiggenNagios')
        self.persist.SVNConfigWriter.assert_called_once_with(
            self.mock_temporary.temp_dir.return_value.__enter__.return_value,
            'http://sc.phim.isyntax.net/svn/ibc/sites/',
            'operator',
            'superPassword',
            'site1'
        )
        self.persist.get_class.return_value.assert_called_once_with({'key1': 'val1'})
        self.persist.get_class.return_value.return_value.generate.assert_called_once_with()
        self.persist.SVNConfigWriter.return_value.write.assert_called_once_with(
            self.persist.get_class.return_value.return_value.generate.return_value
        )

    def test_generate_from_facts(self):
        self.persist.poperator_gen = MagicMock(name='poperator_gen')
        self.persist.find.return_value = ['this is an item']
        self.persist.generate_from_facts('site1', ['host1', 'host2'])
        self.persist.find.assert_called_once_with(
            'FACTS', {'hostname': {'$in': ['host1', 'host2']}, 'siteid': 'site1'}, {'Components.timestamp': 0}
        )
        self.persist.poperator_gen.delay.assert_called_once_with(
            data=['this is an item'], data_type='Nagios', siteid='site1'
        )

    def test_swd_subscribe(self):
        self.persist.poperator_gen = MagicMock(name='poperator_gen')
        self.persist.swd_subscribe(['site1', 'site2'], ['subscription1', 'sub2'])
        self.assertEqual(
            self.persist.poperator_gen.delay.mock_calls,
            [
                call(data=['subscription1', 'sub2'], data_type='SWD', siteid='site1'),
                call(data=['subscription1', 'sub2'], data_type='SWD', siteid='site2')
            ]
        )
    def test_poperator_del(self):
        self.persist.poperator_del('site', 'PCM', {'deployment_id':'id'})
        self.persist.SVNConfigWriter.assert_called_once_with(
             '/tmp',
             'http://sc.phim.isyntax.net/svn/ibc/sites/',
             'operator',
             'superPassword',
             'site'
        )
        self.assertEqual(self.persist.SVNConfigWriter.return_value.remove.call_count,2)

if __name__ == '__main__':
    unittest.main()
