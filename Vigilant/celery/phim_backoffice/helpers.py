
def get_class(module, name):
    mod = __import__(module, fromlist=[name])
    return getattr(mod, name)


def get_identified_host_dictionaries(siteid, host_dictionaries):
    for host, host_dictionary in host_dictionaries.iteritems():
        try:
            host_dictionary['hostname'] = host
            host_dictionary['siteid'] = siteid
        except TypeError:
            # host_dictionary is not a dictionary
            continue
        yield get_identifier(siteid, host), host_dictionary


def get_identifier(siteid, host):
    return '{siteid}-{host}'.format(siteid=siteid, host=host)


def get_validated_fields(fields, **kwargs):
    if not fields.issuperset(kwargs.keys()):
        unexpected_args = list(set(kwargs.keys()).difference(fields))
        raise TypeError('got unexpected arguments {0}'.format(unexpected_args))
    return dict((k, v) for k, v in kwargs.iteritems() if v)
