from __future__ import absolute_import

from celery.utils.log import get_logger
from temporary import temp_dir

from phim_backoffice.celery import app
from phim_backoffice.datastore import find
from phim_backoffice.helpers import get_class
from poperator.configwrite import SVNConfigWriter
from tbconfig import POPERATOR_TYPE_CLASS_MAP, IBC


logger = get_logger(__name__)


@app.task(ignore_result=True)
def poperator_gen(siteid, data_type, data):
    class_map = POPERATOR_TYPE_CLASS_MAP.get(data_type)
    if not class_map:
        logger.error('Persist - No classmap for data type %s', data_type)
        return

    config_generator = get_class(class_map['module'], class_map['class'])
    with temp_dir(prefix='configgen_', parent_dir=IBC['WORKING_DIRECTORY']) as tmp_work_dir:
        logger.debug(
            'Persist Working on data from siteid %s using %s %s at %s',
            siteid,
            class_map['module'],
            class_map['class'],
            tmp_work_dir
        )
        cw = SVNConfigWriter(tmp_work_dir, IBC['SVN_URL'], IBC['SVN_USERNAME'], IBC['SVN_PASSWORD'], siteid)
        gen = config_generator(data)
        config = gen.generate()
        cw.write(config)


@app.task(ignore_result=True)
def generate_from_facts(siteid, hostnames):
    hosts = list(find('FACTS', {'siteid': siteid, 'hostname': {'$in': hostnames}},{'Components.timestamp':0}))
    poperator_gen.delay(siteid=siteid, data_type='Nagios', data=hosts)


@app.task(ignore_result=True)
def swd_subscribe(sites, subscriptions):
    for siteid in sites:
        logger.info('Adding subscriptions %s to siteid %s', subscriptions, siteid)
        poperator_gen.delay(siteid=siteid, data_type='SWD', data=subscriptions)


@app.task(ignore_result=True)
def poperator_del(siteid, data_type, data):
    class_map = POPERATOR_TYPE_CLASS_MAP.get(data_type)
    if not class_map:
        logger.error('Persist - No classmap for data type %s', data_type)
        return

    tmp_work_dir = '/tmp'
    cw = SVNConfigWriter(tmp_work_dir, IBC['SVN_URL'], IBC['SVN_USERNAME'], IBC['SVN_PASSWORD'], siteid)
    files = [ '{directory}/{siteid}/siteinfo/SiteInfo_{id}.xml'.format(directory=tmp_work_dir,siteid=siteid,id=data.get('deployment_id')),
              '{directory}/{siteid}/siteinfo/pcm_manifest_{id}.json'.format(directory=tmp_work_dir,siteid=siteid,id=data.get('deployment_id'))
            ]
    for fl in files:
        cw.remove('',fl)
