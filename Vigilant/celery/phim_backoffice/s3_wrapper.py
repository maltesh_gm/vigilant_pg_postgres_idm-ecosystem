from __future__ import absolute_import

from celery.utils.log import get_logger

import boto
from boto.s3.key import Key
from boto.exception import S3ResponseError
from boto.s3.connection import Location

from cached_property import cached_property

logger = get_logger(__name__)

# _acces_key = 'AKIAJZTX6O6MCXBOO5SQ'
# _secret_key = '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'


class S3Wrapper():
    def __init__(self, access_key, secret_key, bucket_name):
        self._bucket_name = bucket_name
        self.conn = boto.connect_s3(access_key, secret_key)

    @cached_property
    def bucket(self):
        try:
            return self.conn.get_bucket(self._bucket_name)
        except S3ResponseError:
            # Try Creating new bucket, if bucket is not available
            return self.conn.create_bucket(self._bucket_name, location=Location.DEFAULT)

    @cached_property
    def S3Key(self):
        return Key(self.bucket)

    def _s3_file_name(self, folder, file_name):
        if folder:
            folder = folder if folder.endswith('/') else folder + '/'
            return folder + file_name
        return file_name

    def upload(self, file_name, absolute_file_path, s3_folder):
        # file_name - name of the file
        # absolute_file_path - Absolute path to the file
        self.S3Key.key = self._s3_file_name(s3_folder, file_name)
        _uploaded_size = self.S3Key.set_contents_from_filename(
            absolute_file_path)

        return _uploaded_size