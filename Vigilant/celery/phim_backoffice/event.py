from __future__ import absolute_import

from celery.utils.log import get_logger
from datetime import datetime

from phim_backoffice.celery import app, rdb
from phim_backoffice.dashboard import handle_dashboard
from phim_backoffice.datastore import find_one, upsert_one_by_id, insert_one
from phim_backoffice.pg_datastore import pg_insert_one, prepare_json_doc, pg_find_one
from phimutils.message import Message
from phimutils.namespace import ns2dict
from tbconfig import ALIVE, DB_EVENTS
import json

logger = get_logger(__name__)


@app.task(ignore_result=True)
def stale_alive(siteid):
    stored_alive = find_one('ALIVES', siteid)
    if not stored_alive:
        return

    current_time = datetime.utcnow()
    print stored_alive['alive_expiration']
    if isinstance(stored_alive['alive_expiration'],basestring):
        stored_alive['alive_expiration'] = datetime.strptime(stored_alive['alive_expiration'], '%Y-%m-%dT%H:%M:%S.%f')
    if stored_alive['alive_expiration'] < current_time:
        logger.info('Site alive expired siteid %s', siteid)
        state = 'CRITICAL'
        type = 'PROBLEM'
    else:
        state = 'OK'
        type = 'RECOVERY'

    last_state = stored_alive.get('last_state', 'OK')
    if last_state == state:
        logger.debug('State already present siteid', siteid)
        return

    upsert_one_by_id('ALIVES', siteid, {'last_state': state})
    hostname = stored_alive.get('hostname', ALIVE['DEFAULT_HOSTNAME'])
    send_event(siteid=siteid, hostname=hostname, type=type, hostaddress=ALIVE['ADDRESS'], service=ALIVE['SERVICE'],
               state=state, datetime=current_time)


def send_event(siteid, hostname, service, type, state, datetime=None, hostaddress=None, payload=None):
    msg = Message(
        siteid=siteid,
        hostname=hostname,
        service=service,
        type=type,
        state=state,
        datetime=datetime,
        hostaddress=hostaddress,
        payload=payload
    )
    handle_event.delay(msg.to_dict())


@app.task(ignore_results=True)
def handle_event(data):
    # event_data = data.to_dict()
    event_data = data
    payload = data.pop('payload', {})
    if isinstance(payload, basestring):
        # for backwards compatibility. To be removed after all nodes are sending messages in new format.
        event_data['output'] = payload
        event_data['components'] = []
        event_data['modules'] = []
    else:
        event_data['output'] = payload.get('output', '')
        event_data['components'] = payload.get('components', [])
        event_data['modules'] = payload.get('modules', [])
    event_data['timestamp'] = data.get('timestamp')
    event_data['namespace'] = ns2dict(data.get('service'))
    identifier = '{siteid}-{hostname}'.format(**event_data)
    productid = get_productid(identifier)
    event_data['productid'] = productid
    logger.debug('Handling event data %s', event_data)

    insert_one('EVENTS', event_data)
    siteid_fk = pg_find_one('SITES', 'siteid',event_data.get('siteid'))
    if siteid_fk == 0:
        siteid_fk = pg_insert_one('SITES' , 'siteid', {'siteid':event_data.get('siteid')})
    pg_event_data = {key.lower(): event_data.get(key.lower()) for key in DB_EVENTS}
    # pg_event_data['perfdata'] = json.dumps(pg_event_data['perfdata'])
    pg_event_data['json_doc'] = json.dumps(prepare_json_doc(event_data, DB_EVENTS))
    pg_event_data['siteid_fk'] = pg_event_data.pop('siteid')
    if rdb:
        namspace_fk = rdb.get(data.get('service'))
    if not namspace_fk:
        namspace_fk = pg_insert_one('NAMESPACE', 'namespace_id', pg_event_data.get('namespace'))
    if namspace_fk:
        rdb.set(data.get('service'), namspace_fk)
    print ' NAMESPACE FK -------------->', namspace_fk
    pg_event_data.pop('namespace')
    pg_event_data['namespace_fk'] = namspace_fk

    event_data['namespace_fk'] = namspace_fk
# json.dumps(pg_event_data.pop('namespace'))
    event_id = pg_insert_one('EVENTS', 'event_id',pg_event_data )
    handle_dashboard.delay(event_data,event_id=event_id)
    print event_id


def get_productid(identifier):
    fact = find_one('FACTS', {'_id': identifier})
    if fact:
        return fact.pop('product_id',None)
