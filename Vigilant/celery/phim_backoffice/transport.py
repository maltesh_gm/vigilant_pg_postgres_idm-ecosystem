from __future__ import absolute_import

from phimutils.heartbeat import create_msg, send_message
from phimutils.timestamp import custom_timestamp
from phimutils.queueman import QueueMan
from phim_backoffice.celery import app
from taskbase.mapfiletask import create_json_map_file_task
from tbconfig import HB, QUEUE_TRANSPORT_MAP, EXCHANGE_QUEUE_MAP

from celery.utils.log import get_logger


logger = get_logger(__name__)
qm = QueueMan(EXCHANGE_QUEUE_MAP)


@app.task(base=create_json_map_file_task(HB['CODE_MAP_FILE']), ignore_result=True)
def hb_event(data):
    mapped = hb_event.mapper.get(data.service)
    if not mapped:
        return

    types_map = mapped.get('types')
    if types_map is None or data.type in types_map:
        message = create_msg(
            data.siteid,
            data.hostname,
            data.hostaddress,
            custom_timestamp(None),
            mapped['error_type'],
            mapped['error_code'],
            '{state} - {type}'.format(state=data.state, type=data.type)
        )
        send_site_email_msg.delay(data.siteid, message)


@app.task(base=create_json_map_file_task(HB['SITE_EMAIL_MAP_FILE']), ignore_result=True)
def send_site_email_msg(siteid, message, subject=None):
    address_to = send_site_email_msg.mapper.get(siteid, HB['TO_EMAIL'])
    # If address is set to something falsey in mapper, don't send email
    if not address_to:
        return
    send_message(message, smtpto=address_to, smtpfrom=HB['FROM_EMAIL'], smtpserver=HB['SMTP_SERVER'], subject=subject)


@app.task(ignore_results=True)
def queuemapper(data):
    exchange = qm.get_exchange(QUEUE_TRANSPORT_MAP[data.payload['queue']])
    with app.producer_or_acquire(producer=None) as producer:
        producer.publish(body=data.payload['data'], content_type='application/json', exchange=exchange)
