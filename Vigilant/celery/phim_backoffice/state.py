from __future__ import absolute_import

from celery.utils.log import get_logger

from phim_backoffice.datastore import upsert_one_by_id, delete_many
from phim_backoffice.dashboard import is_problem_site, get_problem_services_by_host
from phim_backoffice.event import send_event
from phim_backoffice.celery import app
from phim_backoffice.helpers import get_identified_host_dictionaries
from phimutils.stateredis import StateRedis
from tbconfig import STATE


logger = get_logger(__name__)


@app.task(ignore_result=True)
def update_state(siteid, data):
    for identifier, host_state in get_identified_host_dictionaries(siteid, data):
        upsert_one_by_id('STATES', identifier, host_state)
    if is_problem_site(siteid):
        update_problem_states.delay(siteid, data)


@app.task(ignore_result=True)
def update_problem_states(siteid, data):
    for item in get_problem_services_by_host(siteid):
        send_events_for_recovered_services(siteid, item['hostname'], item['services'], data.get(item['hostname']))


def get_hostaddress(host_state):
    if host_state:
        return host_state.get(STATE['HOSTADDRESS_KEY'])


def send_events_for_recovered_services(siteid, hostname, problem_services, host_state):
    hostaddress = get_hostaddress(host_state)
    for service, reason in get_recovered_services(host_state, problem_services):
        logger.info('State recovery siteid %s, hostname %s, service %s', siteid, hostname, service)
        send_event(
            siteid=siteid,
            hostname=hostname,
            type='RECOVERY',
            hostaddress=hostaddress,
            service=service,
            state='OK',
            payload=dict(output='OK - {reason}'.format(reason=reason))
        )


def get_recovered_services(host_state, services):
    if host_state is None:
        # host not found in state; problems not longer valid
        for service in services:
            yield service, 'Host not present in State data'
        return

    for service in services:
        try:
            if host_state[StateRedis.get_state_key_name(service)] == 'OK':
                yield service, 'From Service State data'
        except KeyError:
            # service not found in state; problem not longer valid
            yield service, 'Service not present in State data'


@app.task(ignore_result=True)
def purge_extra_hosts_state(siteid, hostnames):
    logger.debug('State purging for hosts not in %s', hostnames)
    delete_many('STATES', {'siteid': siteid, 'hostname': {'$nin': hostnames}})
