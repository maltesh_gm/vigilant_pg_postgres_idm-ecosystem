# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimuwsgi %{_prefix}/lib/philips/uwsgi/

# Basic Information
Name:      phim-tank
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   phim-tank setup

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  phimutils
Requires:  phim-celery-tasks-server
Requires:  phim-uwsgi
Provides:  phim-tank

%description
phim-tank setup


%prep
%setup -q -n %{name}-%{version}

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/uwsgi
%{__cp} %{_builddir}/%{buildsubdir}/uwsgi.tank.ini %{buildroot}%{_sysconfdir}/uwsgi/tank.ini
%{__install} -d -m 0755 %{buildroot}%{phimuwsgi}/tank/
%{__install} -Dp -m 0755 %{_builddir}/%{buildsubdir}/tank_app.py %{buildroot}%{phimuwsgi}/tank/
%{__cp} -r %{_builddir}/%{buildsubdir}/tank %{buildroot}%{phimuwsgi}/tank/


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sysconfdir}/uwsgi/tank.ini
%{phimuwsgi}/tank/


%changelog
* Thu Apr 02 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
