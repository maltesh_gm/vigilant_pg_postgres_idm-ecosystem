from tank.models.state import StateModel
from tank.util import get_bottle_app_with_mongo
from tank.tnconfig import NEB_HOSTNAME


def get_app(uri, db_name):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name)
    app.get('/siteid/<siteid>/hostname/<hostname>/key/<key>', callback=get_key_state)
    app.get('/siteids', callback=get_all_siteids)
    app.get('/sites/thruk/mapping', callback=get_sites_thruk_mapping)
    return app


def get_key_state(mongodb, siteid, hostname, key):
    return StateModel(mongodb, siteid).get_state(hostname, key)


def get_all_siteids(mongodb):
    return StateModel(mongodb, None).get_siteids()

def get_sites_thruk_mapping(mongodb):
    return StateModel(mongodb, None).get_mappings(NEB_HOSTNAME)


