from bottle import request
from tank.models.audit import AuditDistributionModel, AuditDeploymentModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name)
    app.get('/distribution', callback=get_distribution_data)
    app.get('/deployment', callback=get_deployment_data)
    return app


def get_distribution_data(mongodb):
    return AuditDistributionModel(mongodb).get_data(request.query)


def get_deployment_data(mongodb):
    return AuditDeploymentModel(mongodb).get_data(request.query)
