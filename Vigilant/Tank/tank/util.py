import logging
import time
import re
import bottle_mongo
from bottle import Bottle
from datetime import datetime

from phimutils.plogging import LOG_DATE_FORMAT, LOG_FORMAT


def get_logger():
    log = logging.getLogger('phim.tank')
    log.setLevel(logging.INFO)

    logging.Formatter.converter = time.gmtime
    formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)

    handler_console = logging.StreamHandler()
    handler_console.setFormatter(formatter)
    log.addHandler(handler_console)

    return log


def get_bottle_app_with_mongo(uri, db_name):
    app = Bottle()
    mongo_plugin = bottle_mongo.MongoPlugin(uri=uri, db=db_name)
    app.install(mongo_plugin)
    return app


def get_projection(items):
    result = dict((key, 1) for key in items)
    result['_id'] = 0
    return result


def get_keys_in_site(site_facts):
    result = set()
    for host in site_facts:
        result.update(host.keys())
    return list(result)


def get_version_parts(version):
    return re.split(r'\.|,', version)


def is_version_match(left, right):
    left_parts, right_parts = map(get_version_parts, [left, right])
    return all(map(lambda parts: parts[0] == parts[1], zip(left_parts, right_parts)))


def get_date_to_string_field_projection(field):
    return {'$dateToString': {'format': '%Y/%m/%d  %H:%M:%S', 'date': field}}


def rename_id(label, item):
    """Destructively rename _id with the label"""
    item[label] = item.pop('_id')
    return item


def get_list_from_param(param):
    """Get a list from a parameter. It is assumed that the parameter may be a comma separated string"""
    if param is None:
        return []
    return filter(None, set(x.strip() for x in param.split(',')))


class QueryBuilder(object):
    parameters = {}

    def __init__(self, query):
        self.query = query

    def process_query(self):
        pass

    def get_filter(self):
        self.process_query()
        parameters = ((field, self.query.get(query_key)) for query_key, field in self.parameters.iteritems())
        return dict(filter(lambda x: x[1], parameters))

    def get_datetime(self, timestamp):
        if timestamp is not None:
            return datetime.strptime(timestamp, '%Y-%m-%d')

    def get_between_filter(self, start, end):
        result = {}
        if start is not None:
            result['$gte'] = start
        if end is not None:
            result['$lte'] = end
        return result

    def get_between_dates_filter(self, start, end):
        return self.get_between_filter(self.get_datetime(start), self.get_datetime(end))
