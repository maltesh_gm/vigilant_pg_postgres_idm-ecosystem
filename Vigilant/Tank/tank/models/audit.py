from tank.tnconfig import COLLECTIONS
from tank.util import QueryBuilder
from tank.util import get_date_to_string_field_projection


class AuditQuery(QueryBuilder):
    audit_type = None

    def process_query(self):
        site_id = self.query.pop('site_id', None)
        if site_id:
            self.query['site_id'] = {'$in': site_id.split(',')}
        start_date = self.query.pop('start_date', None)
        end_date = self.query.pop('end_date', None)
        self.query['timestamp'] = self.get_between_dates_filter(start_date, end_date)
        self.query['type'] = self.audit_type


class AuditDeploymentQuery(AuditQuery):
    audit_type = 'deployment'

    parameters = {
        'site_id': 'siteid',
        'package': 'package',
        'timestamp': 'timestamp',
        'type': 'type',
    }


class AuditDistributionQuery(AuditQuery):
    audit_type = 'distribution'

    parameters = {
        'site_id': 'siteid',
        'subscription': 'subscription',
        'timestamp': 'timestamp',
        'type': 'type',
    }


class AuditModel(object):
    query_builder = None

    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['AUDIT']]
        self.filter = {}

    def get_matches(self):
        matches = self.collection.aggregate([
            {'$match': self.filter},
            {'$project': self.get_projection()},
            {'$sort': {'timestamp': -1}}
        ])
        return {'result': list(matches)}

    def get_projection(self):
        pass

    def get_data(self, query):
        self.filter = self.query_builder(query).get_filter()
        return self.get_matches()


class AuditDistributionModel(AuditModel):
    query_builder = AuditDistributionQuery

    def get_projection(self):
        return {
            '_id': 0,
            'subscription': '$subscription',
            'status': '$status',
            'user': '$user',
            'siteid': '$siteid',
            'timestamp': get_date_to_string_field_projection('$timestamp'),
            'version': '$version'
        }


class AuditDeploymentModel(AuditModel):
    query_builder = AuditDeploymentQuery

    def get_projection(self):
        return {
            '_id': 0,
            'package': '$package',
            'status': '$status',
            'hosts': '$hosts',
            'user': '$user',
            'siteid': '$siteid',
            'timestamp': get_date_to_string_field_projection('$timestamp'),
            'version': '$version'
        }
