from tank.tnconfig import COLLECTIONS
from tank.util import get_logger


log = get_logger()


class ExceptionsModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['EXCEPTIONS']]

    def get_exceptions(self, exception_type):
        return {'exceptions': sorted(self.collection.distinct(exception_type))}
