#!/usr/bin/env python

from bottle import Bottle, BaseRequest, response,request
from bottle.ext import beaker


session_opts = {
    'session.type': 'cookie',
    'session.cookie_expires':600,
    'session.data_dir': '/tmp',
    'session.auto': True,
    'session.validate_key': 'PHILIPS_IDM',
    'session.encrypt_key': 'PHILIPS_IDM'
}

from tank.controllers import (
    audit,
    country,
    dashboard,
    event,
    exception,
    fact,
    field,
    info,
    site,
    state,
    subscription,
    tasks,
    tags,
    user
)
from tank.tnconfig import MONGODB
from tank.util import get_logger


log = get_logger()


def get_app():
    app = Bottle()
    db_params = dict(uri=MONGODB['URL'], db_name=MONGODB['DB_NAME'])
    app.mount('/audit/', audit.get_app(**db_params))
    app.mount('/info/', info.get_app(**db_params))
    app.mount('/dashboard/', dashboard.get_app(**db_params))
    app.mount('/subscription/', subscription.get_app(**db_params))
    app.mount('/facts/', fact.get_app(**db_params))
    app.mount('/event/', event.get_app(**db_params))
    app.mount('/exceptions/', exception.get_app(**db_params))
    app.mount('/field/', field.get_app(**db_params))
    app.mount('/site/', site.get_app(**db_params))
    app.mount('/country/', country.get_app(**db_params))
    app.mount('/state/', state.get_app(**db_params))
    app.mount('/tags/', tags.get_app(**db_params))
    app.mount('/user/', user.get_app(**db_params))
    app.merge(tasks.get_app())
    app.get('/health', callback=health)
    return app


def health():
    log.debug('Returning Health')
    return 'OK'


BaseRequest.MEMFILE_MAX = 1024 * 512 * 2
app = get_app()
beaker_app = beaker.middleware.SessionMiddleware(get_app(), session_opts)



if __name__ == '__main__':
    log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    app.run(host='0.0.0.0', port=8080, debug=True)
