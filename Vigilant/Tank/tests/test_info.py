import unittest
from mock import patch, MagicMock


class TankInfoTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {
            'tank.util': self.mock_tank.util,
            'tank.models.state': self.mock_tank.models.state
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.controllers import info
        self.info = info

        self.info.COLLECTIONS = {'ALIVES': 'alives', 'STATES': 'states', 'SITES': 'sites'}
        self.info.NEB_IP_NAMESPACE = 'Administrative__Philips__Host__Information__IPAddress'
        self.info.NEB_HOSTNAME = 'localhost'
        self.info.MONITOR_URL_TEMPLATE = 'https://{address}/thruk'
        self.mongodb = MagicMock(name='mongo_db')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_empty_results_is_fine(self):
        hosts = []
        alives = []
        states = []
        names = []

        self.assertEqual(self.info.get_site_status(hosts, alives, states, names), {'info' : []})

    def test_simple_sites(self):
        hosts = [{'_id': 'FOOXX', 'hosts': 43}]
        alives = [{'_id': 'FOOXX', 'last_state': 'OK', 'alive_expiration': "12:00" }]
        states = [{'siteid': 'FOOXX', self.info.NEB_IP_NAMESPACE: '192.168.44.2' }]
        names = [{'siteid' : 'ASPXX', 'sitename': 'The ASPX Test Site'}]

        expected = {
            'info': [
                {
                    'siteid' :'FOOXX',
                    'last_state': 'OK',
                    'alive_expiration': '12:00',
                    'hosts': 43,
                    'nebnode': '192.168.44.2'
                }
            ]
        }
        self.assertEqual(self.info.get_site_status(hosts, alives, states, names), expected)

    def test_missing_host_sites(self):
        hosts = []
        alives = [{'_id': 'FOOXX', 'last_state': 'OK', 'alive_expiration': "12:00"}]
        states = [{'siteid': 'FOOXX', self.info.NEB_IP_NAMESPACE: '192.168.44.2'}]
        names = [{'siteid' : 'ASPXX', 'sitename': 'The ASPX Test Site'}]

        expected = {
            'info': [
                {'siteid' :'FOOXX','last_state': 'OK', 'alive_expiration': '12:00', 'nebnode': '192.168.44.2'}
            ]
        }
        self.assertEqual(self.info.get_site_status(hosts, alives, states, names), expected)

    def test_missing_alives(self):
        hosts = [{'_id': 'FOOXX', 'hosts': 43}]
        alives = []
        states = [{'siteid': 'FOOXX', self.info.NEB_IP_NAMESPACE: '192.168.44.2'}]
        names = [{'siteid' : 'ASPXX', 'sitename': 'The ASPX Test Site'}]

        expected = {'info': [{'hosts': 43, 'nebnode': '192.168.44.2'}]}

        self.assertEqual(self.info.get_site_status(hosts, alives, states, names), expected)

    def test_missing_states(self):
        hosts = [{'_id': 'FOOXX', 'hosts' : 43}]
        alives = [{'_id': 'FOOXX', 'last_state' : 'OK', 'alive_expiration' : "12:00"}]
        states = []
        names = [{'siteid' : 'ASPXX', 'sitename': 'The ASPX Test Site'}]

        expected = {
            'info': [{'siteid' :'FOOXX','last_state': 'OK', 'alive_expiration': '12:00', 'hosts': 43}]
        }
        self.assertEqual(self.info.get_site_status(hosts, alives, states, names), expected)

    def test_missing_names(self):
        hosts = [{'_id': 'FOOXX', 'hosts': 43}]
        alives = [{'_id': 'FOOXX', 'last_state': 'OK', 'alive_expiration': "12:00" }]
        states = [{'siteid': 'FOOXX', self.info.NEB_IP_NAMESPACE: '192.168.44.2' }]
        names = []

        expected = {
            'info': [
                {
                    'siteid' :'FOOXX',
                    'last_state': 'OK',
                    'alive_expiration': '12:00',
                    'hosts': 43,
                    'nebnode': '192.168.44.2'
                }
            ]
        }
        self.assertEqual(self.info.get_site_status(hosts, alives, states, names), expected)


    def test_mixed_sites_union(self):
        hosts = [{'_id': 'FOOXX', 'hosts': 43}]
        alives = [{'_id': 'BARXX', 'last_state': 'OK', 'alive_expiration': "12:00"}]
        states = [{'siteid': 'ASPXX', self.info.NEB_IP_NAMESPACE: '192.168.44.2'}]
        names = [{'siteid' : 'ASPXX', 'sitename': 'The ASPX Test Site'}]

        expected = {'info': [
            {'last_state': 'OK', 'alive_expiration': '12:00', 'siteid': 'BARXX'},
            {'hosts': 43},
            {'nebnode': '192.168.44.2', 'sitename': 'The ASPX Test Site'}
        ]}

        self.assertEqual(self.info.get_site_status(hosts, alives, states, names), expected)

    def test_mixed_sites_nounion(self):
        hosts = [{'_id': 'FOOXX', 'hosts': 43}]
        alives = [{'_id': 'BARXX', 'last_state': 'OK', 'alive_expiration': "12:00"}]
        states = [{'siteid': 'ASPXX', self.info.NEB_IP_NAMESPACE: '192.168.44.2'}]
        names = [{'siteid' : 'ASPXX', 'sitename': 'The ASPX Test Site'}]

        expected ={ 'info': [{ 'siteid' :'BARXX','last_state': 'OK', 'alive_expiration': '12:00'}]}

        self.assertEqual(self.info.get_site_status(hosts, alives, states, names, False), expected)

    def test_get_monitor_address(self):
        self.info.StateModel.return_value.get_state.return_value = {'result': '10.3.4.5'}
        self.assertEqual(self.info.get_monitor_address(self.mongodb, 'YY123'), {'address': '10.3.4.5'})
        self.info.StateModel.assert_called_once_with(self.mongodb, 'YY123')
        self.info.StateModel.return_value.get_state.assert_called_once_with(
            self.info.NEB_HOSTNAME, self.info.NEB_IP_NAMESPACE
        )

    def test_get_monitor_url(self):
        self.info.get_monitor_address = MagicMock(name='get_monitor_address', return_value={'address': '10.5.6.7'})
        self.assertEqual(self.info.get_monitor_url(self.mongodb, 'YY123'), {'url': 'https://10.5.6.7/thruk'})
        self.info.get_monitor_address.assert_called_once_with(self.mongodb, 'YY123')

    def test_get_monitor_url_no_address(self):
        self.info.get_monitor_address = MagicMock(name='get_monitor_address', return_value={'address': None})
        self.assertEqual(self.info.get_monitor_url(self.mongodb, 'YY123'), {'url': None})
        self.info.get_monitor_address.assert_called_once_with(self.mongodb, 'YY123')


if __name__ == '__main__':
    unittest.main()
