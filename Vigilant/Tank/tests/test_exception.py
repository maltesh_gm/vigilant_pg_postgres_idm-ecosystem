import unittest

from mock import patch, MagicMock


class TankExceptionModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import exception
        self.exception = exception

        self.mongodb = MagicMock(name='mongo_db')
        self.model = exception.ExceptionsModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_exceptions(self):
        self.model.collection.distinct.return_value = ['exception2', 'exception1']
        self.assertEqual(
            self.model.get_exceptions('type1'),
            {'exceptions': ['exception1', 'exception2']}
        )
        self.model.collection.distinct.assert_called_once_with('type1')


if __name__ == '__main__':
    unittest.main()
