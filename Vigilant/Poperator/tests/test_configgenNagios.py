import unittest
from mock import MagicMock, patch


class ConfiggenNagiosTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        self.mock_poperator = MagicMock(name='poperator')
        modules = {
            'logging': self.mock_logging,
            'poperator.nagios_configuration': self.mock_poperator.nagios_configuration,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from poperator import configgenNagios
        self.configgenNagios = configgenNagios

        var1 = [
            {
                'hostname': 'host1',
                'address': '10.0.0.1',
            },
            {
                'hostname': 'host02',
                'address': '10.0.0.2',
            }
        ]
        self.cn = self.configgenNagios.ConfiggenNagios(var1)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_filename(self):
        self.assertEqual(self.cn.get_filename('host1'), 'hosts/host1.cfg')

    def test_generate(self):
        mock_rule_set = self.configgenNagios.NagiosConfigurationRuleSet
        self.assertEqual(
            self.cn.generate(),
            {
                'hosts/host1.cfg': mock_rule_set.return_value.eval.return_value.__str__.return_value,
                'hosts/host02.cfg': mock_rule_set.return_value.eval.return_value.__str__.return_value
            }
        )


if __name__ == '__main__':
    unittest.main()
