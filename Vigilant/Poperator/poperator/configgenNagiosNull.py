import logging
from configgenNagios import ConfiggenNagios


spit_bucket = logging.getLogger(__name__)


class ConfiggenNagiosNull(ConfiggenNagios):
    def generate(self):
        return dict((self.get_filename(host['hostname']), None) for host in self.data)


# var1 = [
#     {
#         'hostname': 'host1',
#         'address': '10.0.0.1',
#         'manufacturer': 'VMware, Inc.',
#         'ISP': {
#             'module_type': '2',
#             'anywhere_version': '1.2',
#             'version': '4,4,3,1',
#             'identifier': '4x',
#             'flags': ['domain_needed', 'anywhere']
#         }
#     },
#     {
#         'hostname': 'host02',
#         'address': '10.0.0.254',
#         'LN': {
#             'alias': 'HOST02',
#             'use': 'esx_server'
#         }
#     }
# ]
# hh = ConfiggenNagios(var1)
#
# config = hh.generate()
