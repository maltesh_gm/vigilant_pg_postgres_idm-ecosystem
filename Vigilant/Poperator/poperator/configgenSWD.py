import os
import logging


spit_bucket = logging.getLogger(__name__)


class ConfiggenSWD(object):
    def __init__(self, data):
        self.data = data
        self.subscription_stubs_dir = 'subscription_links'

    def get_filename(self, subscription):
        return os.path.join(self.subscription_stubs_dir, subscription)

    def generate(self):
        return dict((self.get_filename(subscription), '') for subscription in self.data)


# var1 = ['product1', 'product2']
# hh = ConfiggenSWD(var1)
# config = hh.generate()

# this is how the data to post to discovery should look like
# b = {
#     'siteid': 'sux01',
#     'payload': {
#         'type': 'SWD',
#         'data': '####'
#     }
# }
