import py
import logging
import os
from phimutils.timestamp import iso_timestamp
from cached_property import cached_property


spit_bucket = logging.getLogger(__name__)


class SVNConfigWriter(object):
    def __init__(self, work_dir, sites_root_url, username, password, subpath):
        self.work_dir = work_dir
        self.sites_root_url = sites_root_url
        self.username = username
        self.password = password
        self.subpath = subpath

    @cached_property
    def wc(self):
        return self.setup_wc()

    @property
    def subpath_wc(self):
        return os.path.join(self.work_dir, self.subpath)

    @property
    def subpath_url(self):
        return os.path.join(self.sites_root_url, self.subpath)

    def setup_wc(self):
        auth = py.path.SvnAuth(self.username, self.password, cache_auth=False, interactive=False)
        subpath_dir = self.subpath_wc
        subpath_svn_url = self.subpath_url
        wc = py.path.svnwc(subpath_dir)
        wc.auth = auth
        spit_bucket.debug('Checking out %s to %s', subpath_svn_url, subpath_dir)
        wc.checkout(subpath_svn_url)
        return wc

    def process_config_file(self, filename, content):
        if content is None:
            file_to_remove = self.wc.join(filename)
            if file_to_remove.check():
                file_to_remove.remove()
        else:
            file_to_write = self.wc.ensure(filename)
            file_to_write.write(content)

    def log_status(self):
        status = self.wc.status(rec=1)
        attr_to_list = ['added', 'deleted', 'modified', 'conflict', 'unknown']
        for attr in attr_to_list:
            svnwc_files_with_attr = getattr(status, attr)
            with_attr_filenames = [item.strpath for item in svnwc_files_with_attr]
            spit_bucket.info('%s files: %s', attr, str(with_attr_filenames))

    def write(self, config):
        for filename, content in config.iteritems():
            self.process_config_file(filename, content)

        self.log_status()

        revision = self.wc.commit(msg='Generated configuration on %s' % iso_timestamp(None))
        if revision:
            spit_bucket.info('revision: %s', str(revision))
        else:
            spit_bucket.debug('No changes, nothing to commit')

    def remove(self,siteid,filename):
        self.wc.localpath = filename
        self.wc.remove(filename)
        self.wc.commit (msg="Deleteing the file %s for siteid %s"%(filename,siteid))