import re
import yaml
import json
from pynag import Model
from jinja2 import Template, Environment
from cached_property import cached_property
from itertools import chain
from copy import deepcopy


class NagiosConfiguration(object):
    def __init__(self):
        self.attributes = {}
        self.hostgroups = HostgroupsAttribute()

    def add_attribute(self, attribute, value):
        if not value:
            return
        if attribute == 'hostgroups':
            self.hostgroups.add(value)
        else:
            self.attributes[attribute] = value

    def get_attribute(self, attribute):
        if attribute == 'hostgroups':
            return self.hostgroups
        return self.attributes[attribute]

    def __nonzero__(self):
        return 'use' in self.attributes

    def get_model(self):
        model_attributes = self.attributes.copy()
        model_attributes['hostgroups'] = str(self.hostgroups)
        model = Model.Host()
        for attribute, value in model_attributes.iteritems():
            if value:
                model.set_attribute(attribute, value)
        return model

    def __str__(self):
        return str(self.get_model())


class HostgroupsAttribute(object):
    def __init__(self):
        self.base = ''
        self.groups = []

    def __nonzero__(self):
        return bool(self.base or self.groups)

    def set_base(self, base):
        self.base = base
        self.groups = []

    def add(self, groups):
        if groups.startswith('+'):
            self.groups.append(groups[1:])
        else:
            self.set_base(groups)

    def __str__(self):
        glue = ','
        combined_groups = filter(None, [self.base] + self.groups)
        joined_groups = glue.join(combined_groups)
        if not self.base and joined_groups:
           return '+{0}'.format(joined_groups)
        return joined_groups


class NagiosConfigurationRule(object):
    def __init__(self, rule):
        self.attribute = rule['attribute']
        self.set_condition(rule.get('when'))

    @cached_property
    def environment(self):
        return self.get_environment()

    def get_environment(self):
        environment = Environment()
        environment.tests['re_search'] = lambda value, pattern: bool(re.search(pattern, value))
        environment.filters['json'] = json.dumps
        return environment

    def set_condition(self, raw_condition):
        if not raw_condition:
            self.condition = None
            return
        # a Jinja2 evaluation that results in something Python can eval! #Ripped from Ansible
        presented = "{%% if %s %%} True {%% else %%} False {%% endif %%}" % raw_condition
        template = self.environment.from_string(presented)
        self.condition = lambda x: template.render(x).strip() == 'True'

    def eval(self, data):
        if (not self.condition) or self.condition(data):
            value = self.get_value(data)
            if value:
                return self.attribute, value

    def get_value(self, data):
        raise NotImplementedError

    def render_bare(self, source, data):
        return self.environment.from_string('{{%s}}' % source).render(data)


class NagiosMapConfigurationRule(NagiosConfigurationRule):
    def __init__(self, rule):
        super(NagiosMapConfigurationRule, self).__init__(rule)
        self.value_map = rule['map']
        self.source = rule['source']
        self.default = rule.get('default')

    def get_value(self, data):
        source_key = self.render_bare(self.source, data)
        return self.value_map.get(source_key, self.default)


class NagiosValueConfigurationRule(NagiosConfigurationRule):
    def __init__(self, rule):
        super(NagiosValueConfigurationRule, self).__init__(rule)
        self.value = rule['value']

    def get_value(self, data):
        return self.render_bare(self.value, data)


class NagiosConfigurationRuleException(Exception):
    pass


class NagiosConfigurationRuleFactory(object):
    rule_map = {
        'map': NagiosMapConfigurationRule,
        'value': NagiosValueConfigurationRule
    }

    def get_nagios_rule(self, data):
        for key, rule_class in self.rule_map.iteritems():
            if key in data:
                return rule_class(data)
        raise NagiosConfigurationRuleException('Rule missing valid type.')


class NagiosConfigurationRuleSet(object):
    def __init__(self):
        self.raw_data = None
        self.rule_set = []

    def load_dict(self, data):
        self.raw_data = deepcopy(data)
        rule_factory = NagiosConfigurationRuleFactory()
        for sub_rule_set in data:
            required_key, raw_rules = sub_rule_set.popitem()
            rules = [rule_factory.get_nagios_rule(raw_rule) for raw_rule in raw_rules]
            self.rule_set.append(dict(required_key=required_key, rules=rules))

    def load(self, filename):
        with open(filename) as f:
            data = yaml.load(f)
        self.load_dict(data)

    def get_results(self, data):
        applicable_subsets = (subset['rules'] for subset in self.rule_set if subset['required_key'] in data)
        return chain.from_iterable(self.get_outcomes(data, rules) for rules in applicable_subsets)

    def get_outcomes(self, data, rules):
        for rule in rules:
            rule_result = rule.eval(data)
            if rule_result:
                yield rule_result

    def eval(self, data):
        if not self.rule_set:
            raise NagiosConfigurationRuleException('Rule set empty')
        result = NagiosConfiguration()
        for attribute, value in self.get_results(data):
            result.add_attribute(attribute, value)
        return result
