import logging

import boto
from boto.s3.key import Key
from boto.exception import S3ResponseError
from boto.s3.connection import Location

from cached_property import cached_property

logger = logging.get_logger(__name__)

_acces_key = 'AKIAJZTX6O6MCXBOO5SQ'
_secret_key = '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'


class S3Wrapper():
    def __init__(self, access_key, secret_key, bucket_name):
        self._bucket_name = bucket_name
        self.conn = boto.connect_s3(access_key, secret_key)

    @cached_property
    def bucket(self):
        try:
            return self.conn.get_bucket(self._bucket_name)
        except S3ResponseError as e:
            if e.status == 404:
                logger.info("Creating new S3 bucket with name - %s",
                            self._bucket_name)
                return self.conn.create_bucket(self._bucket_name, location=Location.DEFAULT)
            logger.error("Boto Error: Status - %s, Reason - %s, Message - %s",
                         e.status, e.reason, e.message)
            raise e

    @cached_property
    def S3Key(self):
        return Key(self.bucket)

    def upload(self, file_name, absolute_file_path):
        # file_name - name of the file
        # absolute_file_path - Absolute path to the file
        self.S3Key.key = file_name
        _uploaded_size = self.S3Key.set_contents_from_filename(
            absolute_file_path)
        logger.info("File %s uploaded to S3, Size %s Bytes",
                    file_name, str(_uploaded_size))
