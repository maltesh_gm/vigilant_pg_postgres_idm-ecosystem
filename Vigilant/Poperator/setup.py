from distutils.core import setup
import os
phiversion = os.environ['phiversion']
setup(name='poperator',
      version=phiversion,
      description='Philips IDM Operator',
      author='Leonardo Ruiz',
      author_email='leonardo.ruiz@philips.com',
      #py_modules=['phimutils.timestamp'],
      packages=['poperator'],
      package_data={
          'poperator': [
              'nagios_configuration_rules.yml'
          ]
      },
)
