#!/usr/bin/env python
#Version:0.1

import argparse
from elasticsearch import Elasticsearch
from elasticsearch.client import IndicesClient
from datetime import datetime

PERF_INDEX_PREFIX = 'perf'
PERF_ALIAS = 'perf'
PERF_DATE_FORMAT = '%Y_%m_%d'


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to setup a time based index with an alias')
    parser.add_argument('-H', '--host',
                        help='The address of the elasticsearch server',
                        default='localhost')
    parser.add_argument('-p', '--port',
                        help='The port of the elasticsearch server',
                        default='9200')
    parser.add_argument('--prefix',
                        help='The prefix for the time based indexes',
                        default=PERF_INDEX_PREFIX)
    parser.add_argument('--date_format',
                        help='The date format string for the time based indexes',
                        default=PERF_DATE_FORMAT)
    parser.add_argument('--alias',
                        help='The alias for the time based indexes',
                        default=PERF_ALIAS)
    results = parser.parse_args(args)
    return results.host, results.port, results.prefix, results.date_format, results.alias


def elasticsearch_alias(host, port, prefix, date_format, alias):
    es = Elasticsearch(['%s:%s' % (host, port)])
    ic = IndicesClient(es)

    new_index = datetime.utcnow().strftime(prefix + '_' + date_format)

    if not ic.exists(new_index):
        ic.create(new_index)

    perf_exists = ic.exists(alias)
    perf_is_alias = ic.exists_alias(alias)

    #index exists and it is not an alias
    if perf_exists and not perf_is_alias:
        # not guaranteed to work :S (hopefully it will)
        # cuz after deletion it may be recreated by something trying to index before the alias is created further below
        ic.delete(alias)

    if perf_is_alias:
        update_alias(ic, new_index, alias)
    else:
        ic.put_alias(name=alias, index=new_index)


def main():
    elasticsearch_alias(*check_arg())


def update_alias(ic, new_index, alias):
    to_remove = ic.get_alias(alias).keys()
    update_actions = []
    for index in to_remove:
        update_actions.append(
            {"remove": {
                "alias": alias,
                "index": index
            }}
        )

    update_actions.append(
        {"add": {
            "alias": alias,
            "index": new_index
        }}
    )

    ic.update_aliases({"actions": update_actions})


if __name__ == '__main__':
    main()

