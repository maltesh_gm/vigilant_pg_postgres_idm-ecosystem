# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimuwsgi %{_prefix}/lib/philips/uwsgi/

# Basic Information
Name:      phim-billingapp
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   phim-billingapp setup

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Sijo Jose <sijo.jose@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  phimutils
Requires:  phim-celery-tasks-server
Requires:  phim-uwsgi
Provides:  phim-billingapp

%description
phim-billingapp setup


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/uwsgi
%{__cp} %{_builddir}/%{name}-%{version}/uwsgi.billingapp.ini %{buildroot}%{_sysconfdir}/uwsgi/billingapp.ini
%{__install} -d -m 0755 %{buildroot}%{phimuwsgi}/billingapp/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/billing_app.py %{buildroot}%{phimuwsgi}/billingapp/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/app_config.py %{buildroot}%{phimuwsgi}/billingapp/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/billing_models.py %{buildroot}%{phimuwsgi}/billingapp/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/utils.py %{buildroot}%{phimuwsgi}/billingapp/

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sysconfdir}/uwsgi/billingapp.ini
%{phimuwsgi}/billingapp/


%changelog
* Mon Oct 10 2017 Sijo Jose <sijo.jose@philips.com>
- Initial Spec File
